#!/bin/bash

count=0;
exit_status=$?

for ((i=1;i<=$1;i++))
do
  ../build/test/raft_test part1 leader_election
  exit_status=$?
  if [ $exit_status -eq 0 ]; then
      ((count++))
      echo "Case $i, $count/$1"
  else
      echo "Case $i, $count/$1"
  fi
done