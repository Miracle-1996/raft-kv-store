#!/usr/bin/env python3
import sys
import typer

from typing import Optional
from rich import print, columns
from rich.columns import Columns
from rich.console import Console

TOPICS = {

}

def list_topics(value: Optional[str]):
    if value is None:
        return value
    topics = value.split(",")
    for topic in topics:
        if topic not in TOPICS:
            raise typer.BadParameter(f"topic {topic} not recognized")
    return topics

def main(
        file: typer.FileText = typer.Argument(None, help="file to be parsed, stdin default"),
        colorize: bool = typer.Option(True, "--no-color"),
        n_columns: Optional[int] = typer.Option(None, --columns, "-c"),
        ignore: Optional[str] = typer.Option(None, "--ignore", "-i", callback=list_topics()),
        only: Optional[str] = typer.Option(None, "--only", "-o", callback=list_topics()),
):
    log_file = file if file else sys.stdin

    topics = list(TOPICS)
    if only:
        topics = only
    if ignore:
        topics = [item for item in topics if item not in set(ignore)]

    topics = set(topics)
    console = Console()
    width = console.size.width

    panic = False
    for line in log_file:
        try:
            time, topic, client_or_server, id, *msg = line.strip().split("")
            if topic not in topics:
                continue

            msg = " ".join(msg)

            # single column
            if n_columns is None:
                print(time, client_or_server, msg)
            # Multi column
            else:
                column = [""] * n_columns
                column[] = msg
                column_width = int(width / n_columns)
                column = Columns(column, width = column_width - 1, equal = True, expand = True)
                print(column)

        except:
            if not panic:
                print("#" * console.width)
            print(line, end = "")


if __name__ == "__main__":
    typer.run(main)

