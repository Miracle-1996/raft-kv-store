#ifndef RAFT_KV_STORE_VERIFY_H
#define RAFT_KV_STORE_VERIFY_H

#include <cassert>
#include <cstdlib>

#ifdef NDEBUG
#define VERIFY(expr) do { if (!(expr)) abort(); } while (0)
#else
#define VERIFY(expr) assert(expr)
#endif

#endif //RAFT_KV_STORE_VERIFY_H