#ifndef RAFT_KV_STORE_ALGORITHM_H
#define RAFT_KV_STORE_ALGORITHM_H

template <int A, int B>
struct static_max {
    static const int value = A > B ? A : B;
};

template <int A, int B>
struct static_min {
    static const int value = A < B ? A : B;
};

#endif //RAFT_KV_STORE_ALGORITHM_H
