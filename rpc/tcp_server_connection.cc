#include <spdlog/spdlog.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <rpc/connection.h>
#include <rpc/log.h>
#include <rpc/method_thread.h>
#include <rpc/tcp_server_connection.h>

TCPServerConnection::TCPServerConnection(TransmitManager *manager, int port, int lossy_test)
: lossy(lossy_test), manager(manager)
{
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));

    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    SPDLOG_INFO("server socket_fd = {}", socket_fd);
    if (-1 == socket_fd) {
        debug_server_call_socket_failure(manager->get_id());
    }
    int val = 1;
    VERIFY(0 == setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)));
    VERIFY(0 == setsockopt(socket_fd, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)));

    if (-1 == bind(socket_fd, (sockaddr *)&sin, sizeof(sin))) {
        debug_server_call_bind_failure(manager->get_id(), socket_fd);
    }
    if (-1 == listen(socket_fd, 1000)) {
        debug_server_call_listen_failure(manager->get_id(), socket_fd);
    }
    if (-1 == pipe(pipe_for_exit)) {
        debug_server_call_pipe_failure(manager->get_id());
    }
    SPDLOG_INFO("server pipe_for_exit[0] = {}", pipe_for_exit[0]);
    SPDLOG_INFO("server pipe_for_exit[1] = {}", pipe_for_exit[1]);
    int args = fcntl(pipe_for_exit[0], F_GETFL, NULL);
    args |= O_NONBLOCK;
    fcntl(pipe_for_exit[0], F_SETFL, args);

    SPDLOG_INFO("server {} starting accept_connection_thread.", manager->get_id());
    VERIFY((thread = method_thread0(this, &TCPServerConnection::accept_connection_thread, false)) not_eq 0);
}

TCPServerConnection::~TCPServerConnection() {
    VERIFY(0 == close(pipe_for_exit[1]));
    VERIFY(0 == pthread_join(thread, NULL));
    // close all the active connections
    for (auto iter = connection.begin(); iter != connection.end(); ++iter) {
        iter->second->close_connection();
        iter->second->decrease_reference();
    }
}

int
TCPServerConnection::get_port() {
    struct sockaddr_in sin;
    socklen_t size = sizeof(sin);
    if (-1 == getsockname(socket_fd, (sockaddr *)&sin, &size)) {
        debug_server_call_get_port_failure(manager->get_id());
    }
    return sin.sin_port;
}

void
TCPServerConnection::do_accept() {
    sockaddr_in sin;
    socklen_t size = sizeof(sin);
    int fd = accept(socket_fd, (sockaddr *)&sin, &size);
    if (-1 == fd) {
        debug_server_do_accept_fail(manager->get_id());
        pthread_exit(NULL);
    }

    SPDLOG_INFO("server {} do_accept get connection fd = {} {}:{}.", manager->get_id(), fd, inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
    Connection *c = new Connection(manager, fd, lossy);
    // garbage collect all dead connections with reference 1
    for (auto iter = connection.begin(); iter != connection.end(); ) {
        bool dead_state = iter->second->check_dead();
        int reference = iter->second->get_reference();
        if (dead_state and (1 == reference)) {
            SPDLOG_DEBUG("server {} do_accept garbage collect fd {}.", manager->get_id(), iter->second->get_socket_fd());
            iter->second->decrease_reference();
            connection.erase(iter++);
        }
        else {
            ++iter;
        }
    }
    connection[c->get_socket_fd()] = c;
}

void
TCPServerConnection::accept_connection_thread() {
    fd_set  read_fds;
    int max_fd = pipe_for_exit[0] > socket_fd ? pipe_for_exit[0] : socket_fd;

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(socket_fd, &read_fds);
        FD_SET(pipe_for_exit[0], &read_fds);

        int retval = select(max_fd + 1, &read_fds, NULL, NULL, NULL );

        if (retval < 0) {
            if (EINTR == errno) continue;
            else {
                debug_server_accept_connection_thread_call_select_fail(manager->get_id());
            }
        }

        if (FD_ISSET(socket_fd, &read_fds)) {
            SPDLOG_INFO("server {} accept_connection_thread receive select ok retval: {} call do_accept().", manager->get_id(), retval);
            do_accept();
        }
        else if (FD_ISSET(pipe_for_exit[0], &read_fds)) {
            close(socket_fd);
            close(pipe_for_exit[0]);
            SPDLOG_INFO("server {} accept_connection_thread receive select ok retval: {} close fd {} {}.", manager->get_id(), retval, socket_fd, pipe_for_exit[0]);
            return;
        }
        else {
            VERIFY(0);
        }
    }
}