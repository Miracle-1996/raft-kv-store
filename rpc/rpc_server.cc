#include <spdlog/spdlog.h>
#include <cstdlib>
#include <rpc/log.h>
#include <rpc/marshall.h>
#include <rpc/reply_request.h>
#include <rpc/rpc_const.h>
#include <rpc/rpc_server.h>
#include <rpc/rpc_utils.h>
#include <rpc/slock.h>
#include <rpc/tcp_server_connection.h>
#include <rpc/unmarshall.h>
#include <utils/verify.h>

RPCServer::RPCServer(unsigned int p, int count): port(p), remain_count(count), refresh_count_value(count) {
    VERIFY(0 == pthread_mutex_init(&mutex_for_counts, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_handlers, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_connections, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_reply_window, NULL));

    set_rand_seed();
    sid_nonce = random();

    char *lossy_env = getenv("RPC_LOSSY");
    if (lossy_env != NULL) {
        lossy_test = atoi(lossy_env);
    }

    SPDLOG_INFO("Server {} start lossy_test {}.", sid_nonce, lossy_test);
    SPDLOG_INFO("Server {} registering service bind.", sid_nonce);
    reg(RPCConst::bind, this, &RPCServer::bind);
    listener = new TCPServerConnection(this, port, lossy_test);
    if (0 == port) {
        port = listener->get_port();
    }
    SPDLOG_INFO("Server {} listener port is {}.", sid_nonce, port);
    dispatch_thread_pool = new ThreadPool(sid_nonce, 10, false);
}

RPCServer::~RPCServer() {
    // must delete listener before dispatch_thread_pool
    delete listener;
    delete dispatch_thread_pool;
    free_reply_window();
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_counts));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_handlers));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_connections));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_reply_window));
}

void
RPCServer::add_reply(unsigned int cid_nonce, unsigned int xid, char *buf, int size) {
    ScopedLock lock(&mutex_for_reply_window);
    auto reply_list = reply_window[cid_nonce];
    for (auto iter = reply_list.begin(); iter != reply_list.end(); ++iter) {
        if (iter->xid == xid) {
            iter->buf = buf;
            iter->size = size;
            iter->callback_valid = true;
            break;
        }
    }
}

int
RPCServer::bind(int &retval) {
    retval = sid_nonce;
    SPDLOG_INFO("server {} bind() called by client return sid_nonce {}.", sid_nonce, sid_nonce);
    return 0;
}

rpc_state
RPCServer::check_duplicate_and_update(unsigned int cid_nonce, unsigned int xid, unsigned int xid_reply, char **b, int *size) {
    rpc_state retval = NEW;
    ScopedLock reply_window_mutex(&mutex_for_reply_window);
    auto reply_list = reply_window[cid_nonce];
    auto iter = reply_list.begin();
    if (not reply_list.empty() and xid < reply_list.front().xid) {
        retval = FORGOTTEN;
        iter = reply_list.end();
    }
    for (; iter != reply_list.end(); ++iter) {
        if (xid == iter->xid) {
            if (iter->callback_valid) {
                *b = iter->buf;
                *size = iter->size;
                retval = DONE;
            }
            else {
                retval = INPROGRESS;
            }
            break;
        }
        else if (xid < iter->xid) {
            reply_list.insert(iter, reply_state(xid));
            VERIFY(NEW == retval);
            break;
        }
    }

    if (iter == reply_list.end() and NEW == retval) {
        reply_list.push_back(reply_state(xid));
    }
    // delete remembered requests with xid < xid_reply and free the reply_state::buf of each such request
    // the client says it has received a reply for every RPC up through xid_reply.
    while (not reply_list.empty() and reply_list.front().xid < xid_reply) {
        free(reply_list.front().buf);
        reply_list.pop_front();
    }
    if (reply_list.empty() or xid_reply < reply_list.front().xid) {
        reply_list.push_front(reply_state(xid_reply));
    }
    return retval;
}

void
RPCServer::dispatch_thread(dispatch_task_struct *d) {
    SPDLOG_DEBUG("dispatch_thread start for fd = {}.", d->connection->get_socket_fd());
    Connection *c = d->connection;
    Unmarshall request(d->buf, d->size);
    delete d;

    request_header header;
    request.unpack_request_header(&header);
    if (not request.check_ok()) {
        debug_server_dispatch_unmarshall_header_failure(sid_nonce);
        c->decrease_reference();
        SPDLOG_DEBUG("dispatch_thread end for fd = {}.", c->get_socket_fd());
        return;
    }

    int pid = header.procedure;
    debug_server_dispatch_rpc(header.sid, header.xid, pid, header.xid_reply, header.cid, sid_nonce);

    Marshall reply;
    reply_header header_for_reply(header.xid, 0);

    if (not reachable and pid != RPCConst::bind) {
        debug_server_dispatch_can_not_reachable(sid_nonce);
        header_for_reply.result = RPCConst::unreachable_failure;
        reply.pack_reply_header(header_for_reply);
        c->transmit(reply.get_buf(), reply.get_index());
        SPDLOG_DEBUG("dispatch_thread end for fd = {}.", c->get_socket_fd());
        return;
    }

    // check for client send to an old instance of server
    if (header.sid != 0 and header.sid != sid_nonce) {
        debug_server_dispatch_send_old_server_sid(sid_nonce, header.procedure, header.sid);
        header_for_reply.result = RPCConst::old_server_failure;
        reply.pack_reply_header(header_for_reply);
        c->transmit(reply.get_buf(), reply.get_index());
        SPDLOG_DEBUG("dispatch_thread end for fd = {}.", c->get_socket_fd());
        return;
    }

    // for raft test
    if (not reliable) {
        int delay = rand() % 27;
        usleep(delay * 1000);
    }

    // for raft test
    if (not reliable and pid != RPCConst::bind) {
        bool drop = rand() % 1000 < 100;
        if (drop) {
            debug_server_dispatch_timeout(sid_nonce);
            header_for_reply.result = RPCConst::timeout_failure;
            reply.pack_reply_header(header_for_reply);
            c->transmit(reply.get_buf(), reply.get_index());
            SPDLOG_DEBUG("dispatch_thread end for fd = {}.", c->get_socket_fd());
            return;
        }
    }

    // check for RPC pid is a registered procedure
    Handler *handler;
    {
        ScopedLock lock(&mutex_for_handlers);
        if (handlers.count(pid) < 1) {
            debug_server_dispatch_unknown_procedure(sid_nonce, pid);
            c->decrease_reference();
            SPDLOG_DEBUG("dispatch_thread end for fd = {}.", c->get_socket_fd());
            return;
        }
        handler = handlers[pid];
        SPDLOG_DEBUG("Server pass procedure {} to handler for fd = {}.", pid, c->get_socket_fd());
    }

    int size = 0;
    char *b = nullptr;
    rpc_state state;

    if (header.cid) {
        unsigned int cid_nonce = header.cid;
        {
            ScopedLock reply_window_mutex(&mutex_for_reply_window);
            if (reply_window.find(cid_nonce) == reply_window.end()) {
                VERIFY(0 == reply_window[cid_nonce].size());
                reply_window[cid_nonce].push_back(reply_state(0));
                debug_server_dispatch_add_client_to_reply_window(sid_nonce, header.cid, header.xid, c->get_socket_fd(), (int)reply_window.size());
            }
        }
        // save the latest good connection to the client
        {
            ScopedLock connection_mutex(&mutex_for_connections);
            if (connections.find(cid_nonce) == connections.end()) {
                c->increase_reference();
                connections[cid_nonce] = c;
            }
            else if (connections[cid_nonce]->compare(c) < 0) {
                connections[cid_nonce]->decrease_reference();
                c->increase_reference();
                connections[cid_nonce] = c;
            }
        }
        state = check_duplicate_and_update(header.cid, header.xid, header.xid_reply, &b, &size);
    }
    else {
        // the client does not require at-most-once semantics
        state = NEW;
    }
    SPDLOG_DEBUG("rpc_state is {} for fd = {}.", state, c->get_socket_fd());
    switch (state) {
        case NEW:
            if (refresh_count_value) {
                update_status(pid);
            }
            header_for_reply.result = handler->fn(request, reply);
            if (header_for_reply.result == RPCConst::unmarshall_args_failure) {
                debug_server_dispatch_unmarshall_args_failure(sid_nonce);
                VERIFY(0);
            }
            VERIFY(header_for_reply.result >= 0);
            reply.pack_reply_header(header_for_reply);
            reply.take_buf(&b, &size);
            debug_server_dispatch_send_and_save_reply(sid_nonce, header.cid, header.xid, pid, header_for_reply.result, size);
            if (header.cid > 0) {
                // only record replies for clients that require at-most-once semantics
                add_reply(header.cid, header.xid, b, size);
            }
            // get the latest connection to the client
            {
                ScopedLock connection_mutex(&mutex_for_connections);
                if (c->check_dead() and c != connections[header.cid]) {
                    c->decrease_reference();
                    c = connections[header.cid];
                    c->increase_reference();
                }
            }
            c->transmit(b, size);
            if (0 == header.cid) {
                // reply is not need to be added to the at-most-once window so free it
                free(b);
                b = nullptr;
            }
            break;
        case INPROGRESS: // server is working on this request
            break;
        case DONE:       // duplicate and we still have the response
            c->transmit(b, size);
        case FORGOTTEN:  // old request and we do not have the response
            debug_server_dispatch_recv_old_request(sid_nonce, header.xid, header.cid);
            header_for_reply.result = RPCConst::at_most_once_failure;
            reply.pack_reply_header(header_for_reply);
            c->transmit(reply.get_buf(), reply.get_index());
            break;
        default:
            debug_server_dispatch_unknown_rpc_state(sid_nonce);
    }
    c->decrease_reference();
    SPDLOG_DEBUG("dispatch_thread will be end for fd = {}.", c->get_socket_fd());
}

void
RPCServer::free_reply_window() {
    ScopedLock lock(&mutex_for_reply_window);
    for (auto it = reply_window.begin(); it != reply_window.end(); ++it) {
        for (auto iter = it->second.begin(); iter != it->second.end(); ++iter) {
            free((*iter).buf);
        }
        it->second.clear();
    }
    reply_window.clear();
}

int
RPCServer::get_port() const {
    return port;
}

bool
RPCServer::get_reachable() const {
    return reachable;
}

unsigned int
RPCServer::get_id() const {
    return sid_nonce;
}

bool
RPCServer::get_client_or_server() const {
    return client_or_server;
}

bool
RPCServer::get_pdu(Connection *c, char *b, int size) {
    dispatch_task_struct *arg = new dispatch_task_struct(c, b, size);
    c->increase_reference();
    SPDLOG_INFO("Server {} event_loop_thread add task to dispatch_thread_pool for fd = {}", c->get_manager_id(), c->get_socket_fd());
    bool success = dispatch_thread_pool->add_object_task(this, &RPCServer::dispatch_thread, arg);
    if (not success) {
        c->decrease_reference();
        delete arg;
    }
    return success;
}

void
RPCServer::register_service(unsigned int pid, Handler *handler) {
    ScopedLock lock(&mutex_for_handlers);
    VERIFY(0 == handlers.count(pid));
    handlers[pid] = handler;
    VERIFY(1 == handlers.count(pid));
}

void
RPCServer::set_reachable(bool reach) {
    reachable = reach;
}

void
RPCServer::set_reliable(bool r) {
    reliable = r;
}

void
RPCServer::unregister_all() {
    ScopedLock lock(&mutex_for_handlers);
    Handler *h = handlers[RPCConst::bind];
    handlers.clear();
    handlers[RPCConst::bind] = h;
}

void
RPCServer::update_status(unsigned int pid) {
    ScopedLock lock(&mutex_for_counts);
    ++counts[pid];
    --remain_count;
    if (0 == remain_count) {
        printf("RPC status: \n");
        for (auto iter = counts.begin(); iter != counts.end(); ++iter) {
            printf("%x:%d\n", iter->first, iter->second);
        }
        printf("\n");

        ScopedLock reply_window_mutex(&mutex_for_reply_window);
        unsigned int all_reply = 0;
        unsigned int max_reply = 0;
        for (auto iter = reply_window.begin(); iter != reply_window.end(); ++iter) {
            all_reply += iter->second.size();
            if (iter->second.size() > max_reply) {
                max_reply = iter->second.size();
            }
        }
        debug_server_show_reply_window_info(sid_nonce, (int)reply_window.size(), all_reply, max_reply);
        remain_count = refresh_count_value;
    }
}