#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <vector>
#include <rpc/log.h>
#include <rpc/select.h>
#include <rpc/slock.h>
#include <utils/verify.h>

Select::Select() {
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);

    VERIFY(0 == pipe(pipe_fd_for_sync));
    FD_SET(pipe_fd_for_sync[0], &read_fds);
    max_fd = pipe_fd_for_sync[0];

    // set nonblock on read pipe
    int args = fcntl(pipe_fd_for_sync[0], F_GETFL, NULL);
    args |= O_NONBLOCK;
    fcntl(pipe_fd_for_sync[0], F_SETFL, args);

    // init mutex
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));
    debug_init_select();
}

Select::~Select() {
    VERIFY(0 == pthread_mutex_destroy(&mutex));
}

void
Select::watch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask) {
    ScopedLock lock(&mutex);
    if (max_fd <= fd) {
        max_fd = fd;
    }

    if (RDONLY == mask) {
        FD_SET(fd, &read_fds);
    }
    else if (WRONLY == mask) {
        FD_SET(fd, &write_fds);
    }
    else if (RDWR == mask) {
        FD_SET(fd, &read_fds);
        FD_SET(fd, &write_fds);
    }
    else {
        debug_select_unknown_mask(client_or_server, id);
    }

    // sync
    char sync = 1;
    VERIFY(1 == write(pipe_fd_for_sync[1], &sync, sizeof(sync)));
}

bool
Select::unwatch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask) {
    ScopedLock lock(&mutex);
    if (RDONLY == mask) {
        FD_CLR(fd, &read_fds);
    }
    else if (WRONLY == mask) {
        FD_CLR(fd, &write_fds);
    }
    else if (RDWR == mask) {
        FD_CLR(fd, &read_fds);
        FD_CLR(fd, &write_fds);
    }
    else {
        debug_select_unknown_mask(client_or_server, id);
    }

    // update max_fd
    if (fd == max_fd and not FD_ISSET(fd, &read_fds) and not FD_ISSET(fd, &write_fds)) {
        int new_max_fd = pipe_fd_for_sync[0];
        for (int i = 0; i <= max_fd; ++i) {
            if (FD_ISSET(i, &read_fds)) {
                new_max_fd = i;
            }
            else if (FD_ISSET(i, &write_fds)) {
                new_max_fd = i;
            }
        }
        max_fd = new_max_fd;
    }
    // sync
    // question
    if (RDWR == mask) {
        char sync = 1;
        VERIFY(1 == write(pipe_fd_for_sync[1], &sync, sizeof(sync)));
    }
    return (not FD_ISSET(fd, &read_fds) and not FD_ISSET(fd, &write_fds));
}

bool
Select::is_watched(bool client_or_server, unsigned int id, int fd, read_write_mask mask) {
    ScopedLock lock(&mutex);
    if (RDONLY == mask) {
        return FD_ISSET(fd, &read_fds);
    }
    else if (WRONLY == mask) {
        return FD_ISSET(fd, &write_fds);
    }
    else if (RDWR == mask) {
        return (FD_ISSET(fd, &read_fds) and FD_ISSET(fd, &write_fds));
    }
    else {
        debug_select_unknown_mask(client_or_server, id);
        return false;
    }
}

void
Select::wait_ready(std::vector<int> *readable, std::vector<int> *writeable) {
    int copy_max_fd;
    fd_set copy_read_fds;
    fd_set copy_write_fds;

    {
        ScopedLock lock(&mutex);
        copy_max_fd = max_fd;
        copy_read_fds  = read_fds;
        copy_write_fds = write_fds;
    }
    debug_call_select_wait_ready(copy_max_fd);

    int select_retval = select(copy_max_fd + 1, &copy_read_fds, &copy_write_fds, NULL, NULL);

    if (-1 == select_retval) {
        // ignore interrupt
        if (EINTR == errno) {
            debug_select_wait_ready_interrupt();
            return;
        }
        else {
            debug_select_wait_ready_failure();
        }
    }

    VERIFY(select_retval > 0);
    debug_select_wait_ready_ok(select_retval);
    for (int fd = 0; fd <= copy_max_fd; ++fd) {
        if (fd == pipe_fd_for_sync[0] and FD_ISSET(fd, &copy_read_fds)) {
            char sync;
            VERIFY (1 == read(pipe_fd_for_sync[0], &sync, sizeof(sync)));
            VERIFY ((char)1 == sync);
        }
        else {
            if (FD_ISSET(fd, &copy_read_fds)) {
                readable->push_back(fd);
            }
            if (FD_ISSET(fd, &copy_write_fds)) {
                writeable->push_back(fd);
            }
        }
    }
}