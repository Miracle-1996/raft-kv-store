#include <spdlog/spdlog.h>
#include <cstddef>
#include <pthread.h>
#include <rpc/log.h>
#include <rpc/thread_pool.h>
#include <utils/verify.h>

static void *
worker_thread_start_routine(void *arg)
{
    ThreadPool *ptr = (ThreadPool *)arg;
    while (1) {
        ThreadPool::thread_task task;
        if (!ptr->get_thread_task(&task)) {
            break; // destructor force work thread to exit
        }
        (void)(task.start_routine)(task.arg);
    }
    pthread_exit(NULL);
}

ThreadPool::ThreadPool(int size, bool blocking): nthreads(size), block_add_thread(blocking), tasks(100 * size)
{
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 128 << 10);

    for (int i = 0; i < size; ++i) {
        pthread_t thread;
        VERIFY(0 == pthread_create(&thread, &attr, worker_thread_start_routine, (void *)this));
        thread_list.push_back(thread);
    }
}

ThreadPool::ThreadPool(unsigned int sid, int size, bool blocking): sid(sid), nthreads(size), block_add_thread(blocking), tasks(100 * size)
{
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 128 << 10);

    for (int i = 0; i < size; ++i) {
        pthread_t thread;
        VERIFY(0 == pthread_create(&thread, &attr, worker_thread_start_routine, (void *)this));
        thread_list.push_back(thread);
    }
    SPDLOG_INFO("server {} init thread pool size {} block {}.", sid, size, (blocking) ? "true" : "false");
}

ThreadPool::~ThreadPool() {
    destroy();
}

bool
ThreadPool::get_thread_task(thread_task* task) {
    tasks.dequeue(task);
    return (task->start_routine != NULL);
}

bool
ThreadPool::add_thread_task(void *(*start_routine)(void *), void *arg) {
    thread_task task;
    task.start_routine = start_routine;
    task.arg = arg;
    return tasks.enqueue(task, block_add_thread);
}

void
ThreadPool::destroy() {
    if (stopped) return;
    tasks.clear();
    for (int i = 0; i < nthreads; ++i) {
        thread_task task;
        task.start_routine = (void *(*)(void *))NULL; // in order to force the worker threads to exit
        tasks.enqueue(task);
    }

    for (int i = 0; i < nthreads; ++i) {
        VERIFY(0 == pthread_join(thread_list[i], NULL));
    }
    VERIFY(0 == pthread_attr_destroy(&attr));
    stopped = true;
}