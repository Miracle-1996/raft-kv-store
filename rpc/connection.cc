#include <spdlog/spdlog.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <strings.h>
#include <sys/time.h>
#include <unistd.h>
#include <rpc/asyn_event_manager.h>
#include <rpc/connection.h>
#include <rpc/log.h>
#include <rpc/slock.h>
#include <rpc/transmit.h>
#include <utils/verify.h>

Connection::Connection(TransmitManager *manager, int fd, int lossy_test): lossy(lossy_test), socket_fd(fd), manager(manager) {
    id = manager->get_id();
    client_or_server = manager->get_client_or_server();
    int args = fcntl(socket_fd, F_GETFL, NULL);
    args |= O_NONBLOCK;
    fcntl(socket_fd, F_SETFL, args);

    signal(SIGPIPE, SIG_IGN);

    VERIFY(0 == gettimeofday(&build_time, NULL));
    VERIFY(0 == pthread_cond_init(&send_wait, NULL));
    VERIFY(0 == pthread_cond_init(&send_complete, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_other, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_reference, NULL));

    AsynEventManager::Instance()->add_callback(manager->get_client_or_server(), manager->get_id(), socket_fd, RDONLY, this);
}

Connection::~Connection() {
    VERIFY(dead);
    VERIFY(0 == pthread_cond_destroy(&send_wait));
    VERIFY(0 == pthread_cond_destroy(&send_complete));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_other));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_reference));
    if (read_pdu_buffer.buf) {
        free(read_pdu_buffer.buf);
    }
    VERIFY(nullptr == write_pdu_buffer.buf);
    close(socket_fd);
}

int
Connection::get_reference() {
    ScopedLock lock(&mutex_for_reference);
    return reference;
}

int
Connection::get_socket_fd() {
    return socket_fd;
}

unsigned int
Connection::get_manager_id() {
    return manager->get_id();
}

int
Connection::compare(Connection *another) {
    if (build_time.tv_sec  > another->build_time.tv_sec)  return  1;
    if (build_time.tv_sec  < another->build_time.tv_sec)  return -1;
    if (build_time.tv_usec > another->build_time.tv_usec) return  1;
    if (build_time.tv_usec < another->build_time.tv_usec) return -1;
    return 0;
}

bool
Connection::check_dead() {
    ScopedLock lock(&mutex_for_other);
    return dead;
}

bool
Connection::transmit(char *buf, int size) {
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} Connection::transmit start for fd = {}.", get_manager_id(), get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} Connection::transmit start for fd = {}.", get_manager_id(), get_socket_fd());
    }
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} try to acquire lock = {} for fd = {}.", get_manager_id(), "mutex_for_other", get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} try to acquire lock = {} for fd = {}.", get_manager_id(), "mutex_for_other", get_socket_fd());
    }
    ScopedLock lock(&mutex_for_other);
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} hold lock = {} for fd = {}.", get_manager_id(), "mutex_for_other", get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} hold lock = {} for fd = {}.", get_manager_id(), "mutex_for_other", get_socket_fd());
    }
    ++waiters;
    TransmitBuffer &pdu = this -> write_pdu_buffer;
    while (not dead and pdu.buf) {
        VERIFY(0 == pthread_cond_wait(&send_wait, &mutex_for_other));
    }
    --waiters;
    if (dead) {
        if (client_or_server) {
            SPDLOG_DEBUG("Server {} Connection::transmit begin to return cos dead for fd = {}.", get_manager_id(), get_socket_fd());
        }
        else {
            SPDLOG_DEBUG("Client {} Connection::transmit begin to return cos dead for fd = {}.", get_manager_id(), get_socket_fd());
        }
        return false;
    }
    set_pdu(&write_pdu_buffer, buf, size, 0);

    if (lossy) {
        if ((random() % 100) < lossy) {
            debug_shutdown_socket(manager->get_client_or_server(), manager->get_id(), socket_fd);
            shutdown(socket_fd, SHUT_RDWR);
        }
    }

    if (not write_pdu()) {
        dead = true;
        // release lock because of blocking call for block_remove_fd()
        VERIFY(0 == pthread_mutex_unlock(&mutex_for_other));
        AsynEventManager::Instance()->block_remove_fd(manager->get_client_or_server(), manager->get_id(), socket_fd);
        VERIFY(0 == pthread_mutex_lock(&mutex_for_other));
    }
    else {
        if (pdu.transmit_amount == pdu.size);
        else {
            // should be rare to need to explicitly add writ callback
            debug_transmit_add_callback(manager->get_client_or_server(), manager->get_id());
            AsynEventManager::Instance()->add_callback(manager->get_client_or_server(), manager->get_id(), socket_fd, WRONLY, this);
            while (not dead and (0 <= pdu.transmit_amount and pdu.transmit_amount < pdu.size)) {
                debug_transmit_wait_send_complete(manager->get_client_or_server(), manager->get_id(), socket_fd, pdu.transmit_amount, pdu.size);
                VERIFY(0 == pthread_cond_wait(&send_complete, &mutex_for_other));
            }
            debug_transmit_done(manager->get_client_or_server(), manager->get_id(), socket_fd, pdu.transmit_amount, pdu.size);
        }
    }

    bool retval = ((not dead) and (pdu.size == pdu.transmit_amount));
    set_pdu(&write_pdu_buffer, nullptr, 0, 0);
    if (waiters > 0) {
        VERIFY(0 == pthread_cond_broadcast(&send_wait)); // question
    }
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} Connection::transmit begin to return for fd = {}.", get_manager_id(), get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} Connection::transmit begin to return for fd = {}.", get_manager_id(), get_socket_fd());
    }
    return retval;
}

void
Connection::increase_reference() {
    ScopedLock lock(&mutex_for_reference);
    ++reference;
}

void
Connection::decrease_reference() {
    VERIFY(0 == pthread_mutex_lock(&mutex_for_reference));
    --reference;
    VERIFY(reference >= 0);
    if (0 == reference) {
        VERIFY(0 == pthread_mutex_lock(&mutex_for_other));
        if (dead) {
            // question
            VERIFY(0 == pthread_mutex_unlock(&mutex_for_reference));
            VERIFY(0 == pthread_mutex_unlock(&mutex_for_other));
            delete this;
            return;
        }
        VERIFY(0 == pthread_mutex_unlock(&mutex_for_other));
    }
    VERIFY(0 == pthread_mutex_unlock(&mutex_for_reference));
}

void
Connection::read_callback(int fd) {
    ScopedLock lock(&mutex_for_other);
    SPDLOG_DEBUG("Connection::read_callback start for fd = {}", fd);
    VERIFY(socket_fd == fd);
    if (dead) {
        SPDLOG_WARN("The fd = {}'s connection is dead.", fd);
        return;
    }
    bool ok = true;
    TransmitBuffer &pdu = this -> read_pdu_buffer;
    if (not pdu.buf or pdu.transmit_amount < pdu.size) {
        ok = read_pdu();
    }
    if (not ok) {
        dead = true;
        if (client_or_server) {
            SPDLOG_WARN("event_loop_thread: Server {} fd = {}'s connection is dead.", manager->get_id(), socket_fd);
        }
        else {
            SPDLOG_WARN("event_loop_thread: Client {} fd = {}'s connection is dead.", manager->get_id(), socket_fd);
        }
        AsynEventManager::Instance()->del_callback(manager->get_client_or_server(), manager->get_id(), socket_fd, RDWR);
        pthread_cond_signal(&send_complete);
        debug_signal_send_complete_in_case_of_dead(manager->get_client_or_server(), manager->get_id(), socket_fd);
    }

    // question
    if (pdu.buf and pdu.size == pdu.transmit_amount) {
        if (manager->get_pdu(this, pdu.buf, pdu.size)) {
            set_pdu(&read_pdu_buffer, nullptr, 0, 0);
        }
    }
    SPDLOG_DEBUG("Connection::read_callback begin to return for fd = {}.", fd);
}

void
Connection::write_callback(int fd) {
    ScopedLock lock(&mutex_for_other);
    VERIFY(not dead);
    VERIFY(socket_fd == fd);
    TransmitBuffer &pdu = write_pdu_buffer;

    if (not write_pdu()) {
        dead = true;
        debug_channel_dead(manager->get_client_or_server(), manager->get_id(), socket_fd);
        AsynEventManager::Instance()->del_callback(manager->get_client_or_server(), manager->get_id(), socket_fd, RDWR);
        // either dead or complete so need to signal
        pthread_cond_signal(&send_complete);
        debug_signal_send_complete_in_case_of_dead(manager->get_client_or_server(), manager->get_id(), socket_fd);
    }
    else {
        VERIFY(pdu.transmit_amount >= 0);
        if (pdu.transmit_amount < pdu.size) return;
        VERIFY(pdu.transmit_amount == pdu.size);
        AsynEventManager::Instance()->del_callback(manager->get_client_or_server(), manager->get_id(), socket_fd, WRONLY);
        // either dead or complete so need to signal
        pthread_cond_signal(&send_complete);
        debug_signal_send_complete_in_case_of_complete(manager->get_client_or_server(), manager->get_id(), socket_fd, pdu.transmit_amount, pdu.size);
    }
}

void
Connection::close_connection() {
    {
        ScopedLock lock(&mutex_for_other);
        if (not dead) {
            dead = true;
            shutdown(socket_fd, SHUT_RDWR);
        }
        else {
            return;
        }
    }
    AsynEventManager::Instance()->block_remove_fd(manager->get_client_or_server(), manager->get_id(), socket_fd);
}

// private
bool
Connection::read_pdu() {
    TransmitBuffer &pdu = this -> read_pdu_buffer;
    if (0 == pdu.size) {
        uint32_t  net_size;
        uint32_t host_size;
        int n = read(socket_fd, &net_size, sizeof(net_size));

        // end of file
        if (0 == n) {
            debug_read_pdu_end_of_file(manager->get_client_or_server(), manager->get_id(), socket_fd);
            return false;
        }
        if (n < 0) {
            debug_read_pdu_failure(manager->get_client_or_server(), manager->get_id(), socket_fd);
            VERIFY(errno not_eq EAGAIN);
            return false;
        }
        if (n > 0 and (n not_eq sizeof(uint32_t))) {
            debug_read_pdu_short_size_error(manager->get_client_or_server(), manager->get_id(), n);
            return false;
        }
        host_size = ntohl(net_size);
        if (host_size > MAX_PDU) {
            debug_read_pdu_too_large_size_error(manager->get_client_or_server(), manager->get_id(), host_size);
            return false;
        }
        pdu.size = host_size;
        VERIFY(nullptr == pdu.buf);
        pdu.buf = (char *)malloc(host_size + sizeof(host_size));
        VERIFY(pdu.buf);
        bcopy(&net_size, pdu.buf, sizeof(net_size));
        pdu.transmit_amount = sizeof(net_size);
    }

    int n = read(socket_fd, pdu.buf + pdu.transmit_amount, pdu.size - pdu.transmit_amount);
    if (n <= 0) {
        if (errno == EAGAIN) return true;
        if (pdu.buf) {
            free(pdu.buf);
        }
        set_pdu(&read_pdu_buffer, nullptr, 0, 0);
        return false;
    }
    pdu.transmit_amount += n;
    return true;
}

bool
Connection::write_pdu() {
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} Connection::write_pdu start for fd = {}.", get_manager_id(), get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} Connection::write_pdu start for fd = {}.", get_manager_id(), get_socket_fd());
    }
    TransmitBuffer &pdu = this -> write_pdu_buffer;
    VERIFY(pdu.transmit_amount >= 0);
    if (pdu.transmit_amount == pdu.size) return true;

    if (0 == pdu.transmit_amount) {
        uint32_t size = htonl(pdu.size);
        bcopy(&size, pdu.buf, sizeof(size));
    }
    int n = write(socket_fd, pdu.buf + pdu.transmit_amount, pdu.size - pdu.transmit_amount);
    if (n < 0) {
        if (errno not_eq EAGAIN) {
            debug_write_pdu_failure(manager->get_client_or_server(), manager->get_id(), socket_fd);
            pdu.size = 0;
            pdu.transmit_amount = -1;
        }
        if (client_or_server) {
            SPDLOG_DEBUG("Server {} Connection::write_pdu begin to return for fd = {}.", get_manager_id(), get_socket_fd());
        }
        else {
            SPDLOG_DEBUG("Client {} Connection::write_pdu begin to return for fd = {}.", get_manager_id(), get_socket_fd());
        }
        return (errno == EAGAIN);
    }
    pdu.transmit_amount += n;
    if (client_or_server) {
        SPDLOG_DEBUG("Server {} Connection::write_pdu begin to return for fd = {}.", get_manager_id(), get_socket_fd());
    }
    else {
        SPDLOG_DEBUG("Client {} Connection::write_pdu begin to return for fd = {}.", get_manager_id(), get_socket_fd());
    }
    return true;
}

void
Connection::set_pdu(TransmitBuffer *pdu, char *buf, int size, int amount) {
    pdu->buf = buf;
    pdu->size = size;
    pdu->transmit_amount = amount;
}