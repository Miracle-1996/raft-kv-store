#include <spdlog/spdlog.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <map>
#include <rpc/connection.h>
#include <rpc/log.h>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>
#include <rpc/rpc_client.h>
#include <rpc/rpc_const.h>
#include <rpc/rpc_utils.h>
#include <rpc/slock.h>
#include <rpc/transmit.h>

const time_out RPCClient::min_limit = {1000};
const time_out RPCClient::max_limit = {10000};

RPCClient::RPCClient(sockaddr_in addr, bool retry): dst(addr), retry(retry) {
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex_for_channel, NULL));
    VERIFY(0 == pthread_cond_init(&destroy_wait_cond, NULL));

    if (retry) {
        set_rand_seed();
        cid_nonce = random();
    }
    else {
        // special cid_nonce 0 means this client does not require at-most-once logic from the server
        // because it uses tcp and never retries a failed connection
        cid_nonce = 0;
    }

    char *loss_env = getenv("RPC_LOSSY");
    if (loss_env != NULL) {
        lossy_test = atoi(loss_env);
    }

    // xid starts with 1 and latest received reply starts with 0
    xid_reply_window.push_back(0);

    SPDLOG_INFO("Client {} start lossy_test {}.", cid_nonce, lossy_test);
}

// destruction should happen only when no external threads are blocked inside RPCClient
// or will use RPCClient in the future
RPCClient::~RPCClient() {
    debug_client_destructor(cid_nonce, channel ? channel->get_socket_fd() : -1);
    if (channel) {
        channel->close_connection();
        channel->decrease_reference();
    }
    VERIFY(callers.empty());
    VERIFY(0 == pthread_mutex_destroy(&mutex));
    VERIFY(0 == pthread_mutex_destroy(&mutex_for_channel));
}

int
RPCClient::bind(time_out limit) {
    int sid;
    int retval = call_basic(RPCConst::bind, sid, limit, 0);
    if (0 == retval) {
        ScopedLock lock(&mutex);
        sid_nonce = sid;
        bind_done = true;
        SPDLOG_INFO("Client {} call bind() get return value = {}.", cid_nonce, sid_nonce);
    }
    else {
        debug_client_bind_failure(cid_nonce, inet_ntoa(dst.sin_addr), retval);
    }
    return retval;
}

int
RPCClient::call_main(unsigned int pid, Marshall &request, Unmarshall &reply, time_out limit) {
    SPDLOG_DEBUG("RPCClient::call_main() start for pid = {}.", pid);
    if (not reachable) {
        return RPCConst::unreachable_failure;
    }

    int xid_reply = -1;
    caller call(0, &reply);
    {
        ScopedLock lock(&mutex);
        if ((pid != RPCConst::bind and not bind_done) or (pid == RPCConst::bind and bind_done)) {
            debug_client_bind_error(cid_nonce);
            return RPCConst::bind_failure;
        }

        if (destroy_wait) {
            return RPCConst::cancel_failure;
        }

        call.xid = xid++;
        callers[call.xid] = &call;

        request_header header(call.xid, pid, cid_nonce, sid_nonce, xid_reply_window.front());
        request.pack_request_header(header);
        xid_reply = xid_reply_window.front();
    }

    time_out current;
    struct timespec now, next_deadline, final_deadline;
    clock_gettime(CLOCK_REALTIME, &now);
    add_timespec(now, limit.time_out_limit, &final_deadline);
    current.time_out_limit = min_limit.time_out_limit;

    bool transmit = true;
    Connection *c = NULL;
    while (1) {
        if (transmit) {
            get_connection(&c);
            if (c) {
                if (reachable) {
                    struct request forget;
                    {
                        ScopedLock lock(&mutex);
                        if (duplicate_request.is_valid() and xid_reply_done > duplicate_request.xid) {
                            forget = duplicate_request;
                            duplicate_request.clear();
                        }
                    }
                    if (forget.is_valid()) {
                        SPDLOG_INFO("Client {} begin to call transmit() to send request for pid = {} xid = {}.", cid_nonce, pid, call.xid);
                        c->transmit((char *)forget.buf.c_str(), forget.buf.size());
                    }
                    SPDLOG_INFO("Client {} begin to call transmit() to send request for pid = {} xid = {}.", cid_nonce, pid, call.xid);
                    c->transmit(request.get_buf(), request.get_index());
                }
                else {
                    SPDLOG_WARN("Client {} can not reachable!", cid_nonce);
                }
            }
            // only send once on a given channel
            transmit = false;
        }

        if (0 == final_deadline.tv_sec and 0 == final_deadline.tv_nsec) {
            break;
        }
        clock_gettime(CLOCK_REALTIME, &now);
        add_timespec(now, current.time_out_limit, &next_deadline);
        if (compare_timespec(next_deadline, final_deadline) > 0) {
            next_deadline = final_deadline;
            final_deadline.tv_sec  = 0;
            final_deadline.tv_nsec = 0;
        }

        {
            ScopedLock call_mutex_lock(&call.mutex);
            while (not call.done) {
                debug_client_call_main_wait(cid_nonce);
                if (ETIMEDOUT == pthread_cond_timedwait(&call.cond, &call.mutex, &next_deadline)) {
                    debug_client_call_main_timeout(cid_nonce);
                    break;
                }
            }
            if (call.done) {
                debug_client_call_main_receive_reply(cid_nonce);
                break;
            }
        }

        if (retry and (NULL == c or c->check_dead())) {
            // since connection is dead, retransmit on the new connection
            transmit = true;
        }
        current.time_out_limit *= 2;
    }

    {
        // only this thread changes call.xid so no need for locking of call.mutex
        ScopedLock lock(&mutex);
        callers.erase(call.xid);
        // question
        // may need to update the xid again here, in case the packet times out before it's even sent by the channel.
        update_xid_reply(call.xid);
        if (destroy_wait) {
            VERIFY(0 == pthread_cond_signal(&destroy_wait_cond));
        }
    }

    if (call.done and lossy_test) {
        ScopedLock lock(&mutex);
        if (not duplicate_request.is_valid()) {
            duplicate_request.buf.assign(request.get_buf(), request.get_index());
            duplicate_request.xid = call.xid;
        }
        if (xid_reply > xid_reply_done) {
            xid_reply_done = xid_reply;
        }
    }

    ScopedLock call_mutex_lock(&call.mutex);
    debug_client_call_main_done(cid_nonce, pid, call.xid, inet_ntoa(dst.sin_addr), ntohs(dst.sin_port), call.done, call.retval);
    if (c) {
        c->decrease_reference();
    }
    // destruction of request automatically frees its buffer
    SPDLOG_DEBUG("RPCClient::call_main() begin to return for pid = {}.", pid);
    return (call.done ? call.retval : RPCConst::timeout_failure);
}

void
RPCClient::cancel() {
    ScopedLock mutex_lock(&mutex);
    debug_client_cancel(cid_nonce);
    for (auto iter = callers.begin(); iter != callers.end(); ++iter) {
        caller *call = iter->second;
        debug_client_force_caller_failure(cid_nonce, call->xid);
        {
            ScopedLock caller_mutex_lock(&call->mutex);
            call->done = true;
            call->retval = RPCConst::cancel_failure;
            VERIFY(0 == pthread_cond_signal(&call->cond));
        }
    }
    while (not callers.empty()) {
        destroy_wait = true;
        VERIFY(0 == pthread_cond_wait(&destroy_wait_cond, &mutex));
    }
    debug_client_cancel_done(cid_nonce);
}

Connection *
RPCClient::connect_to_destination(const sockaddr_in &dst, TransmitManager* manager, int lossy) {
    SPDLOG_DEBUG("Client {} RPCClient::connect_to_destination() begin.", cid_nonce);
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    int val = 1;
    setsockopt(socket_fd, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val));
    if (-1 == connect(socket_fd, (sockaddr*)&dst, sizeof(dst))) {
        debug_client_connect_to_server_failure(cid_nonce, inet_ntoa(dst.sin_addr), (int) ntohs(dst.sin_port));
        close(socket_fd);
        return nullptr;
    }
    SPDLOG_INFO("Client {} connect to Server {}:{} socket fd = {}.", cid_nonce, inet_ntoa(dst.sin_addr), (int)ntohs(dst.sin_port), socket_fd);
    Connection *p = new Connection(manager, socket_fd, lossy);
    SPDLOG_DEBUG("Client {} RPCClient::connect_to_destination() end.", cid_nonce);
    return p;
}

unsigned int
RPCClient::get_id() const {
    return cid_nonce;
}

bool
RPCClient::get_client_or_server() const {
    return client_or_server;
}

void
RPCClient::get_connection(Connection **c) {
    ScopedLock lock(&mutex_for_channel);
    SPDLOG_INFO("Client {} RPCClient::get_connection() start.", cid_nonce);
    if (!channel or channel->check_dead()) {
        if (channel) {
            channel->decrease_reference();
        }
        channel = connect_to_destination(dst, this, lossy_test);
    }
    if (c and channel) {
        if (*c) {
            (*c)->decrease_reference();
        }
        *c = channel;
        (*c)->increase_reference();
    }
    SPDLOG_INFO("Client {} RPCClient::get_connection() end.", cid_nonce);
}

// AsynEventManager thread is being used to make this upcall from connection object to RPCClient.
// This function must not block!
// This function keeps no reference for Connection *c.
bool RPCClient::get_pdu(Connection *c, char *buf, int size) {
    SPDLOG_DEBUG("Client {} RPCClient::get_pdu start for fd = {}", c->get_manager_id(), c->get_socket_fd());
    Unmarshall reply(buf, size);
    reply_header header;
    reply.unpack_reply_header(&header);
    if (not reply.check_ok()) {
        debug_client_get_pdu_unmarshall_failure(cid_nonce);
        SPDLOG_DEBUG("Client {} RPCClient::get_pdu begin to return for fd = {}", c->get_manager_id(), c->get_socket_fd());
        return true;
    }

    ScopedLock lock(&mutex);
    update_xid_reply(header.xid);
    if (callers.find(header.xid) == callers.end()) {
        debug_client_get_pdu_no_pending_request_failure(cid_nonce, header.xid);
        SPDLOG_DEBUG("Client {} RPCClient::get_pdu begin to return for fd = {}", c->get_manager_id(), c->get_socket_fd());
        return true;
    }

    caller *call = callers[header.xid];
    ScopedLock caller_mutex_lock(&call->mutex);
    if (not call->done) {
        call->unmarshall->take_content_from_another(reply);
        call->retval = header.result;
        if (call->retval < 0) {
            debug_client_get_pdu_error(cid_nonce, header.xid, call->retval);
        }
        call->done = 1;
    }
    VERIFY(0 == pthread_cond_broadcast(&call->cond));
    SPDLOG_DEBUG("Client {} RPCClient::get_pdu begin to return for fd = {}", c->get_manager_id(), c->get_socket_fd());
    return true;
}

int
RPCClient::get_count() const {
    return count.load();
}

bool
RPCClient::get_reachable() const {
    return reachable;
}

void
RPCClient::set_reachable(bool reach) {
    reachable = reach;
}

// thread must hold mutex
void
RPCClient::update_xid_reply(unsigned int xid) {
    if (xid <= xid_reply_window.front()) {
        return;
    }

    bool exist = false;
    for (auto iter = xid_reply_window.begin(); iter != xid_reply_window.end(); ++iter) {
        if (*iter > xid) {
            exist = true;
            xid_reply_window.insert(iter, xid);
            break;
        }
    }
    if (false == exist) {
        xid_reply_window.push_back(xid);
    }

    // question
    for (auto iter = xid_reply_window.begin(); iter != xid_reply_window.end(); ++iter) {
        while (xid_reply_window.front() + 1 == *iter) {
            xid_reply_window.pop_front();
        }
    }
}
