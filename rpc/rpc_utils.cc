#include <rpc/marshall.h>
#include <rpc/unmarshall.h>
#include <rpc/rpc_utils.h>
#include <utils/verify.h>

caller::caller(unsigned int xid, Unmarshall *u): xid(xid), unmarshall(u), done(false) {
    VERIFY(0 == pthread_cond_init(&cond, NULL));
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));
}

caller::~caller() {
    VERIFY(0 == pthread_cond_destroy(&cond));
    VERIFY(0 == pthread_mutex_destroy(&mutex));
}

reply_state::reply_state(unsigned int id) {
    buf = nullptr;
    size = 0;
    xid = id;
    callback_valid = false;
}

void
add_timespec(const struct timespec &a, int b, struct timespec *result) {
    result->tv_sec  = a.tv_sec  + b / 1000;
    result->tv_nsec = a.tv_nsec + (b % 1000) * 1000000;
    VERIFY(result->tv_nsec >= 0);
    const int second = 1000000000;
    while (result->tv_nsec > second) {
        result->tv_sec++;
        result->tv_nsec -= second;
    }
}

int
compare_timespec(const struct timespec &a, const struct timespec &b) {
    if (a.tv_sec > b.tv_sec) {
        return 1;
    }
    else if (a.tv_sec < b.tv_sec) {
        return -1;
    }
    else if (a.tv_nsec > b.tv_nsec) {
        return 1;
    }
    else if (a.tv_nsec < b.tv_nsec) {
        return  -1;
    }
    else {
        return 0;
    }
}

int
diff_timespec(const struct timespec &start, const struct timespec &end) {
    int diff = (start.tv_sec < end.tv_sec) ? 1000 * (end.tv_sec - start.tv_sec) : 0;
    VERIFY(diff or start.tv_sec == end.tv_sec);
    if (start.tv_nsec < end.tv_nsec) {
        diff += (end.tv_nsec - start.tv_nsec) / 1000000;
    }
    else {
        diff -= (start.tv_nsec - end.tv_nsec) / 1000000;
    }
    return diff;
}

void set_rand_seed() {
    struct timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    srandom((int)time.tv_nsec^((int)getpid()));
}
