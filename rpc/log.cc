#include <string.h>
#include <rpc/log.h>
#include <spdlog/spdlog.h>

void
debug_client_bind_error(unsigned int cid) {
    RPC_DEBUG(BIND, "client %u has not been bound to dst or binding twice!\n", cid);
}

void
debug_asyn_event_manager_del_callback(bool client_or_server, unsigned int id, int fd, int mask) {
    if (client_or_server) {
        RPC_DEBUG(CALLBACK, "server %u asyn_event_manager call del_callback(fd=%d mask=%d).\n", id, fd, mask);
    }
    else {
        RPC_DEBUG(CALLBACK, "client %u asyn_event_manager call del_callback(fd=%d mask=%d).\n", id, fd, mask);
    }
}

void
debug_client_cancel(unsigned int cid) {
    RPC_DEBUG(CANCEL, "client %u call cancel().\n", cid);
}

void
debug_client_cancel_done(unsigned int cid) {
    RPC_DEBUG(CANCEL, "client %u cancel() done.\n", cid);
}

void
debug_client_force_caller_failure(unsigned int cid, unsigned int xid) {
    RPC_DEBUG(CANCEL, "client %u force call %u failure.\n", cid, xid);
}

void
debug_client_call_main_done(unsigned int cid, unsigned pid, unsigned int xid, char *addr, int port, bool done, int retval) {
    RPC_DEBUG(CLIENT, "client %u call_main() done for sending request[pid %u xid %u] to %s:%d done %d retval %d.\n", cid, pid, xid, addr, port, done, retval);
}

void
debug_client_call_main_receive_reply(unsigned int cid) {
    RPC_DEBUG(CLIENT, "client %u call_main() receive reply.\n", cid);
}

void
debug_client_call_main_wait(unsigned int cid) {
    RPC_DEBUG(CLIENT, "client %u call_main() call pthread_cond_timedwait().\n", cid);
}

void
debug_client_destructor(unsigned int cid, int fd) {
    RPC_DEBUG(CLIENT, "client %u destructor called, channel fd %d.\n", cid, fd);
}

void
debug_channel_dead(bool client_or_server, unsigned int id, int fd) {
    if (client_or_server) {
        RPC_DEBUG(DEAD, "server %u set channel dead fd %d.\n", id, fd);
    }
    else {
        RPC_DEBUG(DEAD, "client %u set channel dead fd %d.\n", id, fd);
    }
}

void
debug_server_dispatch_add_client_to_reply_window(unsigned int sid, unsigned int cid, int xid, int fd, int size) {
    RPC_DEBUG(DISPATCH, "server %u dispatch add new client %u xid %d channel fd %d to reply_window total clients %d.\n", sid, cid, xid, fd, size);
}

void
debug_server_dispatch_rpc(unsigned int sid, int xid, int pid, int xid_reply, unsigned int cid, unsigned int sid_nonce) {
    if (pid == 1) {
        RPC_DEBUG(DISPATCH, "server %u dispatch rpc %d [pid %d, last_reply %d] from client %u\n", sid_nonce, xid, pid, xid_reply, cid);
    }
    else {
        RPC_DEBUG(DISPATCH, "server %u dispatch rpc %d [pid %d, last_reply %d] from client %u\n", sid, xid, pid, xid_reply, cid);
    }
}

void
debug_server_dispatch_send_and_save_reply(unsigned int sid, unsigned int cid, int xid, int pid, int result, int size) {
    RPC_DEBUG(DISPATCH, "server %u dispatch sending and saving reply for cid %u [rpc %d pid %d result %d] of size %d.\n", sid, cid, xid, pid, result, size);
}

void
debug_client_bind_failure(unsigned int cid, char *addr, int retval) {
    RPC_DEBUG(FATAL, "client %u bind %s failure return value %d!\n", cid, addr, retval);
}

void
debug_pthread_create_failure(int error) {
    RPC_DEBUG(FATAL, "call pthread_create() return %d %s.\n", error, strerror(error));
}

void
debug_select_unknown_mask(bool client_or_server, unsigned int id) {
    if (client_or_server) {
        RPC_DEBUG(FATAL, "server %u call with unknown read_write_mask!\n", id);
    }
    else {
        RPC_DEBUG(FATAL, "client %u call with unknown read_write_mask!\n", id);
    }
}

void
debug_select_wait_ready_failure() {
    RPC_DEBUG(FATAL, "event_loop_thread select() errno %d called by Select::wait_ready failure.\n", errno);
}

void
debug_server_call_bind_failure(unsigned int sid, int fd) {
    RPC_DEBUG(FATAL, "server %u try to bind on fd %d failure!\n", sid, fd);
}

void
debug_server_call_get_port_failure(unsigned int sid) {
    RPC_DEBUG(FATAL, "server %u fail to call TCPServerConnection::get_port()!\n", sid);
}

void
debug_server_call_listen_failure(unsigned int sid, int fd) {
    RPC_DEBUG(FATAL, "server %u try to listen on fd %d failure!\n", sid, fd);
}

void
debug_server_call_pipe_failure(unsigned int sid) {
    RPC_DEBUG(FATAL, "server %u call pipe() failure!\n", sid);
}

void
debug_server_call_socket_failure(unsigned int sid) {
    RPC_DEBUG(FATAL, "server %u call socket() failure!\n", sid);
}

void
debug_init_select() {
    RPC_DEBUG(INIT, "init select asio.\n");
}

void
debug_read_pdu_end_of_file(bool client_or_server, unsigned int id, int fd) {
    if (client_or_server) {
        RPC_DEBUG(READPDU, "server %u read_pdu(%d) read EOF.\n", id, fd);
    }
    else {
        RPC_DEBUG(READPDU, "client %u read_pdu(%d) read EOF.\n", id, fd);
    }
}

void
debug_call_select_wait_ready(int max_fd) {
    RPC_DEBUG(SELECT, "event_loop_thread call Select::wait_ready() max_fd %d.\n", max_fd);
}

void
debug_select_wait_ready_interrupt() {
    RPC_DEBUG(SELECT, "event_loop_thread select() return -1 called by Select::wait_ready() errno EINTR.\n");
}

void
debug_select_wait_ready_ok(int retval) {
    RPC_DEBUG(SELECT, "event_loop_thread select() return %d called by Select::wait_ready().\n", retval);
}

void
debug_signal_send_complete_in_case_of_complete(bool client_or_server, unsigned int id, int fd, int amount, int size) {
    if (client_or_server) {
        RPC_DEBUG(SIGNAL, "server %u signal send complete in case of transmit completely on fd %d [amount %d size %d].\n", id, fd, amount, size);
    }
    else {
        RPC_DEBUG(SIGNAL, "client %u signal send complete in case of transmit completely on fd %d [amount %d size %d].\n", id, fd, amount, size);
    }
}

void
debug_signal_send_complete_in_case_of_dead(bool client_or_server, unsigned int id, int fd) {
    if (client_or_server) {
        RPC_DEBUG(SIGNAL, "server %u signal send complete in case of fd %d dead.\n", id, fd);
    }
    else {
        RPC_DEBUG(SIGNAL, "client %u signal send complete in case of fd %d dead.\n", id, fd);
    }
}

void
debug_server_accept_connection_thread_call_select_fail(unsigned int id) {
    RPC_DEBUG(TCP, "server %u accept_connection_thread fail to call select errno %d.\n", id, errno);
}

void
debug_server_do_accept_fail(unsigned int id) {
    RPC_DEBUG(TCP, "server %u do_accept fail to call accept().\n", id);
}

void
debug_shutdown_socket(bool client_or_server, unsigned int id, int socket_fd) {
    if (client_or_server) {
        RPC_DEBUG(TEST, "server %u shutdown socket %d for lossy_test.\n", id, socket_fd);
    }
    else {
        RPC_DEBUG(TEST, "client %u shutdown socket %d for lossy_test.\n", id, socket_fd);
    }
}

void
debug_transmit_done(bool client_or_server, unsigned int id, int fd, int transmit_amount, int size) {
    if (client_or_server) {
        RPC_DEBUG(TRANSMIT, "server %u transmit done on fd %d [amount %d size %d].\n", id, fd, transmit_amount, size);
    }
    else {
        RPC_DEBUG(TRANSMIT, "client %u transmit done on fd %d [amount %d size %d].\n", id, fd, transmit_amount, size);
    }
}

void
debug_transmit_wait_send_complete(bool client_or_server, unsigned int id, int fd, int transmit_amount, int size) {
    if (client_or_server) {
        RPC_DEBUG(WAIT, "server %u wait transmit() send completely on fd %d [amount %d size %d].\n", id, fd, transmit_amount, size);
    }
    else {
        RPC_DEBUG(WAIT, "client %u wait transmit() send completely on fd %d [amount %d size %d].\n", id, fd, transmit_amount, size);
    }
}

void
debug_client_call_main_timeout(unsigned int cid) {
    RPC_DEBUG(WARNING, "client %u call_main() timeout!\n", cid);
}

void
debug_client_connect_to_server_failure(unsigned int cid, char *addr, int port) {
    RPC_DEBUG(WARNING, "client %u connect to server %s:%d failure!\n", cid, addr, port);
}

void
debug_client_get_pdu_error(unsigned int cid, int xid, int retval) {
    RPC_DEBUG(WARNING, "client %u get reply error for xid %d retval %d.\n", cid, xid, retval);
}

void
debug_client_get_pdu_no_pending_request_failure(unsigned int cid, int xid) {
    RPC_DEBUG(WARNING, "client %u can not find xid %d in callers.\n", cid, xid);
}

void
debug_client_get_pdu_unmarshall_failure(unsigned int cid) {
    RPC_DEBUG(WARNING, "client %u unmarshall header failure in get_pdu()!\n", cid);
}

void
debug_client_unmarshall_reply_failure(unsigned int cid) {
    RPC_DEBUG(WARNING, "client %u call call_marshall() to unmarshall the reply failure!\n", cid);
}

void
debug_read_pdu_failure(bool client_or_server, unsigned int id, int fd) {
    if (client_or_server) {
        RPC_DEBUG(WARNING, "server %u read_pdu in fd %d failure errno %d.\n", id, fd, errno);
    }
    else {
        RPC_DEBUG(WARNING, "client %u read_pdu in fd %d failure errno %d.\n", id, fd, errno);
    }
}

void
debug_read_pdu_short_size_error(bool client_or_server, unsigned int id, int size) {
    if (client_or_server) {
        RPC_DEBUG(WARNING, "server %u read_pdu short size %d.\n", id, size);
    }
    else {
        RPC_DEBUG(WARNING, "client %u read_pdu short size %d.\n", id, size);
    }
}

void
debug_read_pdu_too_large_size_error(bool client_or_server, unsigned int id, uint32_t host_size) {
    if (client_or_server) {
        RPC_DEBUG(WARNING, "server %u read_pdu too large size %d.\n", id, host_size);
    }
    else {
        RPC_DEBUG(WARNING, "client %u read_pdu too large size %d.\n", id, host_size);
    }
}

void
debug_server_dispatch_can_not_reachable(unsigned int sid) {
    RPC_DEBUG(WARNING, "server %u dispatch can not reachable!\n", sid);
}

void
debug_server_dispatch_recv_old_request(unsigned int sid, int xid, unsigned cid) {
    RPC_DEBUG(WARNING, "server %u dispatch receive old request %d from %u!\n", sid, xid, cid);
}

void
debug_server_dispatch_send_old_server_sid(unsigned int sid, unsigned int old_sid, int pid) {
    RPC_DEBUG(WARNING, "server %u dispatch receive rpc [pid %d] send to old server %u.\n", sid, pid, old_sid);
}

void
debug_server_dispatch_timeout(unsigned int sid) {
    RPC_DEBUG(WARNING, "server %u dispatch timeout!\n", sid);
}

void
debug_server_dispatch_unknown_procedure(unsigned sid, int pid) {
    RPC_DEBUG(WARNING, "server %u dispatch find unknown procedure %d!\n", sid, pid);
}

void
debug_server_dispatch_unknown_rpc_state(unsigned int sid) {
    RPC_DEBUG(WARNING, "server %u dispatch find unknown rpc state!\n", sid);
}

void
debug_server_dispatch_unmarshall_args_failure(unsigned int sid) {
    RPC_DEBUG(WARNING, "server %u dispatch unmarshall arguments failure!\n", sid);
}

void
debug_server_dispatch_unmarshall_header_failure(unsigned int sid) {
    RPC_DEBUG(WARNING, "server %u dispatch unmarshall header failure!\n", sid);
}

void
debug_transmit_add_callback(bool client_or_server, unsigned int id) {
    if (client_or_server) {
        RPC_DEBUG(WARNING, "server %u transmit() add_callback in order to call write_pdu() once more.\n", id);
    }
    else {
        RPC_DEBUG(WARNING, "client %u transmit() add_callback in order to call write_pdu() once more.\n", id);
    }
}

void
debug_write_pdu_failure(bool client_or_server, unsigned int id, int fd) {
    if (client_or_server) {
        RPC_DEBUG(WARNING, "server %u write_pdu in fd %d failure errno %d.\n", id, fd, errno);
    }
    else {
        RPC_DEBUG(WARNING, "client %u write_pdu in fd %d failure errno %d.\n", id, fd, errno);
    }
}

void
debug_server_show_reply_window_info(unsigned int sid, int size, int all_reply, int max_reply) {
    RPC_DEBUG(WINDOW, "server %u show reply window info clients %d all_reply %d max client reply %d\n", sid, size, all_reply, max_reply);
}