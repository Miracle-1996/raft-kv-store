#include <spdlog/spdlog.h>
#include <vector>
#include <rpc/asyn_event_manager.h>
#include <rpc/method_thread.h>
#include <rpc/select.h>
#include <rpc/slock.h>
#include <utils/verify.h>

AsynEventManager *AsynEventManager::instance = nullptr;

static pthread_once_t manager_is_initialized = PTHREAD_ONCE_INIT;

void
AsynEventManager::singleton() {
    AsynEventManager::instance = new AsynEventManager();
    SPDLOG_INFO("init asyn_event_manager.");
}

AsynEventManager *AsynEventManager::Instance() {
    pthread_once(&manager_is_initialized, AsynEventManager::singleton);
    return instance;
}

AsynEventManager::AsynEventManager() {
    bzero(callback, MAX_FD * sizeof(AsioCallback *));
    asio = new Select();
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));
    VERIFY(0 == pthread_cond_init(&revise_done_cond, NULL));
    SPDLOG_INFO("Starting event_loop_thread.");
    VERIFY((thread = method_thread0(this, &AsynEventManager::event_loop_thread, false) not_eq 0));
}

AsynEventManager::~AsynEventManager() {
    VERIFY(0);
}

void
AsynEventManager::block_remove_fd(bool client_or_server, unsigned int id, int fd) {
    VERIFY(0 <= fd and fd < MAX_FD);
    ScopedLock lock(&mutex);
    asio->unwatch_fd(client_or_server, id, fd, RDWR);
    wait_revise = true;
    while (wait_revise) {
        VERIFY(0 == pthread_cond_wait(&revise_done_cond, &mutex));
    }
    callback[fd] = nullptr;
}

void
AsynEventManager::del_callback(bool client_or_server, unsigned int id, int fd, read_write_mask mask) {
    VERIFY(0 <= fd and fd < MAX_FD);
    ScopedLock lock(&mutex);
    if (asio->unwatch_fd(client_or_server, id, fd, mask)) {
        callback[fd] = nullptr;
        debug_asyn_event_manager_del_callback(client_or_server, id, fd, (int)mask);
    }
}

void
AsynEventManager::add_callback(bool client_or_server, unsigned int id, int fd, read_write_mask mask, AsioCallback *channel) {
    VERIFY(0 <= fd and fd < MAX_FD);
    ScopedLock lock(&mutex);
    asio->watch_fd(client_or_server, id, fd, mask);
    VERIFY(nullptr == callback[fd] or callback[fd] == channel);
    callback[fd] = channel;
    if (client_or_server) {
        SPDLOG_INFO("Server {} asyn_event_manager call add_callback(fd={} mask={}).", id, fd, (int)mask);
    }
    else {
        SPDLOG_INFO("Client {} asyn_event_manager call add_callback(fd={} mask={}).", id, fd, (int)mask);
    }
}

void
AsynEventManager::event_loop_thread() {
    std::vector<int>  readable;
    std::vector<int> writeable;
    while (1) {
        {
            ScopedLock lock(&mutex);
            if (wait_revise) {
                wait_revise = false;
                VERIFY(0 == pthread_cond_broadcast(&revise_done_cond));
            }
        }
         readable.clear();
        writeable.clear();
        asio->wait_ready(&readable, &writeable);
        if ((0 == readable.size()) and (0 == writeable.size())) continue;

        for (int i = 0; i < (int)readable.size(); ++i) {
            int fd = readable[i];
            if (callback[fd]) {
                if (callback[fd]->client_or_server) {
                    SPDLOG_DEBUG("Server {} begin to call read_callback() for fd = {}.", callback[fd]->id, fd);
                }
                else {
                    SPDLOG_DEBUG("Client {} begin to call read_callback() for fd = {}.", callback[fd]->id, fd);
                }
                callback[fd]->read_callback(fd);
            }
        }
        for (int i = 0; i < (int)writeable.size(); ++i) {
            int fd = writeable[i];
            if (callback[fd]) {
                if (callback[fd]->client_or_server) {
                    SPDLOG_INFO("server {} call write_callback({}).", callback[fd]->id, fd);
                }
                else {
                    SPDLOG_INFO("client {} call write_callback({}).", callback[fd]->id, fd);
                }
                callback[fd]->write_callback(fd);
            }
        }
    }
}