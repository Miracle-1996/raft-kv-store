#ifndef RAFT_KV_STORE_RPC_CONST_H
#define RAFT_KV_STORE_RPC_CONST_H

class RPCConst {
public:
    static const unsigned int bind = 1;
    static const int timeout_failure = -1;
    static const int unmarshall_args_failure = -2;
    static const int at_most_once_failure = -4;
    static const int old_server_failure = -5;
    static const int bind_failure = -6;
    static const int cancel_failure = -7;
    static const int unreachable_failure = -8;
};

#endif //RAFT_KV_STORE_RPC_CONST_H
