#ifndef RAFT_KV_STORE_TCPSERVERCONNECTION_H
#define RAFT_KV_STORE_TCPSERVERCONNECTION_H

#include <map>
#include <pthread.h>
#include "connection.h"
#include "transmit.h"

class TCPServerConnection {
public:
     TCPServerConnection(TransmitManager *manager, int port, int lossy_test);
    ~TCPServerConnection();
    int   get_port();

private:
    int lossy;
    int socket_fd;
    int pipe_for_exit[2];
    pthread_t thread;
    pthread_mutex_t mutex;
    TransmitManager *manager;
    std::map<int, Connection *> connection;

    void do_accept();
    void accept_connection_thread();
};

#endif //RAFT_KV_STORE_TCPSERVERCONNECTION_H
