#ifndef RAFT_KV_STORE_SLOCK_H
#define RAFT_KV_STORE_SLOCK_H

#include <pthread.h>
#include <utils/verify.h>

// RAII
class ScopedLock {
public:
    ScopedLock(pthread_mutex_t *mu): mutex(mu) {
        VERIFY(0 == pthread_mutex_lock(mutex));
    }

    ~ScopedLock() {
        VERIFY(0 == pthread_mutex_unlock(mutex));
    }
private:
    pthread_mutex_t *mutex;
};

#endif //RAFT_KV_STORE_SLOCK_H