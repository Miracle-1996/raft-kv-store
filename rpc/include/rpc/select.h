#ifndef RAFT_KV_STORE_SELECT_H
#define RAFT_KV_STORE_SELECT_H

#include <pthread.h>
#include <sys/select.h>
#include <vector>
#include "asio.h"

class Select : public Asio {
public:
     Select();
    ~Select();

    void watch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask);
    bool unwatch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask);
    bool is_watched(bool client_or_server, unsigned int id, int fd, read_write_mask mask);
    void wait_ready(std::vector<int> *readable, std::vector<int> *writeable);

private:
    int max_fd = 0;
    int pipe_fd_for_sync[2];
    fd_set read_fds;
    fd_set write_fds;
    pthread_mutex_t mutex;
};

#endif //RAFT_KV_STORE_SELECT_H
