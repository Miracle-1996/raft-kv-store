#ifndef RAFT_KV_STORE_RPC_CLIENT_H
#define RAFT_KV_STORE_RPC_CLIENT_H

#include <spdlog/spdlog.h>
#include <atomic>
#include <list>
#include <map>
#include <netinet/in.h>
#include <utils/verify.h>
#include "connection.h"
#include "log.h"
#include "rpc_utils.h"
#include "transmit.h"

class RPCClient : public TransmitManager {
public:
    static const time_out max_limit;
    static const time_out min_limit;

     RPCClient(sockaddr_in addr, bool retry = true);
    ~RPCClient();

    int bind(time_out = max_limit);

    int call_main(unsigned int, Marshall &, Unmarshall &, time_out);

    void cancel();

    Connection *connect_to_destination(const sockaddr_in &, TransmitManager*, int);

    unsigned int get_id() const;

    bool get_client_or_server() const;

    void get_connection(Connection **);

    int get_count() const;

    bool get_pdu(Connection *, char *, int);

    bool get_reachable() const;

    void set_reachable(bool);

    void update_xid_reply(unsigned int);

    template<class R, class...Args>
    int call_basic(unsigned int, R &, time_out, const Args&...);

    template<class R>
    int call_marshall(unsigned int, Marshall &, R &, time_out);

private:
    std::atomic_int count = 0;
    sockaddr_in dst;
    int lossy_test = 0;
    int xid_reply_done = -1;
    unsigned int xid = 1;
    unsigned int cid_nonce;
    unsigned int sid_nonce = 0;
    bool bind_done = false;
    bool destroy_wait = false;
    bool reachable = true;
    bool retry;
    bool client_or_server = 0;
    Connection *channel = NULL;
    pthread_cond_t destroy_wait_cond;
    pthread_mutex_t mutex;
    pthread_mutex_t mutex_for_channel;
    request duplicate_request;
    std::map<int, caller *> callers;
    std::list<unsigned int> xid_reply_window;
};

template<class R, class...Args>
int
RPCClient::call_basic(unsigned int pid, R & r, time_out limit, const Args&... args) {
    SPDLOG_DEBUG("Client {} RPCClient::call_basic() start for pid = {}.", cid_nonce, pid);
    Marshall marshall;
    (marshall << ... << args);
    SPDLOG_DEBUG("Client {} begin to call call_marshall() for pid = {}.", cid_nonce, pid);
    int retval = call_marshall(pid, marshall, r, limit);
    SPDLOG_DEBUG("Client {} RPCClient::call_basic() begin to return for pid = {}.", cid_nonce, pid);
    return retval;
}

template<class R>
int
RPCClient::call_marshall(unsigned int pid, Marshall & request, R & r, time_out limit) {
    SPDLOG_DEBUG("RPCClient::call_marshall start for pid = {}.", pid);
    Unmarshall reply;
    count.fetch_add(1);
    SPDLOG_DEBUG("Client {} begin to call call_main() for pid = {}.", cid_nonce, pid);
    int retval = call_main(pid, request, reply, limit);
    if (retval < 0) {
        SPDLOG_DEBUG("RPCClient::call_marshall begin to return for pid = {}.", pid);
        return retval;
    }
    reply >> r;
    if (reply.done() != true) {
        debug_client_unmarshall_reply_failure(cid_nonce);
    }
    SPDLOG_DEBUG("RPCClient::call_marshall begin to return for pid = {}.", pid);
    return retval;
}

#endif //RAFT_KV_STORE_RPC_CLIENT_H
