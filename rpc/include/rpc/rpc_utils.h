#ifndef RAFT_KV_STORE_RPC_UTILS_H
#define RAFT_KV_STORE_RPC_UTILS_H

#include <unistd.h>
#include <pthread.h>
#include <string>
#include <sys/socket.h>
#include <utils/verify.h>
#include "connection.h"
#include "unmarshall.h"

struct caller {
     caller(unsigned int, Unmarshall *);
    ~caller();

    int retval;
    unsigned int xid;
    Unmarshall *unmarshall;
    bool done;
    pthread_cond_t  cond;
    pthread_mutex_t mutex;
};

struct dispatch_task_struct {
    dispatch_task_struct(Connection *c, char *b, int size): connection(c), buf(b), size(size) {}

    Connection *connection;
    char *buf;
    int size;
};

struct request {
    int xid;
    std::string buf;

    request() {
        clear();
    }

    void clear() {
        xid = -1;
        buf.clear();
    }

    bool is_valid() {
        return xid != -1;
    }
};

// state about an in-progress or completed RPC, for at-most-once.
// callback_done is true means that the RPC is complete and a reply has been sent;
// in that case buf points to a copy of the reply, and size holds the size of the reply.
struct reply_state {
    reply_state(unsigned int);

    int size;
    char *buf;
    unsigned int xid;
    bool callback_valid; // whether the reply buffer is valid
};

struct time_out {
    time_out() {}

    time_out(int limit) {
        time_out_limit = limit;
    }

    int time_out_limit;
};

void add_timespec(const struct timespec &, int, struct timespec *);

int compare_timespec(const struct timespec &, const struct timespec &);

int diff_timespec(const struct timespec &, const struct timespec &);

void set_rand_seed();

#endif //RAFT_KV_STORE_RPC_UTILS_H
