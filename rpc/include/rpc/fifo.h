#ifndef RAFT_KV_STORE_FIFO_H
#define RAFT_KV_STORE_FIFO_H

#include <cstddef>
#include <list>
#include <pthread.h>
#include <utils/verify.h>
#include "slock.h"

template<class T>
class FIFO {
public:
     FIFO(int size = 0);
    ~FIFO();
    void clear();
    // blocks enqueue() and dequeue() when queue is full or empty
    unsigned int size();
    bool enqueue(T, bool blocking = true);
    void dequeue(T*);
private:
    std::list<T> queue;
    unsigned int max_size; // maximum capacity of the queue, block enqueue threads if exceeds this limit
    pthread_mutex_t mutex;
    pthread_cond_t non_empty_cond; // queue transfer from empty state to non-empty state
    pthread_cond_t has_space_cond; // queue transfer from overfull state to non-overfull state
};

template <class T>
FIFO<T>::FIFO(int limit): max_size(limit)
{
    VERIFY(0 == pthread_mutex_init(&mutex, NULL));
    VERIFY(0 == pthread_cond_init(&non_empty_cond, NULL));
    VERIFY(0 == pthread_cond_init(&has_space_cond, NULL));
}

template <class T>
FIFO<T>::~FIFO()
{
    // FIFO is to be deleted only when no threads are using it!!!
    VERIFY(0 == pthread_mutex_destroy(&mutex));
    VERIFY(0 == pthread_cond_destroy(&non_empty_cond));
    VERIFY(0 == pthread_cond_destroy(&has_space_cond));
}

template <class T>
void
FIFO<T>::clear() {
    ScopedLock lock(&mutex);
    queue.clear();
    if (max_size and queue.size() < max_size) {
        VERIFY(0 == pthread_cond_signal(&has_space_cond));
    }
}

template <class T>
unsigned int FIFO<T>::size() {
    ScopedLock lock(&mutex);
    return queue.size();
}

template <class T>
bool FIFO<T>::enqueue(T item, bool can_block) {
    ScopedLock lock(&mutex);
    while (1) {
        if (0 == max_size or queue.size() < max_size) {
            queue.push_back(item);
            break;
        }
        if (can_block) {
            VERIFY(0 == pthread_cond_wait(&has_space_cond, &mutex));
        }
        else {
            return false;
        }
    }
    VERIFY(0 == pthread_cond_signal(&non_empty_cond));
    return true;
}

template <class T>
void FIFO<T>::dequeue(T *item) {
    ScopedLock lock(&mutex);
    while (1) {
        if (queue.empty()) {
            VERIFY(0 == pthread_cond_wait(&non_empty_cond, &mutex));
        }
        else {
            *item = queue.front();
            queue.pop_front();
            if (max_size && queue.size() < max_size) {
                VERIFY(0 == pthread_cond_signal(&has_space_cond));
            }
            break;
        }
    }
    return;
}

#endif //RAFT_KV_STORE_FIFO_H