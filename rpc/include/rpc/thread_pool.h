#ifndef RAFT_KV_STORE_THREAD_POOL_H
#define RAFT_KV_STORE_THREAD_POOL_H

#include <pthread.h>
#include <vector>
#include "class_method_wrapper.h"
#include "fifo.h"

class ThreadPool {
public:
    struct thread_task {
        void *(*start_routine)(void *) = nullptr;
        void *arg = nullptr;
    };

     ThreadPool(int size, bool blocking = true);
     ThreadPool(unsigned int sid, int size, bool blocking = true);
    ~ThreadPool();

     void destroy();

     template <class T, class Arg>
     bool add_object_task(T *object, void (T::*func)(Arg), Arg a);

     template <class T, class Arg0, class Arg1>
     bool add_object_task(T *object, void (T::*func)(Arg0, Arg1), Arg0 a0, Arg1 a1);

     bool get_thread_task(thread_task *task);
private:
    // if blocking, then add_thread_job() blocks when queue is full
    // otherwise, add_thread_job() simply returns false when queue is full
    unsigned int sid;
    int nthreads;
    bool block_add_thread;
    bool stopped = false;
    pthread_attr_t attr;
    FIFO<thread_task> tasks;
    std::vector<pthread_t> thread_list;
    bool add_thread_task(void *(*start_routine)(void *), void *arg);
};

template <class T, class Arg>
bool
ThreadPool::add_object_task(T *object, void (T::*fptr)(Arg), Arg arg) {
    ClassMethodWrapper1<T, Arg> *ptr = new ClassMethodWrapper1<T, Arg>(object, fptr, arg);
    return ThreadPool::add_thread_task(&ClassMethodWrapper1<T, Arg>::thread_task_start_routine, (void *)ptr);
}

template <class T, class Arg0, class Arg1>
bool
ThreadPool::add_object_task(T *object, void (T::*fptr)(Arg0, Arg1), Arg0 arg0, Arg1 arg1) {
    ClassMethodWrapper2<T, Arg0, Arg1> *ptr = new ClassMethodWrapper2<T, Arg0, Arg1>(object, fptr, arg0, arg1);
    return ThreadPool::add_thread_task(&ClassMethodWrapper2<T, Arg0, Arg1>::thread_task_start_routine, (void *)ptr);
}

#endif //RAFT_KV_STORE_THREAD_POOL_H