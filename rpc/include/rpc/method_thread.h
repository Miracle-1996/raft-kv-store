#ifndef RAFT_KV_STORE_METHOD_THREAD_H
#define RAFT_KV_STORE_METHOD_THREAD_H

#include <string.h>
#include <pthread.h>
#include <utils/verify.h>
#include "log.h"
#include "method_thread_wrapper.h"

// start a thread that runs an object method
static pthread_t
method_thread_parent(void *(*start_routine)(void *), void *arg, bool detach) {
    pthread_t thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 100 * 1024); // set stack size to 100KB
    int error = pthread_create(&thread, &attr, start_routine, arg);
    pthread_attr_destroy(&attr);
    if (error != 0) {
        debug_pthread_create_failure(error);
    }
    if (detach) {
        VERIFY(0 == pthread_detach(thread));
    }
    return thread;
}

static void
method_thread_child() {
    int oldtype;
    int oldstate;
    VERIFY(0 == pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype));
    VERIFY(0 == pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldstate));
}

template <class T>
pthread_t
method_thread0(T *object, void (T::*fptr)(), bool detach) {
    MethodThreadWrapper0<T> *ptr = new MethodThreadWrapper0<T>(object, fptr);
    return method_thread_parent(&MethodThreadWrapper0<T>::method_thread_start_routine, (void *)ptr, detach);
}

template <class T, class Arg1>
pthread_t
method_thread1(T *object, void (T::*fptr)(Arg1), Arg1 a1, bool detach) {
    MethodThreadWrapper1<T, Arg1> *ptr = new MethodThreadWrapper1<T, Arg1>(object, fptr, a1);
    return method_thread_parent(&MethodThreadWrapper1<T, Arg1>::method_thread_start_routine, (void *)ptr, detach);
}

template <class T, class Arg1, class Arg2>
pthread_t
method_thread2(T *object, void (T::*fptr)(Arg1, Arg2), Arg1 a1, Arg2 a2, bool detach) {
    MethodThreadWrapper2<T, Arg1, Arg2> *ptr = new MethodThreadWrapper2<T, Arg1, Arg2>(object, fptr, a1, a2);
    return method_thread_parent(&MethodThreadWrapper2<T, Arg1, Arg2>::method_thread_start_routine, (void *)ptr, detach);
}

template <class T, class Arg1, class Arg2, class Arg3>
pthread_t
method_thread3(T *object, void (T::*fptr)(Arg1, Arg2, Arg3), Arg1 a1, Arg2 a2, Arg3 a3, bool detach) {
    MethodThreadWrapper3<T, Arg1, Arg2, Arg3> *ptr = new MethodThreadWrapper3<T, Arg1, Arg2, Arg3>(object, fptr, a1, a2, a3);
    return method_thread_parent(&MethodThreadWrapper3<T, Arg1, Arg2, Arg3>::method_thread_start_routine, (void *)ptr, detach);
}

#endif //RAFT_KV_STORE_METHOD_THREAD_H
