#ifndef RAFT_KV_STORE_TRANSMIT_H
#define RAFT_KV_STORE_TRANSMIT_H

#include <cstdlib>

class TransmitBuffer {
public:
    char *buf = nullptr;
    int size = 0;
    int transmit_amount = 0;

     TransmitBuffer() {}
    ~TransmitBuffer() {
         free(buf);
     }
};

class Connection;

class TransmitManager {
public:
    virtual bool get_pdu(Connection *c, char *buf, int size) = 0;
    virtual bool get_client_or_server() const = 0;
    virtual unsigned get_id() const = 0;
    virtual ~TransmitManager() {}
};

#endif //RAFT_KV_STORE_TRANSMIT_H
