#ifndef RAFT_KV_STORE_ASIO_H
#define RAFT_KV_STORE_ASIO_H

#include <vector>

enum read_write_mask { NONE = 0x00, RDONLY = 0x01, WRONLY = 0x10, RDWR = 0x11, MASK = ~0x11 };

class Asio {
public:
    virtual void   watch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask) = 0;
    virtual bool unwatch_fd(bool client_or_server, unsigned int id, int fd, read_write_mask mask) = 0;
    virtual bool is_watched(bool client_or_server, unsigned int id, int fd, read_write_mask mask) = 0;
    virtual void wait_ready(std::vector<int> *readable, std::vector<int> *writeable) = 0;
    virtual ~Asio() {}
};

class AsioCallback {
public:
    unsigned int id;
    bool client_or_server;
    virtual void  read_callback(int fd) = 0;
    virtual void write_callback(int fd) = 0;
    virtual ~AsioCallback() {}
};

#endif //RAFT_KV_STORE_ASIO_H
