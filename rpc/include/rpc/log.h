#ifndef RAFT_KV_STORE_LOG_H
#define RAFT_KV_STORE_LOG_H

#include <chrono>
#include <cstdio>
#include <string>
#include <cstdlib>

enum RPC_TOPIC {
    FATAL = 0,
    WARNING,
    SERVER,
    CLIENT,
    TEST,
    REGISTER,
    TCP,
    BIND,
    THREAD,
    SELECT,
    INIT,
    TRANSMIT,
    CANCEL,
    DISPATCH,
    WINDOW,
    CALLBACK,
    READPDU,
    DEAD,
    WAIT,
    SIGNAL,
};

const std::string rpc_topic_string[] = {
        "FATAL",
        "WARNING",
        "SERVER",
        "CLIENT",
        "TEST",
        "REGISTER",
        "TCP",
        "BIND",
        "THREAD",
        "SELECT",
        "INIT",
        "TRANSMIT",
        "CANCEL",
        "DISPATCH",
        "WINDOW",
        "CALLBACK",
        "READPDU",
        "DEAD",
        "WAIT",
        "SIGNAL"
};

#define RPC_LOG(topic, fmt, args...) \
do {\
    auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();\
    printf("%ld %-10s " fmt, now, rpc_topic_string[topic].c_str(), ##args);\
}   while(0)

#define RPC_DEBUG(topic, fmt, args...) \
do {\
    if (ENABLE_RPC_DEBUG == std::string("ON")) {\
        RPC_LOG(topic, fmt, ##args);\
    }\
}   while(0)

/******************************************************************

                        Debug functions

*******************************************************************/

void debug_asyn_event_manager_add_callback(bool client_or_server, unsigned int id, int fd, int mask);

void debug_asyn_event_manager_del_callback(bool client_or_server, unsigned int id, int fd, int mask);

void debug_call_select_wait_ready(int max_fd);

void debug_channel_dead(bool client_or_server, unsigned int id, int fd);

void debug_client_bind(unsigned int cid, unsigned int sid);

void debug_client_bind_error(unsigned int cid);

void debug_client_bind_failure(unsigned int cid, char *addr, int retval);

void debug_client_call_basic(unsigned int cid);

void debug_client_call_main(unsigned int cid);

void debug_client_call_main_done(unsigned int cid, unsigned pid, unsigned int xid, char *addr, int port, bool done, int retval);

void debug_client_call_main_receive_reply(unsigned int cid);

void debug_client_call_main_timeout(unsigned int cid);

void debug_client_call_main_wait(unsigned int cid);

void debug_client_call_marshall(unsigned int cid);

void debug_client_can_not_reachable(unsigned int cid);

void debug_client_cancel(unsigned int cid);

void debug_client_cancel_done(unsigned int cid);

void debug_client_connect_to_destination(unsigned int cid);

void debug_client_connect_to_server_failure(unsigned int cid, char *addr, int port);

void debug_client_connect_to_server_ok(unsigned int cid, char *addr, int port, int fd);

void debug_client_destructor(unsigned int cid, int fd);

void debug_client_force_caller_failure(unsigned int cid, unsigned int xid);

void debug_client_get_connection(unsigned int cid);

void debug_client_get_pdu_error(unsigned int cid, int xid, int retval);

void debug_client_get_pdu_no_pending_request_failure(unsigned int cid, int xid);

void debug_client_get_pdu_unmarshall_failure(unsigned int cid);

void debug_client_transmit_request(unsigned int cid, unsigned pid, unsigned int xid);

void debug_client_unmarshall_reply_failure(unsigned int cid);

void debug_event_loop_thread_call_read_callback(bool client_or_server, unsigned int id, int fd);

void debug_event_loop_thread_call_write_callback(bool client_or_server, unsigned int id, int fd);

void debug_init_asyn_event_manager();

void debug_init_select();

void debug_pthread_create_failure(int error);

void debug_read_pdu_end_of_file(bool client_or_server, unsigned int id, int fd);

void debug_read_pdu_failure(bool client_or_server, unsigned int id, int fd);

void debug_read_pdu_short_size_error(bool client_or_server, unsigned int id, int size);

void debug_read_pdu_too_large_size_error(bool client_or_server, unsigned int id, uint32_t host_size);

void debug_register_service(unsigned int id, int pid);

void debug_select_unknown_mask(bool client_or_server, unsigned int sid);

void debug_select_wait_ready_failure();

void debug_select_wait_ready_interrupt();

void debug_select_wait_ready_ok(int retval);

void debug_server_accept_connection_thread_call_do_accept(unsigned int id, int retval);

void debug_server_accept_connection_thread_call_select_fail(unsigned int id);

void debug_server_accept_connection_thread_close_fd(unsigned int id, int retval, int sockfd, int pipefd);

void debug_server_bind(unsigned int sid);

void debug_server_call_bind_failure(unsigned int sid, int fd);

void debug_server_call_get_port_failure(unsigned int sid);

void debug_server_call_listen_failure(unsigned int sid, int fd);

void debug_server_call_pipe_failure(unsigned int sid);

void debug_server_call_socket_failure(unsigned int sid);

void debug_server_dispatch_add_client_to_reply_window(unsigned int sid, unsigned int cid, int xid, int fd, int size);

void debug_server_dispatch_can_not_reachable(unsigned int sid);

void debug_server_dispatch_recv_old_request(unsigned int sid, int xid, unsigned cid);

void debug_server_dispatch_rpc(unsigned int sid, int xid, int pid, int xid_reply, unsigned int cid, unsigned int sid_nonce);

void debug_server_dispatch_send_and_save_reply(unsigned int sid, unsigned int cid, int xid, int pid, int result, int size);

void debug_server_dispatch_send_old_server_sid(unsigned int sid, unsigned int old_sid, int pid);

void debug_server_dispatch_timeout(unsigned int sid);

void debug_server_dispatch_unknown_procedure(unsigned sid, int pid);

void debug_server_dispatch_unknown_rpc_state(unsigned int sid);

void debug_server_dispatch_unmarshall_args_failure(unsigned int sid);

void debug_server_dispatch_unmarshall_header_failure(unsigned int sid);

void debug_server_do_accept_fail(unsigned int id);

void debug_server_do_accept_garbage_collect(unsigned int id, int fd);

void debug_server_do_accept_ok(unsigned int id, int fd, char *addr, int port);

void debug_server_init_thread_pool(unsigned int id, int size, bool blocking);

void debug_server_listener_port(unsigned int id, int port);

void debug_server_show_reply_window_info(unsigned int sid, int size, int all_reply, int max_reply);

void debug_server_start_accept_connection_thread(unsigned int id);

void debug_shutdown_socket(bool client_or_server, unsigned int id, int socket_fd);

void debug_signal_send_complete_in_case_of_complete(bool client_or_server, unsigned int id, int fd, int amount, int size);

void debug_signal_send_complete_in_case_of_dead(bool client_or_server, unsigned int id, int fd);

void debug_start_client(unsigned int cid_nonce, int lossy_test);

void debug_start_event_loop_thread();

void debug_start_server(unsigned int sid_nonce, int lossy_test);

void debug_transmit_add_callback(bool client_or_server, unsigned int id);

void debug_transmit_done(bool client_or_server, unsigned int id, int fd, int transmit_amount, int size);

void debug_transmit_wait_send_complete(bool client_or_server, unsigned int id, int fd, int transmit_amount, int size);

void debug_write_pdu_failure(bool client_or_server, unsigned int id, int fd);

#endif //RAFT_KV_STORE_LOG_H
