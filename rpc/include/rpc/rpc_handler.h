#ifndef RAFT_KV_STORE_RPC_HANDLER_H
#define RAFT_KV_STORE_RPC_HANDLER_H

#include "marshall.h"
#include "rpc_handler.h"
#include "unmarshall.h"

class Handler {
public:
    Handler() {}
    virtual ~Handler() {}
    virtual int fn(Unmarshall &, Marshall &) = 0;
};

template<class S, class R>
class Handler0 : public Handler {
public:
    Handler0(S* s, int (S::*m)(R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        R r;
        int result = (server->*method)(r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(R &);
};

template<class S, class R, class A1>
class Handler1 : public Handler {
public:
    Handler1(S* s, int (S::*m)(const A1 a1, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        R r;
        args >> a1;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, R &);
};

template<class S, class R, class A1, class A2>
class Handler2 : public Handler {
public:
    Handler2(S* s, int (S::*m)(const A1 a1, const A2 a2, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        R r;
        args >> a1 >> a2;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, R &);
};

template<class S, class R, class A1, class A2, class A3>
class Handler3 : public Handler {
public:
    Handler3(S* s, int (S::*m)(const A1 a1, const A2 a2, const A3 a3, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        A3 a3;
        R r;
        args >> a1 >> a2 >> a3;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, a3, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, const A3, R &);
};

template<class S, class R, class A1, class A2, class A3, class A4>
class Handler4 : public Handler {
public:
    Handler4(S* s, int (S::*m)(const A1 a1, const A2 a2, const A3 a3, const A4 a4, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        A3 a3;
        A4 a4;
        R r;
        args >> a1 >> a2 >> a3 >> a4;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, a3, a4, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, const A3, const A4, R &);
};

template<class S, class R, class A1, class A2, class A3, class A4, class A5>
class Handler5 : public Handler {
public:
    Handler5(S* s, int (S::*m)(const A1 a1, const A2 a2, const A3 a3, const A4 a4, const A5 a5, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        A3 a3;
        A4 a4;
        A5 a5;
        R r;
        args >> a1 >> a2 >> a3 >> a4 >> a5;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, a3, a4, a5, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, const A3, const A4, const A5, R &);
};

template<class S, class R, class A1, class A2, class A3, class A4, class A5, class A6>
class Handler6 : public Handler {
public:
    Handler6(S* s, int (S::*m)(const A1 a1, const A2 a2, const A3 a3, const A4 a4, const A5 a5, const A6 a6, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        A3 a3;
        A4 a4;
        A5 a5;
        A6 a6;
        R r;
        args >> a1 >> a2 >> a3 >> a4 >> a5 >> a6;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, a3, a4, a5, a6, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, const A3, const A4, const A5, const A6, R &);
};

template<class S, class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7>
class Handler7 : public Handler {
public:
    Handler7(S* s, int (S::*m)(const A1 a1, const A2 a2, const A3 a3, const A4 a4, const A5 a5, const A6 a6, const A7 a7, R &r)): server(s), method(m) {}
    int fn(Unmarshall &args, Marshall &retval) {
        A1 a1;
        A2 a2;
        A3 a3;
        A4 a4;
        A5 a5;
        A6 a6;
        A7 a7;
        R r;
        args >> a1 >> a2 >> a3 >> a4 >> a5 >> a6 >> a7;
        if (not args.done()) {
            return RPCConst::unmarshall_args_failure;
        }
        int result = (server->*method)(a1, a2, a3, a4, a5, a6, a7, r);
        retval << r;
        return result;
    }
private:
    S *server;
    int (S::*method)(const A1, const A2, const A3, const A4, const A5, const A6, const A7, R &);
};

#endif //RAFT_KV_STORE_RPC_HANDLER_H
