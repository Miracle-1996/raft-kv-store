#ifndef RAFT_KV_STORE_MARSHALL_H
#define RAFT_KV_STORE_MARSHALL_H

#include <map>
#include <string>
#include <vector>
#include "reply_request.h"

class Marshall {
public:
     Marshall();
    ~Marshall();

    char *get_buf();
    int get_index();
    std::string get_content();
    void pack(int);
    void pack_reply_header(const reply_header &);
    void pack_request_header(const request_header &);
    void raw_byte(unsigned char);
    void raw_bytes(const char *, int);
    void take_buf(char **, int *);

private:
    char *buf;
    int index; // read/write head position
    int capacity;
};

Marshall & operator<<(Marshall &, bool);
Marshall & operator<<(Marshall &, char);
Marshall & operator<<(Marshall &, unsigned char);
Marshall & operator<<(Marshall &, short);
Marshall & operator<<(Marshall &, unsigned short);
Marshall & operator<<(Marshall &, int);
Marshall & operator<<(Marshall &, unsigned int);
Marshall & operator<<(Marshall &, unsigned long long);
Marshall & operator<<(Marshall &, const std::string &);

template <class T>
Marshall & operator<<(Marshall &m, std::vector<T> v) {
    m << (unsigned int)v.size();
    for (unsigned int i = 0; i < v.size(); ++i) {
        m << v[i];
    }
    return m;
}

template <class Key, class Value>
Marshall & operator<<(Marshall &m, const std::map<Key, Value> &kv) {
    m << (unsigned int)kv.size();
    typename std::map<Key, Value>::const_iterator iter;
    for (iter = kv.begin(); iter != kv.end(); ++iter) {
        m << iter->first << iter->second;
    }
    return m;
}

#endif //RAFT_KV_STORE_MARSHALL_H
