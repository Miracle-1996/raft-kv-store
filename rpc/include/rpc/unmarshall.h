#ifndef RAFT_KV_STORE_UNMARSHALL_H
#define RAFT_KV_STORE_UNMARSHALL_H

#include <map>
#include <string>
#include <vector>

class Unmarshall {
public:
     Unmarshall();
     Unmarshall(char *, int);
     Unmarshall(const std::string &);
    ~Unmarshall();

    bool done();
    bool check_ok();
    char *get_buf();
    int get_capacity();
    int get_index();
    unsigned int raw_byte();
    void raw_bytes(std::string &, int);
    void take_buf(char **, int *);
    // take the content which does not exclude an RPC header from a string
    void take_content_from_string(const std::string &);
    // take contents from another Unmarshall object
    void take_content_from_another(Unmarshall &);
    void unpack(int *);
    void unpack_reply_header(reply_header *);
    void unpack_request_header(request_header *);

private:
    char *buf = nullptr;
    int index = 0;
    int capacity = 0;
    bool ok = false;
};

Unmarshall & operator>>(Unmarshall &, bool &);
Unmarshall & operator>>(Unmarshall &, char &);
Unmarshall & operator>>(Unmarshall &, unsigned char &);
Unmarshall & operator>>(Unmarshall &, short &);
Unmarshall & operator>>(Unmarshall &, unsigned short &);
Unmarshall & operator>>(Unmarshall &, int &);
Unmarshall & operator>>(Unmarshall &, unsigned int &);
Unmarshall & operator>>(Unmarshall &, unsigned long long &);
Unmarshall & operator>>(Unmarshall &, std::string &);

template <class T>
Unmarshall & operator>>(Unmarshall &u, std::vector<T> &v) {
    unsigned int n;
    u >> n;
    for (unsigned int i = 0; i < n; ++i) {
        T x;
        u >> x;
        v.push_back(x);
    }
    return u;
}

template <class Key, class Value>
Unmarshall & operator>>(Unmarshall &u, std::map<Key, Value> &kv) {
    unsigned int n;
    u >> n;
    kv.clear();
    for (unsigned int i = 0; i < n; ++i) {
        Key key;
        Value value;
        u >> key >> value;
        kv[key] = value;
    }
    return u;
}

#endif //RAFT_KV_STORE_UNMARSHALL_H
