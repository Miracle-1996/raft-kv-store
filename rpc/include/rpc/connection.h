#ifndef RAFT_KV_STORE_CONNECTION_H
#define RAFT_KV_STORE_CONNECTION_H

#include "asio.h"
#include "transmit.h"

constexpr int MAX_PDU = 10 * 1024 * 1024; // 10MB

class Connection : public AsioCallback {
public:
     Connection(TransmitManager *m, int fd, int lossy_test = 0);
    ~Connection();

    int get_reference();
    int get_socket_fd();
    unsigned int get_manager_id();
    int compare(Connection *another);
    bool check_dead();
    bool transmit(char *buf, int size);
    void  read_callback(int fd);
    void write_callback(int fd);
    void close_connection();
    void increase_reference();
    void decrease_reference();

private:
    bool  read_pdu();
    bool write_pdu();
    void set_pdu(TransmitBuffer *pdu, char *buf, int size, int amount = 0);

    int waiters = 0;
    int reference = 1;
    bool dead = false;
    const int lossy;
    const int socket_fd;
    TransmitBuffer  read_pdu_buffer;
    TransmitBuffer write_pdu_buffer;
    TransmitManager *manager;
    struct timeval build_time;
    pthread_cond_t send_wait;
    pthread_cond_t send_complete;
    pthread_mutex_t mutex_for_other;
    pthread_mutex_t mutex_for_reference;
};

#endif //RAFT_KV_STORE_CONNECTION_H
