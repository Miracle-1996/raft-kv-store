#ifndef RAFT_KV_STORE_RPC_SERVER_H
#define RAFT_KV_STORE_RPC_SERVER_H

#include <spdlog/spdlog.h>
#include <pthread.h>
#include "connection.h"
#include "rpc_handler.h"
#include "rpc_utils.h"
#include "tcp_server_connection.h"
#include "thread_pool.h"
#include "transmit.h"

enum rpc_state {
    NEW,        // new RPC, not a duplicate
    INPROGRESS, // duplicate of an RPC we are still processing
    DONE,       // duplicate of an RPC we already replied to (have reply)
    FORGOTTEN,  // duplicate of an old RPC whose reply we've forgotten
};

class RPCServer : public TransmitManager {
public:
     RPCServer(unsigned int port, int count = 0);
    ~RPCServer();

    int bind(int &);

    rpc_state check_duplicate_and_update(unsigned int, unsigned int, unsigned int, char **, int *);

    bool get_client_or_server() const;

    bool get_pdu(Connection *, char *, int);

    int  get_port() const;

    bool get_reachable() const;

    unsigned int get_id() const;

    void set_reachable(bool);

    void set_reliable(bool);

    void unregister_all();

    template<class S, class R>
    void reg(unsigned int, S *, int (S::*method)(R &));

    template<class S, class R, class A1>
    void reg(unsigned int, S *, int (S::*method)(const A1, R &));

    template<class S, class R, class A1, class A2>
    void reg(unsigned int, S *, int (S::*method)(const A1, const A2, R &));

private:
    void add_reply(unsigned int, unsigned int, char *, int);

    void dispatch_thread(dispatch_task_struct *);

    void free_reply_window();

    void register_service(unsigned int, Handler *);

    void update_status(unsigned int);

    int port;
    int lossy_test = 0;
    int remain_count;
    unsigned int sid_nonce;
    const int refresh_count_value;
    bool reachable = true;
    bool reliable  = true;
    bool client_or_server = 1;
    TCPServerConnection *listener;
    ThreadPool *dispatch_thread_pool;
    std::map<int, int> counts;
    std::map<unsigned int, Connection *> connections;
    // provide at most once semantics by maintaining a window of replies(indexed by cid_nonce) per client
    // that client has not acknowledged receiving yet.
    std::map<int, Handler *> handlers;
    std::map<unsigned int, std::list<reply_state>> reply_window;
    pthread_mutex_t mutex_for_counts;
    pthread_mutex_t mutex_for_handlers;
    pthread_mutex_t mutex_for_connections;
    pthread_mutex_t mutex_for_reply_window;
};

template<class S, class R>
void
RPCServer::reg(unsigned int pid, S *s, int (S::*method)(R & r)) {
    register_service(pid, new Handler0<S, R>(s, method));
    SPDLOG_INFO("Server {} register service {}.", sid_nonce, pid);
}

template<class S, class R, class A1>
void
RPCServer::reg(unsigned int pid, S *s, int (S::*method)(const A1 a1, R & r)) {
    register_service(pid, new Handler1<S, R, A1>(s, method));
    SPDLOG_INFO("Server {} register service {}.", sid_nonce, pid);
}

template<class S, class R, class A1, class A2>
void
RPCServer::reg(unsigned int pid, S *s, int (S::*method)(const A1 a1, const A2 a2, R & r)) {
    register_service(pid, new Handler2<S, R, A1, A2>(s, method));
    SPDLOG_INFO("Server {} register service {}.", sid_nonce, pid);
}

#endif //RAFT_KV_STORE_RPC_SERVER_H
