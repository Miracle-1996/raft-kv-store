#ifndef RAFT_KV_STORE_CLASS_METHOD_WRAPPER_H
#define RAFT_KV_STORE_CLASS_METHOD_WRAPPER_H

template <class T, class Arg>
class ClassMethodWrapper1 {
public:
    T *object;
    void (T::*fptr)(Arg a);
    Arg arg;

    ClassMethodWrapper1(T *object, void (T::*fptr)(Arg), Arg arg): object(object), fptr(fptr), arg(arg) {}

    static void *thread_task_start_routine(void *arg) {
        ClassMethodWrapper1 *p = (ClassMethodWrapper1*)arg;
        (p->object->*p->fptr)(p->arg);
        delete p;
        return nullptr;
    }
};

template <class T, class Arg0, class Arg1>
class ClassMethodWrapper2 {
public:
    T *object;
    void (T::*fptr)(Arg0 a0, Arg1 a1);
    Arg0 arg0;
    Arg1 arg1;

    ClassMethodWrapper2(T *object, void (T::*fptr)(Arg0, Arg1), Arg0 a0, Arg1 a1): object(object), fptr(fptr), arg0(a0), arg1(a1) {}

    static void *thread_task_start_routine(void *arg) {
        ClassMethodWrapper2 *p = (ClassMethodWrapper2*)arg;
        (p->object->*p->fptr)(p->arg0, p->arg1);
        delete p;
        return nullptr;
    }
};

#endif //RAFT_KV_STORE_CLASS_METHOD_WRAPPER_H
