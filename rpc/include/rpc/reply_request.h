#ifndef RAFT_KV_STORE_REPLY_REQUEST_H
#define RAFT_KV_STORE_REPLY_REQUEST_H

#include <cstdint>
#include <utils/algorithm.h>

using rpc_size_t = int;
using rpc_checksum_t = uint64_t;

struct reply_header {
    reply_header(int r = 0, int result = 0): xid(r), result(result) {}

    int xid;
    int result; // rpc reply code
};

struct request_header {
    request_header(int r = 0, int p = 0, int c = 0, int s = 0, int reply = 0): xid(r), procedure(p), cid(c), sid(s), xid_reply(reply) {}

    int xid;
    int procedure;
    unsigned int cid; // client  id
    unsigned int sid; // server  id
    int xid_reply;
};

enum {
    //size of initial buffer allocation
    DEFAULT_RPC_SZ = 1024,
#if RPC_CHECKSUMMING
    //size of rpc_header includes a 4-byte int to be filled by tcp channel and uint64_t checksum
	RPC_HEADER_SZ = static_max<sizeof(request_header), sizeof(reply_header)>::value + sizeof(rpc_size_t) + sizeof(rpc_checksum_t)
#else
    RPC_HEADER_SZ = static_max<sizeof(request_header), sizeof(reply_header)>::value + sizeof(rpc_size_t)
#endif
};

#endif //RAFT_KV_STORE_REPLY_REQUEST_H
