#ifndef RAFT_KV_STORE_METHOD_THREAD_WRAPPER_H
#define RAFT_KV_STORE_METHOD_THREAD_WRAPPER_H

static void method_thread_child();

template <class T>
class MethodThreadWrapper0 {
public:
    T *object;
    void (T::*fptr)();

    MethodThreadWrapper0(T *object, void (T::*fptr)()): object(object), fptr(fptr) {}

    static void *method_thread_start_routine(void *arg) {
        MethodThreadWrapper0 *p = (MethodThreadWrapper0*)arg;
        method_thread_child();
        (p->object->*p->fptr)();
        delete p;
        return nullptr;
    }
};

template <class T, class Arg1>
class MethodThreadWrapper1 {
public:
    T *object;
    void (T::*fptr)(Arg1);
    Arg1 a1;

    MethodThreadWrapper1(T *object, void (T::*fptr)(Arg1), Arg1 a1): object(object), fptr(fptr), a1(a1) {}

    static void *method_thread_start_routine(void *arg) {
        MethodThreadWrapper1 *p = (MethodThreadWrapper1*)arg;
        method_thread_child();
        (p->object->*p->fptr)(p->a1);
        delete p;
        return nullptr;
    }
};

template <class T, class Arg1, class Arg2>
class MethodThreadWrapper2 {
public:
    T *object;
    void (T::*fptr)(Arg1, Arg2);
    Arg1 a1;
    Arg2 a2;

    MethodThreadWrapper2(T *object, void (T::*fptr)(Arg1, Arg2), Arg1 a1, Arg2 a2): object(object), fptr(fptr), a1(a1), a2(a2) {}

    static void *method_thread_start_routine(void *arg) {
        MethodThreadWrapper2 *p = (MethodThreadWrapper2*)arg;
        method_thread_child();
        (p->object->*p->fptr)(p->a1, p->a2);
        delete p;
        return nullptr;
    }
};

template <class T, class Arg1, class Arg2, class Arg3>
class MethodThreadWrapper3 {
public:
    T *object;
    void (T::*fptr)(Arg1, Arg2, Arg3);
    Arg1 a1;
    Arg2 a2;
    Arg3 a3;

    MethodThreadWrapper3(T *object, void (T::*fptr)(Arg1, Arg2, Arg3), Arg1 a1, Arg2 a2, Arg3 a3): object(object), fptr(fptr), a1(a1), a2(a2), a3(a3) {}

    static void *method_thread_start_routine(void *arg) {
        MethodThreadWrapper3 *p = (MethodThreadWrapper3*)arg;
        method_thread_child();
        (p->object->*p->fptr)(p->a1, p->a2, p->a3);
        delete p;
        return nullptr;
    }
};

#endif //RAFT_KV_STORE_METHOD_THREAD_WRAPPER_H
