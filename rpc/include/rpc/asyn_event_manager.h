#ifndef RAFT_KV_STORE_ASYNEVENTMANAGER_H
#define RAFT_KV_STORE_ASYNEVENTMANAGER_H

#include <pthread.h>
#include "asio.h"

constexpr int MAX_FD = 1024;

class AsynEventManager {
public:
    ~AsynEventManager();
    static AsynEventManager *Instance();

    void event_loop_thread();
    void add_callback(bool client_or_server, unsigned int id, int fd, read_write_mask mask, AsioCallback *channel);
    void block_remove_fd(bool client_or_server, unsigned int id, int fd);
    void del_callback(bool client_or_server, unsigned int id, int fd, read_write_mask mask);

private:
    AsynEventManager();
    static void singleton();

    bool wait_revise = false;
    pthread_t thread;
    pthread_mutex_t mutex;
    pthread_cond_t revise_done_cond;

    Asio *asio;
    AsioCallback *callback[MAX_FD];
    static AsynEventManager *instance;
};

#endif //RAFT_KV_STORE_ASYNEVENTMANAGER_H
