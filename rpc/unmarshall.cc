#include <cstdlib>
#include <cstring>
#include <string>
#include <rpc/reply_request.h>
#include <rpc/unmarshall.h>
#include <utils/verify.h>

Unmarshall::Unmarshall() {}

Unmarshall::Unmarshall(char *b, int size): buf(b), capacity(size), ok(true) {
    VERIFY(size >= 0);
}

Unmarshall::Unmarshall(const std::string &str) {
    take_content_from_string(str);
}

Unmarshall::~Unmarshall() {
    if (buf) {
        free(buf);
        buf = nullptr;
    }
}

bool
Unmarshall::check_ok() {
    return ok;
}

bool
Unmarshall::done() {
    bool retval;
    if (check_ok() and (index == capacity)) {
        retval = true;
    }
    else {
        retval = false;
    }
    return retval;
}

char *
Unmarshall::get_buf() {
    return buf;
}

int
Unmarshall::get_capacity() {
    return capacity;
}

int
Unmarshall::get_index() {
    return index;
}

void
Unmarshall::take_buf(char **b, int *size) {
    *b = buf;
    *size = capacity;
    buf = nullptr;
    index = capacity = 0;
}

void
Unmarshall::take_content_from_another(Unmarshall &another) {
    if (buf) {
        free(buf);
    }
    another.take_buf(&buf, &capacity);
    index = RPC_HEADER_SZ;
    ok = (capacity >= RPC_HEADER_SZ) ? true : false;
}

void
Unmarshall::take_content_from_string(const std::string &str) {
    capacity = str.size() + RPC_HEADER_SZ;
    buf = (char *) realloc(buf, capacity);
    VERIFY(buf);
    index = RPC_HEADER_SZ;
    memcpy(buf + index, str.data(), str.size());
    ok = true;
}

void
Unmarshall::unpack(int *x) {
    *x  = (raw_byte() & 0xff) << 24;
    *x |= (raw_byte() & 0xff) << 16;
    *x |= (raw_byte() & 0xff) << 8;
    *x |= (raw_byte() & 0xff);
}

void
Unmarshall::unpack_reply_header(reply_header *header) {
    // the first 4-byte is for channel to fill size of pdu
    index  = sizeof(rpc_size_t);
#if RPC_CHECKSUMMING
    index += sizeof(rpc_checksum_t);
#endif
    unpack(&header->xid);
    unpack(&header->result);
    index = RPC_HEADER_SZ;
}

void
Unmarshall::unpack_request_header(request_header *header) {
    // the first 4-byte is for channel to fill size of pdu
    index = sizeof(rpc_size_t);
#if RPC_CHECKSUMMING
    index += sizeof(rpc_checksum_t);
#endif
    unpack(&header->xid);
    unpack(&header->procedure);
    unpack((int *)&header->cid);
    unpack((int *)&header->sid);
    unpack(&header->xid_reply);
    index = RPC_HEADER_SZ;
}

unsigned int
Unmarshall::raw_byte()
{
    char retval = 0;
    if (index >= capacity) {
        ok = false;
    }
    else {
        retval = buf[index++];
    }
    return (unsigned int)retval;
}

void
Unmarshall::raw_bytes(std::string &str, int n) {
    VERIFY(n >= 0);
    if (index + n > capacity) {
        ok = false;
    }
    else {
        std::string s = std::string(buf + index, n);
        swap(str, s);
        VERIFY((int)str.size() == n);
        index += n;
    }
}

Unmarshall &
operator>>(Unmarshall &u, bool &x) {
    x = (bool)u.raw_byte();
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, char &x) {
    x = (char)u.raw_byte();
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, unsigned char &x) {
    x = (unsigned char)u.raw_byte();
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, short &x) {
    x  = (u.raw_byte() & 0xff) << 8;
    x |= (u.raw_byte() & 0xff);
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, unsigned short &x) {
    x  = (u.raw_byte() & 0xff) << 8;
    x |= (u.raw_byte() & 0xff);
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, int &x) {
    x  = (u.raw_byte() & 0xff) << 24;
    x |= (u.raw_byte() & 0xff) << 16;
    x |= (u.raw_byte() & 0xff) << 8;
    x |= (u.raw_byte() & 0xff);
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, unsigned int &x) {
    x  = (u.raw_byte() & 0xff) << 24;
    x |= (u.raw_byte() & 0xff) << 16;
    x |= (u.raw_byte() & 0xff) << 8;
    x |= (u.raw_byte() & 0xff);
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, unsigned long long &x) {
    unsigned int low;
    unsigned int high;
    u >> high;
    u >> low;
    x = low | ((unsigned long long) high << 32);
    return u;
}

Unmarshall &
operator>>(Unmarshall &u, std::string &str) {
    unsigned int size;
    u >> size;
    if (u.check_ok()) {
        u.raw_bytes(str, size);
    }
    return u;
}
