#include <cstdlib>
#include <cstring>
#include <string>
#include <rpc/marshall.h>
#include <utils/verify.h>

Marshall::Marshall() {
    buf = (char *)malloc(DEFAULT_RPC_SZ * sizeof(char));
    VERIFY(buf);
    index = RPC_HEADER_SZ;
    capacity = DEFAULT_RPC_SZ;
}

Marshall::~Marshall() {
    if (buf) {
        free(buf);
        buf = nullptr;
    }
}

char *
Marshall::get_buf() {
    return buf;
}

int
Marshall::get_index() {
    return index;
}

std::string
Marshall::get_content() {
    return std::string(buf + RPC_HEADER_SZ, index - RPC_HEADER_SZ);
}

void
Marshall::raw_byte(unsigned char x) {
    if (index >= capacity) {
        capacity *= 2;
        VERIFY(buf != nullptr);
        buf = (char *) realloc(buf, capacity);
        VERIFY(buf);
    }
    buf[index++] = x;
}

void
Marshall::raw_bytes(const char *ptr, int n) {
    VERIFY(n >= 0);
    if (index + n > capacity) {
        capacity = (capacity > n) ? 2 * capacity : capacity + n;
        VERIFY(buf != nullptr);
        buf = (char *) realloc(buf, capacity);
        VERIFY(buf);
    }
    memcpy(buf + index, ptr, n);
    index += n;
}

void
Marshall::pack(int x) {
    raw_byte((x >> 24) & 0xff);
    raw_byte((x >> 16) & 0xff);
    raw_byte((x >> 8)  & 0xff);
    raw_byte(x & 0xff);
}

void
Marshall::pack_reply_header(const reply_header &header) {
    // save index because of raw_byte()
    int save_index = index;
    // leave the first 4-byte empty for channel to fill size of pdu
    index = sizeof(rpc_size_t);
#if RPC_CHECKSUMMING
    index += sizeof(rpc_checksum_t);
#endif
    pack(header.xid);
    pack(header.result);
    // restore index from save_index
    index = save_index;
}

void
Marshall::pack_request_header(const request_header &header) {
    // save index because of raw_byte()
    int save_index = index;
    // leave the first 4-byte empty for channel to fill size of pdu
    index = sizeof(rpc_size_t);
#if RPC_CHECKSUMMING
    index += sizeof(rpc_checksum_t);
#endif
    pack(header.xid);
    pack(header.procedure);
    pack((int)header.cid);
    pack((int)header.sid);
    pack(header.xid_reply);
    // restore index from save_index
    index = save_index;
}

void
Marshall::take_buf(char **ptr, int *idx) {
    *ptr = buf;
    *idx = index;
    buf = nullptr;
    index = 0;
}

Marshall &
operator<<(Marshall &m, bool x) {
    m.raw_byte(x);
    return m;
}

Marshall &
operator<<(Marshall &m, char x) {
    m << (unsigned char)x;
    return m;
}

Marshall &
operator<<(Marshall &m, unsigned char x) {
    m.raw_byte(x);
    return m;
}

Marshall &
operator<<(Marshall &m, unsigned short x) {
    m.raw_byte((x >> 8) & 0xff);
    m.raw_byte(x & 0xff);
    return m;
}

Marshall &
operator<<(Marshall &m, short x) {
    m << (unsigned short)x;
    return m;
}

Marshall &
operator<<(Marshall &m, unsigned int x) {
    m.raw_byte((x >> 24) & 0xff);
    m.raw_byte((x >> 16) & 0xff);
    m.raw_byte((x >> 8)  & 0xff);
    m.raw_byte(x & 0xff);
    return m;
}

Marshall &
operator<<(Marshall &m, int x) {
    m << (unsigned int)x;
    return m;
}

Marshall &
operator<<(Marshall &m, unsigned long long x) {
    m << (unsigned int)(x >> 32);
    m << (unsigned int)x;
    return m;
}

Marshall &
operator<<(Marshall &m, const std::string &str) {
    m << (unsigned int)str.size();
    m.raw_bytes(str.data(), str.size());
    return m;
}