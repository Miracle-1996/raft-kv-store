.PHONY: build clean test

build:
	cmake -B build -DENABLE_RPC_DEBUG=OFF
	cmake -B build -DENABLE_RAFT_DEBUG=OFF
	cmake --build build -j $(nproc)

debug:
	cmake -B build -DENABLE_RPC_DEBUG=ON
	cmake -B build -DENABLE_RAFT_DEBUG=ON
	cmake --build build -j $(nproc)

test:
	ctest --test-dir build -R "^test."

clean:
	rm ./build -rf