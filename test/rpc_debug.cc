#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <string>
#include <unistd.h>
#include <rpc/marshall.h>
#include <rpc/reply_request.h>
#include <rpc/rpc_client.h>
#include <rpc/rpc_const.h>
#include <rpc/rpc_server.h>
#include <rpc/rpc_utils.h>
#include <rpc/unmarshall.h>
#include <utils/verify.h>

constexpr int client_number = 1;
int port = 0;
bool client = false;
bool server = false;
pthread_attr_t attr;
struct sockaddr_in dst;
RPCServer *rpc_server = nullptr;
RPCClient *rpc_client[client_number];

class Service {
public:
    int handle_string(const std::string, const std::string, std::string &);
    int handle_fast(const int, int &);
    int handle_slow(const int, int &);
    int handle_big_reply(const int, std::string &);
};

int
Service::handle_string(const std::string a, const std::string b, std::string &r) {
    r = a + b;
    return 0;
}

int
Service::handle_fast(const int a, int &r) {
    r = a + 1;
    return 0;
}

int
Service::handle_slow(const int a, int &r) {
    usleep(random() % 5000);
    r = a + 2;
    return 0;
}

int
Service::handle_big_reply(const int size, std::string &r) {
    r = std::string(size, 'x');
    return 0;
}

Service service;

void show_status() {
    SPDLOG_INFO("port {}", port);
    SPDLOG_INFO("client {}", client ? "true" : "false");
    SPDLOG_INFO("server {}", server ? "true" : "false");
}

void start_server() {
    rpc_server = new RPCServer(port);
    rpc_server->reg(22, &service, &Service::handle_string);
    rpc_server->reg(23, &service, &Service::handle_fast);
    rpc_server->reg(24, &service, &Service::handle_slow);
    rpc_server->reg(25, &service, &Service::handle_big_reply);
}

void *
client_thread1(void *x) {
    int index = (unsigned long)(x) % client_number;
    for (int i = 0; i < 100; ++i) {
        int arg = (random() % 2000);
        std::string reply;
        int retval = rpc_client[index]->call_basic(25, reply, RPCClient::max_limit, arg);
        VERIFY(0 == retval);
        if ((int)reply.size() != arg) {
            SPDLOG_ERROR("reply size error {} != {}", (int)reply.size(), arg);
        }
        VERIFY((int)reply.size() == arg);
    }
    for (int i = 0; i < 100; ++i) {
        int reply;
        int arg = random() % 1000;
        int which_service = random() % 2;

        struct timespec start;
        struct timespec end;
        clock_gettime(CLOCK_REALTIME, &start);
        int retval = rpc_client[index]->call_basic(which_service ? 23 : 24, reply, RPCClient::max_limit, arg);
        clock_gettime(CLOCK_REALTIME, &end);
        int diff = diff_timespec(start, end);
        if (retval != 0) {
            SPDLOG_ERROR("{} ms have elapsed!\n", diff);
        }
        VERIFY(0 == retval);
        VERIFY(reply == (which_service ? arg + 1 : arg + 2));
    }
    return 0;
}

void *
client_thread2(void *x) {
    int index = ((unsigned long)x) %client_number;
    time_t t;
    time(&t);
    while (time(0) - t < 10) {
        std::string reply;
        int arg = random() % 2000;
        int retval = rpc_client[index]->call_basic(25, reply, RPCClient::max_limit, arg);
        if ((int)reply.size() != arg) {
            SPDLOG_ERROR("ask for {} reply get {} retval {}", arg, (int)reply.size(), retval);
        }
        VERIFY((int)reply.size() == arg);
    }
    return 0;
}

void *
client_thread3(void *x) {
    RPCClient *c = (RPCClient *)x;
    for (int i = 0; i < 4; ++i) {
        int reply;
        int retval = c->call_basic(24, reply, time_out(3000), i);
        VERIFY(retval == RPCConst::timeout_failure or reply == i + 2);
    }
    return 0;
}

void
simple_test(RPCClient *c) {
    SPDLOG_INFO("simple test start");
    std::string reply;
    int retval = c->call_basic(22, reply, RPCClient::max_limit, (std::string)"hello", (std::string)" world");
    VERIFY(0 == retval);
    VERIFY("hello world" == reply);
    SPDLOG_INFO("    -- handle_string .. ok");

//    unsigned int size = 100000;
//    retval = c->call_basic(25, reply, time_out(200000), size);
//    VERIFY(0 == retval);
//    VERIFY(reply.size() == size);
//    SPDLOG_INFO("    -- handle_big_reply .. ok");

#if 0
    retval = c->call_basic(22, reply, RPCClient::max_limit, (std::string)"few argument should fail");
    VERIFY(retval < 0);
    SPDLOG_INFO("    -- too few arguments .. failed ok");

    retval = c->call_basic(23, reply, RPCClient::max_limit, 1001, 1002);
    VERIFY(retval < 0);
    SPDLOG_INFO("    -- too many arguments .. failed ok");
#endif
//    int x = 0;
//    int limit = 3000;
//    int value = random() % 5000;
//    retval = c->call_basic(23, x, time_out(limit), value);
//    VERIFY(0 == retval and x == value + 1);
//    SPDLOG_INFO("    -- no spurious timeout1 .. ok");
//
//    unsigned int random_size = 1000 + random() % 1000;
//    std::string arg(random_size, 'x');
//    c->call_basic(22, reply, time_out(limit), arg, (std::string)"x");
//    VERIFY(reply.size() == random_size + 1);
//    SPDLOG_INFO("    -- no spurious timeout2 .. ok");
//
//    unsigned int huge_size = 1000000;
//    std::string huge(huge_size, 'x');
//    retval = c->call_basic(22, reply, RPCClient::max_limit, huge, (std::string)"z");
//    VERIFY(reply.size() == huge_size + 1);
//    SPDLOG_INFO("    -- huge 1M rpc request .. ok");
//
//    struct sockaddr_in sock_addr;
//    memset(&sock_addr, 0, sizeof(sock_addr));
//    sock_addr.sin_family = AF_INET;
//    sock_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
//    VERIFY(port != 7661);
//    sock_addr.sin_port = htons(7661);
//    RPCClient *rc = new RPCClient(sock_addr);
//    time_t t0 = time(0);
//    retval = rc->bind(time_out(3000));
//    time_t t1 = time(0);
//    VERIFY(retval < 0 && (t1 - t0) <= 3);
//    SPDLOG_INFO("    -- rpc timeout .. ok");
    SPDLOG_INFO("simple_test ok");
}

void start_test() {
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 32 * 1024);
    if (server) {
        SPDLOG_INFO("Starting server on port {} RPC_HEADER_SIZE {}", port, RPC_HEADER_SZ);
        start_server();
    }
    if (client) {
        memset(&dst, 0, sizeof(dst));
        dst.sin_family = AF_INET;
        dst.sin_addr.s_addr = inet_addr("127.0.0.1");
        dst.sin_port = htons(port);
        for (int i = 0; i < client_number; ++i) {
            rpc_client[i] = new RPCClient(dst);
            VERIFY(0 == rpc_client[i]->bind());
        }
        simple_test(rpc_client[0]);
    }
    if (client) {
        SPDLOG_INFO("All tests pass!!!");
    }
}

int main(int argc, char *argv[]) {
    spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%f] %^[%l]%$ [%n] [%s:%#] [%!] [%v]");
    auto console = spdlog::stdout_color_mt("console");
    spdlog::set_default_logger(console);
    spdlog::set_level(spdlog::level::trace);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    srandom(getpid());
    port = 25000;
    char c = 0;
    while ((c = getopt(argc, argv, "clp:s")) != -1) {
        switch (c) {
            case 'c':
                client = true;
                break;
            case 'l':
                VERIFY(0 == setenv("RPC_LOSSY", "5", 1));
            case 'p':
                port = atoi(optarg);
                break;
            case 's':
                server = true;
                break;
            default:
                SPDLOG_ERROR("Invalid argument!");
                VERIFY(0);
        }
    }
    if (not client and not server) {
        client = server = true;
    }
    show_status();
    start_test();
    if (server and not client) {
        while (1) sleep(1);
    }
    return 0;
}