#ifndef RAFT_KV_STORE_TEST_UTILS_H
#define RAFT_KV_STORE_TEST_UTILS_H

#include <iostream>
#include <sys/stat.h>
#include <vector>
#include <raft/kv_command.h>
#include <raft/kv_state_machine.h>
#include <raft/raft.h>

/******************************************************************

                       For Test Framework

*******************************************************************/
#define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << ":" << __LINE__ << " msg: " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)

#define TEST_CASE(part, name, msg) \
class test_case_##part##_##name##_ut : public unit_test_case {\
public:\
    test_case_##part##_##name##_ut(const char *_msg) {this->message = _msg;}\
    virtual void body() override;\
};\
static unit_test_case *part##_##name = unit_test_suite::instance()->register_test_case(#part, #name, new test_case_##part##_##name##_ut(msg));\
void test_case_##part##_##name##_ut::body()

class unit_test_case {
public:
    virtual void body() = 0;
    const char *message;
};

class unit_test_suite {
public:
    static unit_test_suite *instance();
    unit_test_case *register_test_case(const char *part, const char *name, unit_test_case *obj);
    bool run(int argc, char *argv[]);
    bool run_all();
    bool run_case(const std::string &part, const std::string &name, unit_test_case *obj);
    bool run_part_case(const std::string &spart, const std::string &sname);
private:
    std::vector<std::pair<std::string, std::vector<std::pair<std::string, unit_test_case*>>>> cases; // (part, name) -> case
    int count;
    int passed;
    bool is_counting;
};

/******************************************************************

                       For Raft Test

*******************************************************************/

void msleep(int ms);

int remove_directory(const char *path);

class list_command : public raft_command {
public:
    list_command() {}
    list_command(int v): value(v) {}
    list_command(const list_command &cmd) { value = cmd.value; }
    virtual ~list_command() {}

    virtual int size() const override;

    virtual void serialize(char *buf, int sz) const override;

    virtual void deserialize(const char *buf, int sz) override;

    int value;
};

Marshall   &operator<<(Marshall &m, const list_command &cmd);
Unmarshall &operator>>(Unmarshall &u, list_command &cmd);

class list_state_machine : public raft_state_machine {
public:
    list_state_machine();
    virtual ~list_state_machine() {}

    virtual std::vector<char> snapshot() override;

    virtual void apply_log(raft_command &) override;

    virtual void apply_snapshot(const std::vector<char> &) override;

    int num_append_logs;
    std::mutex mtx;
    std::vector<int> store;
};

std::vector<RPCServer*> create_random_rpc_servers(int num);
std::vector<RPCClient*> create_rpc_clients(const std::vector<RPCServer*> &servers);

template <class state_machine, class command>
class raft_group {
public:
     raft_group(int num, const char *dir = "raft_temp");
    ~raft_group();

    int  append_new_command(int, int);
    int  check_exact_one_leader();
    void check_no_leader();
    int  check_same_term();
    void disable_node(int);
    void enable_node(int);
    int  get_committed_value(int);
    int  num_committed(int);
    int  restart(int);
    int  rpc_count(int);
    void set_reliable(bool);
    int  wait_commit(int, int, int);
    
    std::vector<raft<state_machine, command>*> nodes;
    std::vector<std::vector<RPCClient*>> clients;
    std::vector<RPCServer*> servers;
    std::vector<state_machine*> states;
    std::vector<raft_storage<command>*> storages;
};

template <class state_machine, class command>
raft_group<state_machine, command>::raft_group(int num, const char *dir) {
    nodes.resize(num, nullptr);
    clients.resize(num);
    servers = create_random_rpc_servers(num);
    states.resize(num);
    storages.resize(num);
    remove_directory(dir);
    ASSERT(0 == mkdir(dir, 0777), "cannot create dir " << std::string(dir));
    for (int i = 0; i < num; ++i) {
        std::string dir_name(dir);
        dir_name = dir_name + "/raft_storage_" + std::to_string(i);
        ASSERT(0 == mkdir(dir_name.c_str(), 0777), "cannot create dir " << std::string(dir));
        raft_storage<command> *storage = new raft_storage<command>(dir_name);
        state_machine *state = new state_machine();
        auto client = create_rpc_clients(servers);
        raft<state_machine, command> *node = new raft<state_machine, command>(servers[i], client, i, storage, state);
        nodes[i] = node;
        clients[i] = client;
        states[i] = state;
        storages[i] = storage;
    }
    for (int i = 0; i < num; ++i) {
        nodes[i]->start();
    }
}

template <class state_machine, class command>
raft_group<state_machine, command>::~raft_group() {
    set_reliable(true);
    for (size_t i = 0; i < nodes.size(); ++i) {
        raft<state_machine, command> *node = nodes[i];
        node->stop();
        for (size_t j = 0; j < nodes.size(); ++j) {
            clients[i][j]->cancel();
            delete clients[i][j];
        }
        delete servers[i];
        delete node;
        delete states[i];
        delete storages[i];
    }
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::append_new_command(int value, int expected_servers) {
    list_command cmd(value);
    auto start = std::chrono::system_clock::now();
    int leader_idx = 0;
    while (std::chrono::system_clock::now() < start + std::chrono::seconds(10)) {
        int log_index = -1;
        for (size_t i = 0; i < nodes.size(); i++) {
            leader_idx = (leader_idx + 1) % nodes.size();
            // FIXME: lock?
            if (!servers[leader_idx]->get_reachable()) continue;

            int temp_idx, temp_term;
            bool is_leader = nodes[leader_idx]->new_command(cmd, temp_term, temp_idx);
            if (is_leader) {
                log_index = temp_idx;
                break;
            }
        }
        if (log_index != -1) {
            auto check_start = std::chrono::system_clock::now();
            while (std::chrono::system_clock::now() < check_start + std::chrono::seconds(2)) {
                int committed_server = num_committed(log_index);
                if (committed_server >= expected_servers) {
                    // The log is committed!
                    int commited_value = get_committed_value(log_index);
                    if (commited_value == value)
                        return log_index; // and the log is what we want!
                }
                msleep(20);
            }
        } else {
            // no leader
            msleep(50);
        }
    }
    ASSERT(0, "Cannot make agreement!");
    return -1;
}

template <class state_machine, class command>
void
raft_group<state_machine, command>::check_no_leader() {
    int num_nodes = nodes.size();
    for (int j = 0; j < num_nodes; j++) {
        raft<state_machine, command> *node = nodes[j];
        RPCServer *server = servers[j];
        if (server->get_reachable()) {
            int term = -1;
            ASSERT(!node->is_leader(term), "Node " << j << " is leader, which is unexpected.");
        }
    }
    return;
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::check_same_term() {
    int current_term = -1;
    int num_nodes = nodes.size();
    for (int i = 0; i < num_nodes; i++) {
        int term = -1;
        raft<state_machine, command> *node = nodes[i];
        node->is_leader(term); // get the current term
        ASSERT(term >= 0, "invalid term: " << term);
        if (current_term == -1) current_term = term;
        ASSERT(current_term == term, "inconsistent term: " << current_term << ", " << term);
    }
    return current_term;
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::check_exact_one_leader() {
    int num_checks = 10;
    int num_nodes = nodes.size();
    for (int i = 0; i < num_checks; i++) {
        std::map<int, int> term_leaders;
        for (int j = 0; j < num_nodes; j++) {
            raft<state_machine, command> *node = nodes[j];
            if (!servers[j]->get_reachable()) continue;
            int term = -1;
            bool is_leader = node->is_leader(term);
            if (is_leader) {
                ASSERT(term > 0, "term " << term << " should not have a leader.");
                ASSERT(term_leaders.find(term) == term_leaders.end(), "term " << term << " has more than one leader.");
                term_leaders[term] = j;
            }
        }
        if (term_leaders.size() > 0) {
            auto last_term = term_leaders.rbegin();
            return last_term->second; // return the leader index
        }
        // sleep a while, in case the election is not successful.
        msleep(500 + (random() % 10) * 30);
    }
    ASSERT(0, "There is no leader"); // no leader
    return -1;
}

template <class state_machine, class command>
void
raft_group<state_machine, command>::disable_node(int i) {
    RPCServer *server = servers[i];
    std::vector<RPCClient*> &client = clients[i];
    server->set_reachable(false);
    for (auto c : client) {
        c->set_reachable(false);
    }
}

template <class state_machine, class command>
void
raft_group<state_machine, command>::enable_node(int i) {
    RPCServer *server = servers[i];
    std::vector<RPCClient*> &client = clients[i];
    server->set_reachable(true);
    for (auto c : client) {
        c->set_reachable(true);
    }
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::get_committed_value(int log_index) {
    for (size_t i = 0; i < nodes.size(); i++) {
        list_state_machine *state = states[i];
        int log_value;
        {
            std::unique_lock<std::mutex> lock(state->mtx);
            if ((int)state->store.size() > log_index) {
                log_value = state->store[log_index];
                return log_value;
            }
        }
    }
    ASSERT(false, "log " << log_index << " is not committed." );
    return -1;
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::num_committed(int log_index) {
    int cnt = 0;
    int old_value = 0;
    for (size_t i = 0; i < nodes.size(); i++) {
        list_state_machine *state = states[i];
        bool has_log;
        int log_value;
        {
            std::unique_lock<std::mutex> lock(state->mtx);
            if ((int)state->store.size() > log_index) {
                log_value = state->store[log_index];
                has_log = true;
            } else {
                has_log = false;
            }
        }
        if (has_log) {
            cnt ++;
            if (cnt == 1) {
                old_value = log_value;
            } else {
                ASSERT(old_value == log_value, "inconsistent log value: (" << log_value << ", " << old_value << ") at idx " << log_index);
            }
        }
    }
    return cnt;
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::restart(int node) {
    nodes[node]->stop();
    disable_node(node);
    servers[node]->unregister_all();
    delete nodes[node];
    delete states[node];
    states[node] = new state_machine();
    raft_storage<command> *storage = new raft_storage<command>(std::string("raft_temp/raft_storage_") + std::to_string(node));
    // recreate clients
    for (auto &cl : clients[node])
        delete cl;
    servers[node]->set_reachable(true);
    clients[node] = create_rpc_clients(servers);

    nodes[node] = new raft<state_machine, command>(servers[node], clients[node], node, storage, states[node]);
    nodes[node]->start();
    return 0;
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::rpc_count(int node) {
    int sum = 0;
    if (node == -1) {
        for (auto &cl : clients) {
            for (auto &cc : cl) {
                sum += cc->get_count();
            }
        }
    } else {
        for (auto &cc : clients[node]) {
            sum += cc->get_count();
        }
    }
    return sum;
}

template <class state_machine, class command>
void
raft_group<state_machine, command>::set_reliable(bool value) {
    for (auto server : servers) {
        server->set_reliable(value);
    }
}

template <class state_machine, class command>
int
raft_group<state_machine, command>::wait_commit(int index, int num_committed_server, int start_term) {
    int sleep_for = 10; // ms
    for (int iters = 0; iters < 30; iters++) {
        int nc = num_committed(index);
        if (nc >= num_committed_server) break;
        msleep(sleep_for);
        if (sleep_for < 1000) sleep_for *=2;
        if (start_term > -1) {
            for (auto node : nodes) {
                int current_term;
                node->is_leader(current_term);
                if (current_term > start_term) {
                    // someone has moved on
                    // can no longer guarantee that we'll "win"
                    return -1;
                }
            }
        }
    }
    int nc = num_committed(index);
    ASSERT(nc >= num_committed_server, "only " << nc << " decided for index " << index << "; wanted " << num_committed_server);
    return get_committed_value(index);
}

void make_socket_addr(const char *host_and_port, struct sockaddr_in *dst);

void make_socket_addr(const char *host, const char *port, struct sockaddr_in *dst);

#endif //RAFT_KV_STORE_TEST_UTILS_H
