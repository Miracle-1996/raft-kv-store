#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <mutex>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <unistd.h>
#include <test/raft_test_utils.h>

/******************************************************************

                       unit_test_suite

*******************************************************************/
unit_test_suite *
unit_test_suite::instance() {
    static unit_test_suite _instance;
    return &_instance;
}

unit_test_case *
unit_test_suite::register_test_case(const char *part, const char *name, unit_test_case *obj) {
    std::string spart(part);
    std::string sname(name);
    for (auto &part_case : cases) {
        if (part_case.first == spart) {
            part_case.second.push_back({sname, obj});
            return obj;
        }
    }
    std::vector<std::pair<std::string, unit_test_case *>> part_cases;
    part_cases.push_back({sname, obj});
    cases.push_back({spart, part_cases});
    return nullptr;
}

bool
unit_test_suite::run(int argc, char *argv[]) {
    auto f = [=]() {
        if (argc == 1) {
            run_all();
        } else if (argc == 2) {
            std::string spart(argv[1]);
            run_part_case(spart, "*");
        } else {
            std::string spart(argv[1]), sname(argv[2]);
            run_part_case(spart, sname);
        }
    };
    is_counting = true;
    count = 0;
    passed = 0;
    f();

    std::cout << "Running " << count << " Tests ..." << std::endl;
    is_counting = false;
    static int clktck = -1;
    if (clktck == -1) clktck = sysconf(_SC_CLK_TCK);
    clock_t start, end;
    tms start_t, end_t;
    start = times(&start_t);
    f();
    end = times(&end_t);
    std::cout << "Pass " << passed << "/" << count << " tests. wall-time: " << (double)(end-start) / (double)clktck
              << "s, user-time: " << (double)(end_t.tms_utime - start_t.tms_utime) /(double)clktck
              << "s, sys-time: " << (double)(end_t.tms_stime - start_t.tms_stime) / (double)clktck << "s" << std::endl;
    return true;
}

bool
unit_test_suite::run_all() {
    for (auto &parts : cases) {
        run_part_case(parts.first, "*");
    }
    return true;
}

bool
unit_test_suite::run_case(const std::string &part, const std::string &name, unit_test_case *obj) {
    if (is_counting) {
        count++;
        return true;
    }
    std::cout << "Test (" << part << "." << name << "): " << std::string(obj->message) << std::endl;
    static int clktck = -1;
    if (clktck == -1) {
        clktck = sysconf(_SC_CLK_TCK);
    }
    clock_t start, end;
    tms start_t, end_t;
    start = times(&start_t);
    obj->body();
    end = times(&end_t);
    std::cout << "Pass (" << part << "." << name << "). wall-time: " << (double)(end-start) / (double)clktck
              << "s, user-time: " << (double)(end_t.tms_utime - start_t.tms_utime) /(double)clktck
              << "s, sys-time: " << (double)(end_t.tms_stime - start_t.tms_stime) / (double)clktck << "s" << std::endl;
    passed++;
    return true;
}

bool
unit_test_suite::run_part_case(const std::string &spart, const std::string &sname) {
    for (auto &part : cases) {
        if (part.first == spart) {
            for (auto &part_cases : part.second) {
                if (sname == "*" || sname == part_cases.first) {
                    run_case(spart, part_cases.first, part_cases.second);
                }
            }
        }
    }
    return true;
}

/******************************************************************

                       list_command

*******************************************************************/

Marshall &operator<<(Marshall &m, const list_command &cmd) {
    m << cmd.value;
    return m;
}

Unmarshall &operator>>(Unmarshall &u, list_command &cmd) {
    u >> cmd.value;
    return u;
}

int
list_command::size() const {
    return 4;
}

void
list_command::serialize(char *buf, int sz) const {
    if (sz != size()) return;
    buf[0] = (value >> 24) & 0xff;
    buf[1] = (value >> 16) & 0xff;
    buf[2] = (value >>  8) & 0xff;
    buf[3] = value & 0xff;
}

void
list_command::deserialize(const char *buf, int sz) {
    if (sz != size()) return;
    value  = (buf[0] & 0xff) << 24;
    value |= (buf[1] & 0xff) << 16;
    value |= (buf[2] & 0xff) << 8;
    value |= (buf[3] & 0xff);
}

/******************************************************************

                       list_state_machine

*******************************************************************/

list_state_machine::list_state_machine() {
    // log is start from 1, so we push back a value to align with the raft log.
    store.push_back(0);
    num_append_logs = 0;
}

void
list_state_machine::apply_log(raft_command &cmd) {
    std::unique_lock<std::mutex> lock(mtx);
    const list_command &list_cmd = dynamic_cast<const list_command&>(cmd);
    store.push_back(list_cmd.value);
    num_append_logs++;
}

void
list_state_machine::apply_snapshot(const std::vector<char> &snapshot) {
    std::unique_lock<std::mutex> lock(mtx);
    std::string str;
    str.assign(snapshot.begin(), snapshot.end());
    std::stringstream ss(str);
    store = std::vector<int>();
    int size;
    ss >> size;
    for (int i = 0; i < size; i++) {
        int value;
        ss >> value;
        store.push_back(value);
    }
}

std::vector<char>
list_state_machine::snapshot() {
    std::unique_lock<std::mutex> lock(mtx);
    std::vector<char> data;
    std::stringstream ss;
    ss << (int)store.size();
    for (auto value : store) {
        ss << ' ' << value;
    }
    std::string str = ss.str();
    data.assign(str.begin(), str.end());
    return data;
}

/******************************************************************

                       auxiliary functions

*******************************************************************/

void
make_socket_addr(const char *host, const char *port, struct sockaddr_in *dst) {
    bzero(dst, sizeof(*dst));
    dst->sin_family = AF_INET;

    in_addr_t a = inet_addr(host);
    if(a != INADDR_NONE){
        dst->sin_addr.s_addr = a;
    }
    else {
        struct hostent *hp = gethostbyname(host);
        if(nullptr == hp || hp->h_length != 4){
            fprintf(stderr, "cannot find host name %s\n", host);
            exit(1);
        }
        dst->sin_addr.s_addr = ((struct in_addr *)(hp->h_addr))->s_addr;
    }
    dst->sin_port = htons(atoi(port));
}

void
make_socket_addr(const char *host_and_port, struct sockaddr_in *dst) {
    int size = 200;
    char host[size];
    const char *localhost = "127.0.0.1";
    const char *port = index(host_and_port, ':');
    if (nullptr == port) {
        memcpy(host, localhost, strlen(localhost) + 1);
        port = host_and_port;
    }
    else {
        memcpy(host, host_and_port, port - host_and_port);
        host[port - host_and_port] = '\0';
        port++;
    }
    make_socket_addr(host, port, dst);
}

std::vector<RPCClient*>
create_rpc_clients(const std::vector<RPCServer*> &servers) {
    int num = servers.size();
    std::vector<RPCClient*> rpc_client_list(num);
    for (int i = 0; i < num; ++i) {
        struct sockaddr_in sin;
        make_socket_addr(std::to_string(servers[i]->get_port()).c_str(), &sin);
        rpc_client_list[i] = new RPCClient(sin);
        int retval = rpc_client_list[i]->bind();
        ASSERT(0 == retval, "bind fail" << retval);
    }
    return rpc_client_list;
}

std::vector<RPCServer*>
create_random_rpc_servers(int num) {
    std::vector<RPCServer*> rpc_server_list(num);
    static int port = 3536;
    for (int i = 0; i < num; ++i) {
        rpc_server_list[i] = new RPCServer(port++);
    }
    return rpc_server_list;
}

void
msleep(int ms) {
    usleep(1000 * ms);
}

int
remove_directory(const char *path) {
    int retval = -1;
    DIR *d = opendir(path);
    size_t path_size = strlen(path);

    if (d) {
        retval = 0;
        struct dirent *p = nullptr;
        while (!retval && (p = readdir(d))) {
            int result = -1;

            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, "..")) {
                continue;
            }

            size_t size = path_size + strlen(p->d_name) + 2;
            char *buf = (char*)malloc(size);

            if (buf) {
                struct stat stat_buffer;
                snprintf(buf, size, "%s/%s", path, p->d_name);
                if (!stat(buf, &stat_buffer)) {
                    if (S_ISDIR(stat_buffer.st_mode)) {
                        result = remove_directory(buf);
                    }
                    else {
                        result = unlink(buf);
                    }
                }
                free(buf);
            }
            retval = result;
        }
        closedir(d);
    }

    if (!retval) {
        retval = rmdir(path);
    }
    return retval;
}