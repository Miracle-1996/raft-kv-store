#include <iostream>
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <string>
#include <unistd.h>
#include <rpc/marshall.h>
#include <rpc/reply_request.h>
#include <rpc/unmarshall.h>
#include <utils/verify.h>
using namespace std;

constexpr int64_t INT_1  = 1 << 1;
constexpr int64_t INT_8  = 1 << 8;
constexpr int64_t INT_16 = 1 << 16;
constexpr int64_t INT_32 = 1LL << 32;

template <class T>
void test(string type, vector<T> &marshall) {
    VERIFY(0 == marshall.size() % 4);
    Marshall m;
    for (int k = 0; k < (int)marshall.size(); ++k) {
        m << marshall[k];
    }
    string str = m.get_content();
    Unmarshall u(str);
    vector<T> unmarshall;
    int size = (int)marshall.size();
    for (int k = 0; k < size; ++k) {
        T x;
        u >> x;
        unmarshall.push_back(x);
    }
    VERIFY(u.done());
    for (int k = 0; k < size; ++k) {
        VERIFY(marshall[k] == unmarshall[k]);
    }
    cout << "test " << type <<  " ok" << endl;
}

void test_bool(int size) {
    vector<bool> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_1);
    }
    test<bool>(string("bool"), v);
}

void test_char(int size) {
    vector<char> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_8);
    }
    test<char>(string("char"), v);
}

void test_unsigned_char(int size) {
    vector<unsigned char> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_8);
    }
    test<unsigned char>(string("unsigned char"), v);
}

void test_short(int size) {
    vector<short> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_16);
    }
    test<short>(string("short"), v);
}

void test_unsigned_short(int size) {
    vector<unsigned short> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_16);
    }
    test<unsigned short>(string("unsigned short"), v);
}

void test_int(int size) {
    vector<int> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_32);
    }
    test<int>(string("int"), v);
}

void test_unsigned_int(int size) {
    vector<unsigned int> v;
    for (int k = 0; k < size; ++k) {
        v.push_back(rand() % INT_32);
    }
    test<unsigned int>(string("unsigned int"), v);
}

void test_unsigned_long_long(int size) {
    vector<unsigned long long> v;
    for (int k = 0; k < size; ++k) {
        int low  = rand() % INT_32;
        int high = rand() % INT_32;
        unsigned long long x = ((unsigned long long)high << 32) | low;
        v.push_back(x);
    }
    test<unsigned long long>(string("unsigned long long"), v);
}

void test_marshall_unmarshall()
{
    Marshall m;
    request_header m_rh(1, 2, 3, 4, 5);
    m.pack_request_header(m_rh);
    VERIFY(RPC_HEADER_SZ == m.get_index());
    int m_int_val = 12345;
    unsigned long long m_ull_val = 1223344455L;
    string m_str = string("hello....");
    m << m_int_val;
    m << m_ull_val;
    m << m_str;
    int size = 0;
    char *buf = nullptr;
    m.take_buf(&buf, &size);
    VERIFY(size == (int)(RPC_HEADER_SZ + sizeof(int) + sizeof(unsigned long long) + sizeof(unsigned int) + m_str.size()));

    Unmarshall u(buf, size);
    request_header u_rh;
    u.unpack_request_header(&u_rh);
    VERIFY(0 == memcmp(&m_rh, &u_rh, sizeof(request_header)));
    int u_int_val;
    unsigned long long u_ull_val;
    string u_str;
    u >> u_int_val;
    u >> u_ull_val;
    u >> u_str;
    VERIFY(u.done());
    VERIFY(u_int_val == m_int_val);
    VERIFY(u_ull_val == m_ull_val);
    VERIFY(u_str == m_str);
    cout << "test_marshall_unmarshall ok" << endl;
}

int main() {
    int size = 4096;
    srand(getpid());
    test_bool(size);
    test_char(size);
    test_unsigned_char(size);
    test_short(size);
    test_unsigned_short(size);
    test_int(size);
    test_unsigned_int(size);
    test_unsigned_long_long(size);
    test_marshall_unmarshall();
    cout << "All tests pass!!!" << endl;
    return 0;
}