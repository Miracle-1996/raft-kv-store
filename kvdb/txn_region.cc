#include <kvdb/txn_region.h>

txn_region::txn_region(kvdb *db): db(db), txn_id(db->next_txn_id()) {
    this->txn_begin();
}

txn_region::~txn_region() {
    if (this->txn_can_commit() == kvdb_protocol::prepare_ok) {
        this->txn_commit();
    }
    else {
        this->txn_abort();
    }
    release_locks();
}

int
txn_region::dummy() {
    int r;
    this->db->server->execute(1, kvdb_protocol::Dummy, kvdb_protocol::operation_var(txn_id, 1024, 16), r);
    return r;
}

int
txn_region::get(const int key) {
    int r;
    if (locked_keys.find(key) == locked_keys.end()) {
        while (this->db->server->acquire_lock(key, txn_id) != lock_ok) {
            rollback_redo();
        }
        locked_keys.insert(key);
    }
    this->db->server->execute(key, kvdb_protocol::Get, kvdb_protocol::operation_var(txn_id, key, 0), r);
    return r;
}

int
txn_region::put(const int key, const int val) {
    int r;
    if (locked_keys.find(key) == locked_keys.end()) {
        while (this->db->server->acquire_lock(key, txn_id) != lock_ok) {
            rollback_redo();
        }
        locked_keys.insert(key);
    }
    log.emplace_back(key, val);
    this->db->server->execute(key, kvdb_protocol::Put, kvdb_protocol::operation_var(txn_id, key, val), r);
    return r;
}

int
txn_region::txn_can_commit() {
    return this->db->server->txn_can_commit(txn_id);
}

int
txn_region::txn_begin() {
    printf("txn[%d] begin\n", txn_id);
    return this->db->server->txn_begin(txn_id);
}

int
txn_region::txn_commit() {
    printf("txn[%d] commit\n", txn_id);
    return this->db->server->txn_commit(txn_id);
}

int
txn_region::txn_abort() {
    printf("txn[%d] abort\n", txn_id);
    return this->db->server->txn_abort(txn_id);
}

int
txn_region::rollback_redo() {
    bool should_rollback_redo = true;
    while (should_rollback_redo) {
        should_rollback_redo = false;
        if (log.size() > 0) this->db->server->txn_rollback(txn_id);
        release_locks();
        for (auto &entry : log) {
            if (this->db->server->acquire_lock(entry.key, txn_id) != lock_ok) {
                should_rollback_redo = true;
                break;
            }
        }
    }

    return 0;
}

void
txn_region::release_locks() {
    for (int key : locked_keys) {
        this->db->server->release_lock(key);
    }
}