#include <string>
#include <kvdb/kvdb.h>

const std::string command_type_string[] = {
	"CMD_NONE",
	"CMD_GET",
	"CMD_PUT",
	"TXN_BEGIN",
	"TXN_PREPARE",
	"TXN_COMMIT",
	"TXN_ABORT",
	"TXN_ROLLBACK"
};

kvdb::kvdb(int shard_num, int cluster_port, shard_dispatch dispatch): max_txn_id(0), server(new view_server(cluster_port, dispatch))
{
    for (int i = 1; i <= shard_num; ++i) {
        auto *shard = new shard_client(i, i + cluster_port);
        server->add_shard_client(shard);
        this->shards.push_back(shard);
    }
}

kvdb::~kvdb() {
    for (auto &shard: shards) {
        delete shard;
    }
    delete server;
}

int
kvdb::next_txn_id() {
    int res;
    txn_id_mtx.lock();
    res = max_txn_id++;
    txn_id_mtx.unlock();
    return res;
}

void
kvdb::set_shards_down(const std::set<int> &shards_offset) {
    const int size = (int)shards.size();
    VERIFY(size > 0);

    for (auto offset: shards_offset) {
        shards[offset % size]->set_active(false);
    }
}

int
kvdb::default_dispatch(int key, int shard_num) {
    int shard_offset = key % shard_num;
    ++shard_offset;
    return shard_offset;
}