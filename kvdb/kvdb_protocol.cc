#include <kvdb/kvdb_protocol.h>

namespace kvdb_protocol {
    Marshall &
    operator<<(Marshall &m, const dummy_var &var) {
        m << var.v0;
        m << var.v1;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, dummy_var &var) {
        u >> var.v0;
        u >> var.v1;
        return u;
    }

    Marshall &
    operator<<(Marshall &m, const operation_var &var) {
        m << var.key;
        m << var.value;
        m << var.txn_id;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, operation_var &var) {
        u >> var.key;
        u >> var.value;
        u >> var.txn_id;
        return u;
    }

    Marshall &
    operator<<(Marshall &m, const prepare_var &var) {
        m << var.txn_id;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, prepare_var &var) {
        u >> var.txn_id;
        return u;
    }

    Marshall &
    operator<<(Marshall &m, const check_prepare_state_var &var) {
        m << var.txn_id;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, check_prepare_state_var &var) {
        u >> var.txn_id;
        return u;
    }

    Marshall &
    operator<<(Marshall &m, const commit_var &var) {
        m << var.txn_id;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, commit_var &var) {
        u >> var.txn_id;
        return u;
    }

    Marshall &
    operator<<(Marshall &m, const rollback_var &var) {
        m << var.txn_id;
        return m;
    }

    Unmarshall &
    operator>>(Unmarshall &u, rollback_var &var) {
        u >> var.txn_id;
        return u;
    }
}