#include <kvdb/shard_client.h>

shard_client::shard_client(const int shard_id, const int port): active(true), shard_id(shard_id), node(new rpc_node(port))
{
    this->store.resize(this->replica_num);
    // reg rpc handlers.
    this->node->register_service(kvdb_protocol::Dummy, this, &shard_client::dummy);
    this->node->register_service(kvdb_protocol::Put, this, &shard_client::put);
    this->node->register_service(kvdb_protocol::Get, this, &shard_client::get);
    // transaction related
    this->node->register_service(kvdb_protocol::Prepare, this, &shard_client::prepare);
    this->node->register_service(kvdb_protocol::Commit, this, &shard_client::commit);
    this->node->register_service(kvdb_protocol::Rollback, this, &shard_client::rollback);
}

shard_client::~shard_client() {
    delete node;
}

void
shard_client::bind_view_server(const int server_port) {
    this->node->bind_remote_node(server_port);
    this->view_server_port = server_port;
}

int
shard_client::dummy(kvdb_protocol::operation_var var, int &r) {
    printf("Receive dummy Request! txn id:%d\n", var.txn_id);
    r = var.txn_id;
    return 0;
}

int
shard_client::put(kvdb_protocol::operation_var var, int &r) {
    // prepare undo log
    undo_log_entry log_entry;
    log_entry.key = var.key;
    log_entry.new_val = var.value;
    auto entry = store[primary_replica].find(var.key);
    if (entry == store[primary_replica].end()) {
        log_entry.has_old_val = false;
        r = 0;
        // write value
        store[primary_replica][var.key] = value_entry(var.value);
    }
    else {
        log_entry.has_old_val = true;
        r = entry->second.value;
        log_entry.old_val = r;
        // write value
        entry->second.value = var.value;
    }

    auto txn_logs = undo_log.find(var.txn_id);
    if (txn_logs == undo_log.end()) {
        undo_log[var.txn_id] = std::list<undo_log_entry>({ log_entry });
    }
    else {
        txn_logs->second.push_back(log_entry);
    }

    return 0;
}

int
shard_client::get(kvdb_protocol::operation_var var, int &r) {
    r = store[primary_replica][var.key].value;
    return 0;
}

int
shard_client::commit(kvdb_protocol::commit_var var, int &r) {
    if (undo_log.find(var.txn_id) != undo_log.end() && !active) {
        r = kvdb_protocol::prepare_not_ok;
    }
    else {
        r = kvdb_protocol::prepare_ok;
        replicate_txn(var.txn_id);
        undo_log.erase(var.txn_id);
    }
    return 0;
}

int
shard_client::rollback(kvdb_protocol::rollback_var var, int &r) {
    if (undo_log.find(var.txn_id) == undo_log.end()) {
        r = 0;
    }
    else {
        for (auto entry = undo_log[var.txn_id].rbegin(); entry != undo_log[var.txn_id].rend(); ++entry) {
            if (entry->has_old_val) {
                store[primary_replica][entry->key] = value_entry(entry->old_val);
            }
            else {
                store[primary_replica].erase(entry->key);
            }
        }
    }
    return 0;
}

int
shard_client::prepare(kvdb_protocol::prepare_var var, int &r) {
    if (undo_log.find(var.txn_id) != undo_log.end() && !active) {
        r = kvdb_protocol::prepare_not_ok;
    }
    else {
        r = kvdb_protocol::prepare_ok;
    }
    return 0;
}

void
shard_client::replicate_txn(int txn_id) {
    if (undo_log.find(txn_id) != undo_log.end()) {
        int store_size = store.size();
        for (int i = 0; i < store_size; ++i) {
            if (i != primary_replica) {
                for (auto &entry : undo_log[txn_id]) {
                    store[i][entry.key] = value_entry(entry.new_val);
                }
            }
        }
    }
}

void
shard_client::set_active(bool can_active) {
    this->active = can_active;
}

int
shard_client::shuffle_primary_replica() {
    int next = this->primary_replica;
    while (next == this->primary_replica) {
        next = random() % this->store.size();
    }
    this->primary_replica = next;
    return next;
}

std::map<int, value_entry> &
shard_client::get_store() {
    return this->store[primary_replica];
}