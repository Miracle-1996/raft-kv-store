#include <kvdb/common.h>
#include <kvdb/kvdb_command.h>
#include <kvdb/kvdb_protocol.h>
#include <kvdb/kvdb_state_machine.h>
#include <utils/verify.h>

kvdb_state_machine::kvdb_state_machine(int base_port, rpc_node *node, shard_dispatch dispatch): base_port(base_port), node(node), dispatch(dispatch) {}

int
kvdb_state_machine::shard_num() const {
    return this->node->rpc_clients.size();
}

void
kvdb_state_machine::apply_log(raft_command &cmd) {
    kvdb_command &kvdb_cmd = dynamic_cast<kvdb_command&>(cmd);
    int shard_offset = dispatch(kvdb_cmd.key, shard_num());

    if (!kvdb_cmd.should_send_rpc) {
        if (kvdb_cmd.cmd_type == CMD_PUT) {
            related_shards[kvdb_cmd.txn_id].insert(shard_offset);
        }
        return;
    }

    std::unique_lock<std::mutex> lock(kvdb_cmd.res->mtx);
    if (kvdb_cmd.cmd_type == CMD_GET) {
        apply_get(kvdb_cmd, shard_offset);
    }
    else if (kvdb_cmd.cmd_type == CMD_PUT) {
        apply_put(kvdb_cmd, shard_offset);
    }
    else if (kvdb_cmd.cmd_type == TXN_PREPARE) {
        apply_prepare(kvdb_cmd);
    }
    else if (kvdb_cmd.cmd_type == TXN_BEGIN) {
        apply_begin(kvdb_cmd);
    }
    else if (kvdb_cmd.cmd_type == TXN_COMMIT) {
        apply_commit(kvdb_cmd);
    }
    else if (kvdb_cmd.cmd_type == TXN_ABORT) {
        apply_abort(kvdb_cmd);
    }
    else if (kvdb_cmd.cmd_type == TXN_ROLLBACK) {
        apply_rollback(kvdb_cmd);
    }
    else {
        fprintf(stderr, "kvdb_state_machine::apply_log unknown command type!");
        VERIFY(0);
    }

    kvdb_cmd.res->done = true;
    kvdb_cmd.res->cv.notify_all();
}

void
kvdb_state_machine::apply_get(kvdb_command &kvdb_cmd, int shard_offset) {
    kvdb_protocol::operation_var var(kvdb_cmd.txn_id, kvdb_cmd.key, 0);
    int r = 0;
    this->node->template call(base_port + shard_offset, kvdb_protocol::Get, var, r);
    printf("Get %d = %d on shard %d\n", kvdb_cmd.key, r, shard_offset);
    kvdb_cmd.res->value = r;
}

void
kvdb_state_machine::apply_put(kvdb_command &kvdb_cmd, int shard_offset) {
    related_shards[kvdb_cmd.txn_id].insert(shard_offset);
    kvdb_protocol::operation_var var(kvdb_cmd.txn_id, kvdb_cmd.key, kvdb_cmd.value);
    int r = 0;
    this->node->template call(base_port + shard_offset, kvdb_protocol::Put, var, r);
    printf("Put %d = %d on shard %d\n", kvdb_cmd.key, kvdb_cmd.value, shard_offset);
    kvdb_cmd.res->value = r;
}

void
kvdb_state_machine::apply_abort(kvdb_command &kvdb_cmd) {
    int base_port = this->node->port();
    const std::set<int> &shard_offset = related_shards[kvdb_cmd.txn_id];
    kvdb_protocol::rollback_var var(kvdb_cmd.txn_id);

    for (auto offset : shard_offset) {
        int r = 0;
        node->template call(base_port + offset, kvdb_protocol::Rollback, var, r);
    }
    printf("TXN[%d] aborted\n", kvdb_cmd.txn_id);
    related_shards.erase(kvdb_cmd.txn_id);
    kvdb_cmd.res->value = 0;
}

void
kvdb_state_machine::apply_begin(kvdb_command &kvdb_cmd) {
    printf("TXN[%d] begin\n", kvdb_cmd.txn_id);
}

void
kvdb_state_machine::apply_commit(kvdb_command &kvdb_cmd) {
    int base_port = this->node->port();
    kvdb_protocol::commit_var var(kvdb_cmd.txn_id);
    const std::set<int> &shard_offset = related_shards[kvdb_cmd.txn_id];

    for (auto offset : shard_offset) {
        int r = 0;
        node->template call(base_port + offset, kvdb_protocol::Commit, var, r);
        if (r != kvdb_protocol::prepare_ok) {
            printf("Unexpected situation: sending commit to shard %d while not prepared\n", offset);
            VERIFY(0);
            kvdb_cmd.res->value = -1;
            kvdb_cmd.res->done = true;
            kvdb_cmd.res->cv.notify_all();
            return;
        }
    }
    printf("TXN[%d] committed\n", kvdb_cmd.txn_id);
    related_shards.erase(kvdb_cmd.txn_id);
    kvdb_cmd.res->value = 0;
}

void
kvdb_state_machine::apply_prepare(kvdb_command &kvdb_cmd) {
    int base_port = this->node->port();
    kvdb_protocol::prepare_var var(kvdb_cmd.txn_id);
    const std::set<int> &shard_offset = related_shards[kvdb_cmd.txn_id];

    for (auto offset : shard_offset) {
        int r = 0;
        printf("Preparing shard %d\n", offset);
        node->template call(base_port + offset, kvdb_protocol::Prepare, var, r);
        if (r != kvdb_protocol::prepare_ok) {
            printf("Preparing shard %d failed\n", offset);
            kvdb_cmd.res->value = kvdb_protocol::prepare_not_ok;
            kvdb_cmd.res->done = true;
            kvdb_cmd.res->cv.notify_all();
            return;
        }
    }
    printf("Preparing shard succeeded\n");
    kvdb_cmd.res->value = kvdb_protocol::prepare_ok;
}

void
kvdb_state_machine::apply_rollback(kvdb_command &kvdb_cmd) {
    int base_port = this->node->port();
    const std::set<int> &shard_offset = related_shards[kvdb_cmd.txn_id];
    kvdb_protocol::rollback_var var(kvdb_cmd.txn_id);

    for (auto offset : shard_offset) {
        int r = 0;
        node->template call(base_port + offset, kvdb_protocol::Rollback, var, r);
    }
    printf("TXN[%d] rollback\n", kvdb_cmd.txn_id);
    related_shards[kvdb_cmd.txn_id].clear();
    kvdb_cmd.res->value = 0;
}