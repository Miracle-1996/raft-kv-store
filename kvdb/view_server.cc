#include <kvdb/kvdb_utils.h>
#include <kvdb/view_server.h>

view_server::view_server(const int base_port, shard_dispatch dispatch, const int num_raft_nodes): node(new rpc_node(base_port)), dispatch(dispatch) {
#if RAFT_GROUP
    raft_group = new kvdb_raft_group(num_raft_nodes);
    for (int i = 0; i < num_raft_nodes; ++i) {
        auto state = dynamic_cast<kvdb_state_machine *>(raft_group->states[i]);
        state->base_port = base_port;
        state->dispatch = dispatch;
        state->node = node;
    }
#endif
}

view_server::~view_server() {
#if RAFT_GROUP
    delete this->raft_group;
#endif
    delete this->node;
}

int
view_server::add_shard_client(shard_client *shard) {
    int port = shard->node->port();
    int view_server_port = this->node->port();
    shard->bind_view_server(view_server_port);
    return this->node->bind_remote_node(port);
}

int
view_server::execute(unsigned int query_key, unsigned int proc, const kvdb_protocol::operation_var &var, int &r) {
    if (proc == kvdb_protocol::Get) {
        kvdb_command cmd(CMD_GET, var.key, var.value, var.txn_id);
        append_log(cmd);
        std::unique_lock<std::mutex> lock(cmd.res->mtx);
        if (!cmd.res->done) {
            ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "Get command timeout");
        }
        r = cmd.res->value;
    }
    else if (proc == kvdb_protocol::Put) {
        kvdb_command cmd(CMD_PUT, var.key, var.value, var.txn_id);
        append_log(cmd);
        std::unique_lock<std::mutex> lock(cmd.res->mtx);
        if (!cmd.res->done) {
            ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "Put command timeout");
        }
        r = cmd.res->value;
    }
    else {
        fprintf(stderr, "view_server::execute unknown procedure!");
        VERIFY(0);
    }

    return 0;
}

kvdb_protocol::prepare_state
view_server::txn_can_commit(int txn_id) {
    kvdb_command cmd(TXN_PREPARE, 0, 0, txn_id);
    append_log(cmd);
    std::unique_lock<std::mutex> lock(cmd.res->mtx);
    if (!cmd.res->done) {
        if (cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) != std::cv_status::no_timeout) {
            printf("PREPARE command timeout, retry...\n");
            append_log(cmd);
            ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "PREPARE command timeout, failed!");
        }
    }
    return kvdb_protocol::prepare_state(cmd.res->value);
}

int
view_server::txn_begin(int txn_id) {
    kvdb_command cmd(TXN_BEGIN, 0, 0, txn_id);
    append_log(cmd);
    std::unique_lock<std::mutex> lock(cmd.res->mtx);
    if (!cmd.res->done) {
        ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "BEGIN command timeout");
    }
    return cmd.res->value;
}

int
view_server::txn_commit(int txn_id) {
    kvdb_command cmd(TXN_COMMIT, 0, 0, txn_id);
    append_log(cmd);
    std::unique_lock<std::mutex> lock(cmd.res->mtx);
    if (!cmd.res->done) {
        ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "COMMIT command timeout");
    }
    return cmd.res->value;
}

int
view_server::txn_abort(int txn_id) {
    kvdb_command cmd(TXN_ABORT, 0, 0, txn_id);
    append_log(cmd);
    std::unique_lock<std::mutex> lock(cmd.res->mtx);
    if (!cmd.res->done) {
        ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "COMMIT command timeout");
    }
    return cmd.res->value;
}

int
view_server::txn_rollback(int txn_id) {
    kvdb_command cmd(TXN_ROLLBACK, 0, 0, txn_id);
    append_log(cmd);
    std::unique_lock<std::mutex> lock(cmd.res->mtx);
    if (!cmd.res->done) {
        ASSERT(cmd.res->cv.wait_until(lock, std::chrono::system_clock::now() + std::chrono::milliseconds(2500)) == std::cv_status::no_timeout, "ROLLBACK command timeout");
    }
    return cmd.res->value;
}

void
view_server::append_log(kvdb_command &entry) {
    entry.should_send_rpc = 1;
    int leader = raft_group->check_exact_one_leader();
    int term, index;
    printf("[txn_id %d append log] cmd_type: %s, key: %d, val: %d\n", entry.txn_id, command_type_string[entry.cmd_type].c_str(), entry.key, entry.value);
    while (!raft_group->nodes[leader]->new_command(entry, term, index)) {
        leader = raft_group->check_exact_one_leader();
    }
}

lock_state
view_server::acquire_lock(int key, int txn_id) {
    // use wait-die algorithm to solve deadlock
    // txn_id is used to compare the beginning time of each txn
    mtx.lock();
    auto lock = locks.find(key);

    if (lock == locks.end()) {
        auto new_lock = locks.emplace(key, txn_id);
        VERIFY(new_lock.first->second.lock.try_lock());
        mtx.unlock();
        return lock_ok;
    }

    if (lock->second.lock.try_lock()) {
        mtx.unlock();
        lock->second.time = txn_id;
        return lock_ok;
    }

    if (lock->second.time < txn_id) {
        // the resource is locked and current txn is younger, die
        mtx.unlock();
        return lock_die;
    }

    // the resource is locked and current txn is older, wait
    mtx.unlock();
    lock->second.lock.lock();
    lock->second.time = txn_id;
    return lock_ok;
}

void
view_server::release_lock(int key) {
    auto lock = locks.find(key);
    if (lock == locks.end()) {
        VERIFY(0);
    }
    lock->second.lock.unlock();
}