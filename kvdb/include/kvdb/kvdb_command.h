#ifndef RAFT_KV_STORE_KVDB_COMMAND_H
#define RAFT_KV_STORE_KVDB_COMMAND_H

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <kvdb/kvdb_utils.h>
#include <raft/raft_state_machine.h>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class kvdb_command : public raft_command {
public:
    struct result {
        std::chrono::system_clock::time_point start;
        int key, value, txn_id;
        command_type type;

        bool done;
        std::mutex mtx;             // protect the struct
        std::condition_variable cv; // notify the caller
    };

    kvdb_command();

    kvdb_command(command_type type, const int &key, const int &value, const int &txn_id);

    kvdb_command(const kvdb_command &cmd);

    virtual ~kvdb_command() {}

    command_type cmd_type;
    std::shared_ptr<result> res;
    int key, value, txn_id, should_send_rpc;

    virtual int size() const override;

    virtual void serialize(char *buf, int size) const override;

    virtual void deserialize(const char *buf, int size) override;
};

Marshall   &operator<<(Marshall &m, const kvdb_command &cmd);

Unmarshall &operator>>(Unmarshall &u, kvdb_command &cmd);

#endif //RAFT_KV_STORE_KVDB_COMMAND_H