#ifndef RAFT_KV_STORE_KVDB_UTILS_H
#define RAFT_KV_STORE_KVDB_UTILS_H

#include <string>
#include <mutex>

enum command_type {
    CMD_NONE = 0,        // Do nothing
    CMD_GET,             // Get a key-value pair
    CMD_PUT,             // Put a key-value pair
    TXN_BEGIN,
    TXN_PREPARE,
    TXN_COMMIT,
    TXN_ABORT,
    TXN_ROLLBACK
};

extern const std::string command_type_string[];

enum lock_state {
    lock_ok,
    lock_die
};

struct put_log {
    int key;
    int new_val;
    put_log(int k, int v): key(k), new_val(v) {}
};

struct time_lock {
    int time;
    std::mutex lock;

    time_lock(int t): time(t) {}
};

struct undo_log_entry {
    bool has_old_val;
    int key, old_val, new_val;
};

struct value_entry {
    int value;

    value_entry() {}
    value_entry(const int value): value(value) {}
    value_entry(const value_entry &entry) : value(entry.value) {}
};

#endif //RAFT_KV_STORE_KVDB_UTILS_H
