#ifndef RAFT_KV_STORE_COMMON_H
#define RAFT_KV_STORE_COMMON_H

#include <map>
#include <set>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <raft/raft.h>
#include <rpc/marshall.h>
#include <rpc/rpc_client.h>
#include <rpc/rpc_const.h>
#include <rpc/rpc_server.h>
#include <rpc/unmarshall.h>
#include <test/raft_test_utils.h>

#define RAFT_GROUP 1            /* Use raft group for view server. Enable it in part2 */
#define BIG_LOCK 0              /* Use big lock. Disable it to pass the last 3 (bonus) testcases */
#define KVDB_PORT 8041

/**
 * Abstraction of rpc server/client
 * */
class rpc_node {
public:
    RPCServer *rpc_server;                          /* For receive rpc request and send back reply */

    std::map<int, RPCClient *> rpc_clients;         /* For send rpc request and receive rpc reply */

     rpc_node(const int rpc_server_port): rpc_server(new RPCServer(rpc_server_port)) {}

    ~rpc_node() {
        for (auto &rpc_client: this->rpc_clients) {
            rpc_client.second->cancel();
            delete rpc_client.second;
        }
        this->rpc_server->unregister_all();
        delete this->rpc_server;
    }

    /**
     * Bind this rpc endpoint to another one by `remote_port`.
     * Return the bound port (expected to be the origin parameter)
     * */
    int bind_remote_node(const int remote_port) {
        struct sockaddr_in sin;
        make_socket_addr(std::to_string(remote_port).c_str(), &sin);
        RPCClient *client = new RPCClient(sin);
        VERIFY(client->bind() >= 0);
        this->rpc_clients[remote_port] = client;
        return remote_port;
    }

    int port() const {
        return this->rpc_server->get_port();
    }

    /**
     * Reg one rpc request handler
     * */
    template<class S, class A1, class R>
    void register_service(unsigned int proc, S *s, int(S::*method)(const A1 a1, R &r)) {
        this->rpc_server->template reg(proc, s, method);
    }

    /**
     * Post one rpc request to the node binding to `port`
     * */
    template<class R, class A1>
    int call(const int port, unsigned int proc, const A1 &a1, R &r) {
        return this->rpc_clients[port]->template call_basic(proc, r, time_out(RPCClient::max_limit),a1);
    }
};

#endif //RAFT_KV_STORE_COMMON_H
