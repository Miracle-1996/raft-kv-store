#ifndef RAFT_KV_STORE_VIEW_SERVER_H
#define RAFT_KV_STORE_VIEW_SERVER_H

#include "common.h"
#include "kvdb_command.h"
#include "kvdb_state_machine.h"
#include "kvdb_utils.h"
#include "shard_client.h"

using shard_dispatch  = int (*)(int key, int shard_num);
using kvdb_raft_group = raft_group<kvdb_state_machine, kvdb_command>;

class view_server {
public:
    rpc_node *node;
    shard_dispatch dispatch;            /* Dispatch requests to the target shard */
    kvdb_raft_group *raft_group;

     view_server(const int base_port, shard_dispatch dispatch, const int num_raft_nodes = 3);
    ~view_server();

    /**
     * Add the shard client for rpc communication
     * */
    int add_shard_client(shard_client *shard);

    /**
     * Dispatch the request to specific shard client(s)
     * Sync return when use normal view server (single node)
     * Async return when use raft group vie server, since the command log should be distributed first.
     * */
    int execute(unsigned int query_key, unsigned int proc, const kvdb_protocol::operation_var &var, int &r);

    kvdb_protocol::prepare_state txn_can_commit(int txn_id);

    int txn_abort(int txn_id);

    int txn_begin(int txn_id);

    int txn_commit(int txn_id);

    int txn_rollback(int txn_id);

    lock_state acquire_lock(int key, int txn_id);

    void release_lock(int key);

private:
    void append_log(kvdb_command &entry);
    std::mutex mtx;
    std::map<int, time_lock> locks;
};

#endif //RAFT_KV_STORE_VIEW_SERVER_H
