#ifndef RAFT_KV_STORE_KVDB_STATE_MACHINE_H
#define RAFT_KV_STORE_KVDB_STATE_MACHINE_H

#include <map>
#include <set>
#include <raft/raft_state_machine.h>

using shard_dispatch = int (*)(int key, int shard_num);

class kvdb_state_machine : public raft_state_machine {
private:
    std::map<int, std::set<int>> related_shards;

    int shard_num() const;

public:
    int base_port;
    rpc_node *node;
    shard_dispatch dispatch;

    kvdb_state_machine() {}

    kvdb_state_machine(int base_port, rpc_node *node , shard_dispatch dispatch);

    virtual ~kvdb_state_machine() {}

    virtual void apply_log(raft_command &cmd) override;

    virtual std::vector<char> snapshot() override {
        return std::vector<char>();
    }

    virtual void apply_snapshot(const std::vector<char> &) override {}

private:
    void apply_get(kvdb_command &kvdb_cmd, int shard_offset);
    void apply_put(kvdb_command &kvdb_cmd, int shard_offset);
    void apply_abort(kvdb_command &kvdb_cmd);
    void apply_begin(kvdb_command &kvdb_cmd);
    void apply_commit(kvdb_command &kvdb_cmd);
    void apply_prepare(kvdb_command &kvdb_cmd);
    void apply_rollback(kvdb_command &kvdb_cmd);
};

#endif //RAFT_KV_STORE_KVDB_STATE_MACHINE_H
