#ifndef RAFT_KV_STORE_KVDB_H
#define RAFT_KV_STORE_KVDB_H

#include <set>
#include "shard_client.h"
#include "view_server.h"

class kvdb {
public:
     kvdb(int shard_num, int cluster_port, shard_dispatch dispatch = default_dispatch);
    ~kvdb();

    int next_txn_id();

    void set_shards_down(const std::set<int> &shards_offset);

    int max_txn_id;
    view_server *server;
    std::vector<shard_client *> shards;
    std::mutex txn_id_mtx;

private:
    static int default_dispatch(int key, int shard_num);
};

#endif //RAFT_KV_STORE_KVDB_H
