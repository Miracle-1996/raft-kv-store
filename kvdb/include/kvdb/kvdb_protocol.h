#ifndef RAFT_KV_STORE_KVDB_PROTOCOL_H
#define RAFT_KV_STORE_KVDB_PROTOCOL_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

namespace kvdb_protocol {
    enum rpc_numbers {
        Dummy = 0xdead,
        /* kv storage related */
        Put,
        Get,
        /* transaction related */
        Prepare,
        Commit,
        Rollback,
    };

    /* Used for 2PC protocol */
    enum prepare_state {
        prepare_not_ok = 0,
        prepare_ok,
    };

    class operation_var {
    public:
        int txn_id;
        int key;
        int value;
        operation_var() {}
        operation_var(int txn_id, int key, int value): txn_id(txn_id), key(key), value(value) {}
    };

    class dummy_var {
    public:
        int v0;
        int v1;
    };

    class prepare_var {
    public:
        int txn_id;

        prepare_var() {}
        prepare_var(int txn_id): txn_id(txn_id) {}
    };

    class check_prepare_state_var {
    public:
        int txn_id;
    };

    class commit_var {
    public:
        int txn_id;

        commit_var() {}
        commit_var(int txn_id): txn_id(txn_id) {}
    };

    class rollback_var {
    public:
        int txn_id;

        rollback_var() {}
        rollback_var(int txn_id): txn_id(txn_id) {}
    };

    Marshall &
    operator<<(Marshall &m, const dummy_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, dummy_var &var);

    Marshall &
    operator<<(Marshall &m, const operation_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, operation_var &var);

    Marshall &
    operator<<(Marshall &m, const prepare_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, prepare_var &var);

    Marshall &
    operator<<(Marshall &m, const check_prepare_state_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, check_prepare_state_var &var);

    Marshall &
    operator<<(Marshall &m, const commit_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, commit_var &var);

    Marshall &
    operator<<(Marshall &m, const rollback_var &var);

    Unmarshall &
    operator>>(Unmarshall &u, rollback_var &var);
}


#endif //RAFT_KV_STORE_KVDB_PROTOCOL_H
