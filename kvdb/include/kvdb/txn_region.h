#ifndef RAFT_KV_STORE_TXN_REGION_H
#define RAFT_KV_STORE_TXN_REGION_H

#include <list>
#include "kvdb.h"

class txn_region {
public:
     txn_region(kvdb *db);
    ~txn_region();

    /**
     * Dummy request. Only for test
     * */
    int dummy();

    /**
     * Put one kv into the storage
     *
     * Note!: The changes in a the transaction region cannot be viewed by other regions until it commits.
     * Meanwhile, those changes can be **seen** in self region.
     * */
    int put(const int key, const int val);

    /**
     * Query one value from the storage by `key`
     * */
    int get(const int key);

    /**
     * Transaction check whether could commit.
     * Return 1 if all of the shards are ok to commit, and 0 if exists one not ok.
     * */
    int txn_can_commit();

    /*******************************************
     * Transaction part
     * ********************************************/
private:
    /**
     * Transaction begin
     * */
    int txn_begin();

    /**
     * Transaction commit. Sending `Commit` messages to all of the shard clients
    * */
    int txn_commit();

    /**
     * Transaction abort. Sending `Rollback` messages to all of the shard clients
     * */
    int txn_abort();

    int  rollback_redo();

    void release_locks();

    kvdb *db;
    const int txn_id;
    std::set<int> locked_keys;
    std::list<put_log> log;
};

#endif //RAFT_KV_STORE_TXN_REGION_H
