#ifndef RAFT_KV_STORE_SHARD_CLIENT_H
#define RAFT_KV_STORE_SHARD_CLIENT_H

#include <cstdlib>
#include <list>
#include <map>
#include <vector>
#include "common.h"
#include "kvdb_protocol.h"
#include "kvdb_utils.h"

/**
 * Storage layer for each shard. Support fault tolerance.
 * */
class shard_client {
public:
     shard_client(const int shard_id, const int port);
    ~shard_client();

    void bind_view_server(const int server_port);

    int dummy(kvdb_protocol::operation_var var, int &r);

    int get(kvdb_protocol::operation_var var, int &r);

    int put(kvdb_protocol::operation_var var, int &r);

    int commit(kvdb_protocol::commit_var var, int &r);

    int prepare(kvdb_protocol::prepare_var var, int &r);

    int check_prepare_state(kvdb_protocol::check_prepare_state_var var, int &r);

    /**
     * Execute rollback according to `undo_logs`
     * */
    int rollback(kvdb_protocol::rollback_var var, int &r);

    void set_active(bool can_active);

    /**
     * Random pick a new replica for this shard client
     * Only used in testcase
     * */
    int shuffle_primary_replica();

    std::map<int, value_entry> &get_store();

    void replicate_txn(int txn_id);

    bool active;
    int shard_id;
    int view_server_port;
    int primary_replica = 0;
    int replica_num = 5;
    rpc_node *node;
    std::vector<std::map<int, value_entry>> store;

private:
    std::map<int, std::list<undo_log_entry>> undo_log;
};

#endif //RAFT_KV_STORE_SHARD_CLIENT_H
