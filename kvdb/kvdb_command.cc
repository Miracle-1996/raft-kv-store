#include <kvdb/kvdb_command.h>
#include <utils/verify.h>

kvdb_command::kvdb_command(): kvdb_command(CMD_NONE, 0, 0, 0) {}

kvdb_command::kvdb_command(command_type type, const int &key, const int &value, const int &txn_id):
    cmd_type(type), res(std::make_shared<result>()), key(key), value(value), txn_id(txn_id), should_send_rpc(0)
{
    res->start  = std::chrono::system_clock::now();
    res->key    = key;
    res->txn_id = txn_id;
    res->type   = type;
    res->done   = false;
}

kvdb_command::kvdb_command(const kvdb_command &cmd):
    cmd_type(cmd.cmd_type), res(cmd.res), key(cmd.key), value(cmd.value), txn_id(cmd.txn_id), should_send_rpc(cmd.should_send_rpc) {}

int
kvdb_command::size() const {
    return sizeof(*this);
}

void
kvdb_command::serialize(char *buf, int size) const {
    VERIFY(size >= this->size());
    *((command_type *)buf) = cmd_type;
    *((int *)(buf + sizeof(command_type))) = key;
    *((int *)(buf + sizeof(command_type) + sizeof(int))) = value;
    *((int *)(buf + sizeof(command_type) + 2 * sizeof(int))) = txn_id;
    *((int *)(buf + sizeof(command_type) + 3 * sizeof(int))) = should_send_rpc;
}

void
kvdb_command::deserialize(const char *buf, int size) {
    cmd_type = *((command_type *)buf);
    key = *((int *)(buf + sizeof(command_type)));
    value  = *((int *)(buf + sizeof(command_type) + sizeof(int)));
    txn_id = *((int *)(buf + sizeof(command_type) + 2 * sizeof(int)));
    should_send_rpc = *((int *)(buf + sizeof(command_type) + 3 * sizeof(int)));
}

Marshall   &operator<<(Marshall &m, const kvdb_command &cmd) {
    m << (int)cmd.cmd_type << cmd.key << cmd.value;
    return m;
}

Unmarshall &operator>>(Unmarshall &u, kvdb_command &cmd) {
    int cmd_type;
    u >> cmd_type >> cmd.key >> cmd.value;
    cmd.cmd_type = (command_type)cmd_type;
    cmd.should_send_rpc = 0;
    return u;
}