#include <raft/protocol/request_vote_args.h>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

request_vote_args::request_vote_args(int term, int candidateId, int lastLogIndex, int lastLogTerm): term(term), candidateId(candidateId), lastLogIndex(lastLogIndex), lastLogTerm(lastLogTerm) {}

Marshall&   operator<<(Marshall &m, const request_vote_args &args) {
    m << args.term;
    m << args.candidateId;
    m << args.lastLogIndex;
    m << args.lastLogTerm;
    return m;
}

Unmarshall& operator>>(Unmarshall &u, request_vote_args &args) {
    u >> args.term;
    u >> args.candidateId;
    u >> args.lastLogIndex;
    u >> args.lastLogTerm;
    return u;
}