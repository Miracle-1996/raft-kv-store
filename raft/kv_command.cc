#include <cassert>
#include <cstring>
#include <raft/kv_command.h>
#include <utils/verify.h>

kv_command::kv_command(): kv_command(CMD_NONE, "", "") {}

kv_command::kv_command(command_type type, const std::string &key, const std::string &value): type(type), key(key), value(value), res(std::make_shared<result>())
{
    res->start = std::chrono::system_clock::now();
    res->key = key;
}

kv_command::kv_command(const kv_command &cmd): type(cmd.type), key(cmd.key), value(cmd.value), res(cmd.res) {}

kv_command::~kv_command() {}

int
kv_command::size() const {
    return sizeof(command_type) + 2 * sizeof(int) + key.size() + value.size();
}

void
kv_command::serialize(char *buf, int size) const {
    VERIFY(size >= this->size());
    *((command_type *)buf) = type;
    *((int *)(buf + sizeof(command_type))) = key.size();
    *((int *)(buf + sizeof(command_type) + sizeof(int))) = value.size();
    memcpy(buf + sizeof(command_type) + 2 * sizeof(int), key.data(), key.size());
    memcpy(buf + sizeof(command_type) + 2 * sizeof(int) + key.size(), value.data(), value.size());
}

void
kv_command::deserialize(const char *buf, int size) {
    type = *((command_type *)buf);
    int key_size   = *((int *)(buf + sizeof(command_type)));
    int value_size = *((int *)(buf + sizeof(command_type) + sizeof(int)));
    VERIFY((size_t)size >= sizeof(command_type) + 2 * sizeof(int) + key_size + value_size);
    key   = std::string(buf + sizeof(command_type) + 2 * sizeof(int), key_size);
    value = std::string(buf + sizeof(command_type) + 2 * sizeof(int) + key_size, value_size);
}

Marshall&   operator<<(Marshall &m, const kv_command &cmd) {
    m << (int)cmd.type << cmd.key << cmd.value;
    return m;
}

Unmarshall& operator>>(Unmarshall &u, kv_command &cmd) {
    int cmd_type;
    u >> cmd_type >> cmd.key >> cmd.value;
    cmd.type = (kv_command::command_type)cmd_type;
    return u;
}