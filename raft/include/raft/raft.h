#ifndef RAFT_KV_STORE_RAFT_H
#define RAFT_KV_STORE_RAFT_H

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <mutex>
#include <set>
#include <thread>
#include <vector>
#include <rpc/marshall.h>
#include <rpc/rpc_const.h>
#include <rpc/rpc_client.h>
#include <rpc/rpc_server.h>
#include <rpc/thread_pool.h>
#include <rpc/unmarshall.h>
#include "raft_command.h"
#include "raft_state_machine.h"
#include "raft_storage.h"
#include "raft_utils.h"
#include "protocol/append_entries_args.h"
#include "protocol/append_entries_reply.h"
#include "protocol/install_snapshot_args.h"
#include "protocol/install_snapshot_reply.h"
#include "protocol/log_entry.h"
#include "protocol/request_vote_args.h"
#include "protocol/request_vote_reply.h"

template <class state_machine, class command>
class raft {
    #define RAFT_LOG(topic, fmt, args...) \
    do { \
        auto now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();\
        printf("%ld %s " fmt, now, topic_string[topic].c_str(), ##args);\
    }   while(0)

    #define RAFT_DEBUG(topic, fmt, args...) \
    do { \
        if (ENABLE_RAFT_DEBUG == std::string("ON")) {\
            RAFT_LOG(topic, fmt, ##args);\
        }\
    }   while(0)

    static_assert(std::is_base_of<raft_command, command>(), "command must inherit from raft_command");
    static_assert(std::is_base_of<raft_state_machine, state_machine>(), "state_machine must inherit from raft_state_machine");
public:
     raft(RPCServer*, std::vector<RPCClient *>, int, raft_storage<command> *, state_machine *);
    ~raft();

    // returns whether this node is the leader, should also set the current term
    bool is_leader(int &term);

    // send a new command to the raft nodes
    // this method returns true if this raft node is the leader that successfully appends the log
    // if this node is not the leader, it returns false
    bool new_command(command cmd, int &term, int &index);

    // save a snapshot of the state machine and compact the log
    bool save_snapshot();

    // start the raft node
    // make sure all the rpc request handlers have been registered before this method
    void start();

    // stop the raft node
    // make sure all the background threads are joined in this method
    // should check whether the server should be stopped by calling is_stopped()
    // once it returns true, should break all of your long-running loops in the background threads
    void stop();

private:
    std::thread *background_apply;
    std::thread *background_commit;
    std::thread *background_election;
    std::thread *background_ping;
    // the index of this node in rpc_clients, start from 0
    int mid;
    // a big lock to protect the whole data structure
    std::mutex mtx;
    raft_role role;
    // RPC clients of all raft nodes including this node
    std::vector<RPCClient*> rpc_clients;
    // RPC server to receive and handle the RPC requests
    RPCServer *rpc_server;
    // the state machine that applies the raft log, e.g. a kv store
    state_machine *state;
    std::atomic_bool stopped;
    // persist the raft log
    raft_storage<command> *storage;
    ThreadPool *thread_pool;

    // TODO extra private member variables
    // Persistent state on all servers:
    int current_term;
    int voted_for;
    log_with_snapshot<command> log;

    // Volatile state on all servers:
    int commit_index;
    int last_applied;

    // Volatile state on leaders:
    std::vector<int> next_index;
    std::vector<int> match_index;

    // auxiliary variables
    const int majority;
    std::set<int> vote_for_me_set;
    std::chrono::milliseconds candidate_election_timeout;
    std::chrono::milliseconds follower_election_timeout;
    std::chrono::system_clock::time_point last_election_start_time;
    std::chrono::system_clock::time_point last_received_rpc_time;

private:
    // RPC handlers
    int append_entries(append_entries_args<command> arg, append_entries_reply &reply);

    int install_snapshot(install_snapshot_args arg, install_snapshot_reply &reply);

    int request_vote(request_vote_args arg, request_vote_reply &reply);

    // RPC helpers
    void send_append_entries(int target, append_entries_args<command> arg);
    void handle_append_entries_reply(int target, const append_entries_args<command> &arg, const append_entries_reply &reply);

    void send_request_vote(int target, request_vote_args arg);
    void handle_request_vote_reply(int target, const request_vote_args &arg, const request_vote_reply &reply);

    void send_install_snapshot(int target, install_snapshot_args arg);
    void handle_install_snapshot_reply(int target, const install_snapshot_args &arg, const install_snapshot_reply &reply);

private:
    bool is_stopped();

    int num_nodes();

    void run_background_apply();
    void run_background_commit();
    void run_background_election();
    void run_background_ping();

    // TODO extra private member function
    // auxiliary functions
    void check_and_update_commit_index();

    void check_and_update_term(int);

    size_t  get_last_log_index();

    size_t  get_next_log_index();

    void send_heartbeat();

    void set_current_term(int);

    void set_voted_for(int);

    void start_election();

    // debug functions
    void debug_candidate_election_timeout();

    void debug_follower_election_timeout();

    void debug_metadata();

    void debug_send_request_vote(int sid);

    void debug_recv_request_vote(request_vote_args &);

    void debug_recv_request_vote_reply(int sid, int term, bool vote);

    void debug_send_append_entries(int sid, int index, int term, int last_log_index);

    void debug_recv_append_entries(const append_entries_args<command> &);

    void debug_recv_append_entries_reply(const append_entries_reply &);

    void debug_update_match_index_and_next_index(int sid, int last_match_index, int last_next_index);

    void debug_delete_and_append_log(int last_log_index, int last_log_term);

    void debug_send_heartbeat(int sid);

    void debug_recv_heartbeat(append_entries_args<command> &);

    void debug_server_start();

    void debug_state_to_candidate(int, int);

    void debug_state_to_follower(int, int);

    void debug_state_to_leader(int, int);

    void debug_start_election();

    void debug_update_commit_index(int last_commit_index);

    void debug_update_last_applied(int value);

    void debug_voted_for(int sid, int term);

    void debug_voted_from(int, int);
};

template <class state_machine, class command>
raft<state_machine, command>::raft(RPCServer *server, std::vector<RPCClient *> clients, int index, raft_storage<command> *storage, state_machine *state):
        background_apply(nullptr),
        background_commit(nullptr),
        background_election(nullptr),
        background_ping(nullptr),
        mid(index),
        role(follower),
        rpc_clients(clients),
        rpc_server(server),
        state(state),
        stopped(false),
        storage(storage),
        current_term(0),
        voted_for(-1),
        log(storage),
        commit_index(0),
        last_applied(0),
        next_index(clients.size(), 1),
        match_index(clients.size(), 0),
        majority((num_nodes() + 1) / 2)
{
    thread_pool = new ThreadPool(32);

    // register rpc service
    rpc_server->reg(raft_rpc_opcodes::op_append_entries, this, &raft::append_entries);
    rpc_server->reg(raft_rpc_opcodes::op_install_snapshot, this, &raft::install_snapshot);
    rpc_server->reg(raft_rpc_opcodes::op_request_vote, this, &raft::request_vote);
    // TODO initialization
    current_term = storage->restore_current_term();
    voted_for = storage->restore_voted_for();
    debug_metadata();
    VERIFY(num_nodes() & 1);
    candidate_election_timeout = std::chrono::milliseconds(1000 + rand() % 500);
    srand(time(0));
    follower_election_timeout  = std::chrono::milliseconds(300 + rand() % 200);
}

template <class state_machine, class command>
raft<state_machine, command>::~raft() {
    // question order
    if (background_ping) {
        delete background_ping;
    }
    if (background_election) {
        delete background_election;
    }
    if (background_commit) {
        delete background_commit;
    }
    if (background_apply) {
        delete background_apply;
    }
    delete thread_pool;
}

/******************************************************************

                        Public Interfaces

*******************************************************************/

template <class state_machine, class command>
bool
raft<state_machine, command>::is_leader(int &term) {
    term = current_term;
    return role == leader;
}

template <class state_machine, class command>
bool
raft<state_machine, command>::is_stopped() {
    return stopped.load();
}

template <class state_machine, class command>
bool
raft<state_machine, command>::new_command(command cmd, int &term, int &index) {
    // TODO
    std::unique_lock<std::mutex> lock(mtx);
    if (not is_leader(current_term)) {
        return false;
    }

    term = current_term;
    index = get_next_log_index();
    log_entry<command> entry(term, index, cmd);
    log.append(entry);
    match_index[mid] = index;
    next_index[mid]  = match_index[mid] + 1;
    return true;
}

template <class raft_state_machine, class command>
bool
raft<raft_state_machine, command>::save_snapshot() {
    // TODO
    return true;
}

template <class state_machine, class command>
void
raft<state_machine, command>::start() {
    // TODO
    last_election_start_time = std::chrono::system_clock::now();
    last_received_rpc_time   = std::chrono::system_clock::now();
    debug_server_start();
    this->background_election = new std::thread(&raft::run_background_election, this);
    this->background_ping     = new std::thread(&raft::run_background_ping, this);
    this->background_commit   = new std::thread(&raft::run_background_commit, this);
    this->background_apply    = new std::thread(&raft::run_background_apply, this);
}

template <class state_machine, class command>
void
raft<state_machine, command>::stop() {
    stopped.store(true);
    background_ping->join();
    background_election->join();
    background_commit->join();
    background_apply->join();
    thread_pool->destroy();
}
/******************************************************************

                         RPC Related

*******************************************************************/
template <class state_machine, class command>
int
raft<state_machine, command>::append_entries(append_entries_args<command> arg, append_entries_reply &reply) {
    // TODO
    std::unique_lock<std::mutex> lock(mtx);
    if (arg.term < current_term) {
        reply.sid  = mid;
        reply.term = current_term;
        reply.success = false;
        return 0;
    }

    reply.sid  = mid;
    reply.term = current_term;
    check_and_update_term(arg.term);

    if (arg.term != current_term) {
        fprintf(stderr, "arg.term: %d current_term: %d\n", arg.term, current_term);
    }
    VERIFY(arg.term == current_term);
    // If the leader’s term (included in its RPC) is at least
    // as large as the candidate’s current term, then the candidate
    // recognizes the leader as legitimate and returns to follower state.
    VERIFY(arg.leaderId != mid);
    if (role != follower) {
        VERIFY(role == candidate);
        int last_term = current_term;
        int last_role = role;
        role = follower;
        debug_state_to_follower(last_term, last_role);
        last_received_rpc_time = std::chrono::system_clock::now();
        follower_election_timeout = std::chrono::milliseconds(300 + rand() % 200);
    }
    VERIFY(role == follower);
    last_received_rpc_time = std::chrono::system_clock::now();

    // Reply false if log doesn’t contain an entry at prevLogIndex whose term matches prevLogTerm (§5.3)
    if (arg.prevLogIndex > (int)get_last_log_index()) {
        reply.success = false;
        return 0;
    }
    // If an existing entry conflicts with a new one (same index but different terms),
    // delete the existing entry and all that follow it (§5.3)
    if (log[arg.prevLogIndex].term != arg.prevLogTerm) {
        log.delete_after(arg.prevLogIndex, true);
        reply.success = false;
        return 0;
    }

    reply.success = true;
    if (arg.log_entries.empty()) {
        debug_recv_heartbeat(arg);
    }
    else {
        debug_recv_append_entries(arg);
    }
    if (not arg.log_entries.empty()) {
        int index = get_last_log_index();
        int term  = log[index].term;
        log.delete_and_append(arg.prevLogIndex + 1, arg.log_entries);
        debug_delete_and_append_log(index, term);
    }

    if (arg.leaderCommit > commit_index) {
        // If leaderCommit > commitIndex, set commitIndex = min(leaderCommit, index of last new entry)
        int last_commit_index = commit_index;
        VERIFY(log.get_last_entry().index == (int)get_last_log_index());
        commit_index = std::min(arg.leaderCommit, (int)log.get_last_entry().index);
        debug_update_commit_index(last_commit_index);
    }
    reply.match_index = (arg.log_entries.empty()) ? -1 : get_last_log_index();

    return 0;
}

template <class state_machine, class command>
void
raft<state_machine, command>::handle_append_entries_reply(int target, const append_entries_args<command> &arg, const append_entries_reply &reply) {
    // TODO
    std::unique_lock<std::mutex> lock(mtx);

    if (reply.term > arg.term) {
        check_and_update_term(reply.term);
        return;
    }

    if (arg.term < current_term) return;

    if (role != leader) return;

    VERIFY(arg.term == current_term);

    debug_recv_append_entries_reply(reply);

    int last_match_index = match_index[target];
    int last_next_index  = next_index[target];
    if (reply.success) {
        if (reply.match_index != -1) {
            match_index[target] = reply.match_index;
            next_index[target]  = match_index[target] + 1;
        }
    }
    else  {
        next_index[target] = std::max(1, next_index[target] - 1);
    }
    debug_update_match_index_and_next_index(target, last_match_index, last_next_index);
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::send_append_entries(int target, append_entries_args<command> arg) {
    append_entries_reply reply;
    if (rpc_clients[target]->call_basic(raft_rpc_opcodes::op_append_entries, reply, time_out(RPCClient::max_limit), arg) == 0) {
        handle_append_entries_reply(target, arg, reply);
    } else {
        // TODO RPC fails
    }
}

template <class state_machine, class command>
int
raft<state_machine, command>::install_snapshot(install_snapshot_args args, install_snapshot_reply &reply) {
    // TODO
    return 0;
}

template <class state_machine, class command>
void
raft<state_machine, command>::handle_install_snapshot_reply(int target, const install_snapshot_args &arg, const install_snapshot_reply &reply) {
    // TODO
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::send_install_snapshot(int target, install_snapshot_args arg) {
    install_snapshot_reply reply;
    if (rpc_clients[target]->call_basic(raft_rpc_opcodes::op_install_snapshot, reply, time_out(RPCClient::max_limit), reply) == 0) {
        handle_install_snapshot_reply(target, arg, reply);
    } else {
        // TODO RPC fails
    }
}

template <class state_machine, class command>
int
raft<state_machine, command>::request_vote(request_vote_args args, request_vote_reply &reply) {
    // TODO
    std::unique_lock<std::mutex> lock(mtx);
    debug_recv_request_vote(args);
    if (args.term < current_term) {
        reply.sid  = mid;
        reply.term = current_term;
        reply.voteGranted = false;
        return 0;
    }

    reply.sid  = mid;
    reply.term = current_term;

    check_and_update_term(args.term);
    VERIFY(args.term == current_term);

    if (role != follower) {
        reply.voteGranted = false;
        return 0;
    }
    VERIFY(role == follower);
    //  Each server will vote for at most one candidate in a given term, on a first-come-first-served basis.
    if (voted_for != -1 and voted_for != args.candidateId) {
        reply.voteGranted = false;
        return 0;
    }

    int last_log_index = get_last_log_index();
    int last_log_term  = log[last_log_index].term;
    if (last_log_term > args.lastLogTerm) {
        reply.voteGranted = false;
        return 0;
    }
    if (last_log_term == args.lastLogTerm and last_log_index > args.lastLogIndex) {
        reply.voteGranted = false;
        return 0;
    }

    reply.voteGranted = true;
    set_voted_for(args.candidateId);
    last_received_rpc_time = std::chrono::system_clock::now();
    debug_voted_for(args.candidateId, args.term);

    return 0;
}

template <class state_machine, class command>
void
raft<state_machine, command>::handle_request_vote_reply(int target, const request_vote_args &arg, const request_vote_reply &reply) {
    // TODO
    std::unique_lock<std::mutex> lock(mtx);
    if (reply.term > arg.term) {
        check_and_update_term(reply.term);
        return;
    }

    if (arg.term < current_term) {
        return;
    }

    if (role != candidate) {
        return;
    }

    VERIFY(arg.term == current_term);

    if (reply.voteGranted) {
        debug_recv_request_vote_reply(reply.sid, reply.term, reply.voteGranted);
        vote_for_me_set.insert(target);
        debug_voted_from(reply.sid, reply.term);
        if ((int)vote_for_me_set.size() >= majority) {
            int last_term = current_term;
            int last_role = role;
            role = leader;
            debug_state_to_leader(last_term, last_role);
            vote_for_me_set.clear();
            // Volatile state on leaders need to be Reinitialized after election
            int nextIndex = get_next_log_index();
            fill(next_index.begin(), next_index.end(), nextIndex);
            fill(match_index.begin(), match_index.end(), 0);
            send_heartbeat();
        }
    }
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::send_request_vote(int target, request_vote_args arg) {
    request_vote_reply reply;
    if (rpc_clients[target]->call_basic(raft_rpc_opcodes::op_request_vote, reply, time_out(RPCClient::max_limit), arg) == 0) {
        handle_request_vote_reply(target, arg, reply);
    } else {
        // TODO RPC fails
    }
}

/******************************************************************

                        Background Workers

*******************************************************************/
template <class state_machine, class command>
void
raft<state_machine, command>::run_background_apply() {
    while (1) {
        if (is_stopped()) return;
        // TODO
        {
            std::unique_lock<std::mutex> lock(mtx);
            if (commit_index > last_applied) {
                for (int i = last_applied + 1; i <= commit_index; ++i) {
                    VERIFY(i <= (int)get_last_log_index());
                    state->apply_log(log[i].cmd);
                }
                int value = last_applied;
                last_applied = commit_index;
                debug_update_last_applied(value);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::run_background_commit() {
    while (1) {
        if (is_stopped()) return;
        // TODO
        {
            std::unique_lock<std::mutex> lock(mtx);
            if (is_leader(current_term)) {
                int last_log_index = get_last_log_index();
                int server_number = rpc_clients.size();
                for (int i = 0; i < server_number; ++i) {
                    int next = next_index[i];
                    if (next <= last_log_index and i != mid) {
                        int prev_log_index = next - 1;
                        int prev_log_term  = log[prev_log_index].term;
                        std::vector<log_entry<command>> log_entries = log.sub_log(next);
                        append_entries_args<command> appendEntriesArgs(current_term, mid, prev_log_index, prev_log_term, commit_index, log_entries);
                        thread_pool->add_object_task(this, &raft::send_append_entries, i, appendEntriesArgs);
                        debug_send_append_entries(i, prev_log_index, prev_log_term, last_log_index);
                    }
                }
                check_and_update_commit_index();
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::run_background_election() {
    while (1) {
        if (is_stopped()) return;
        // TODO
        {
            std::unique_lock<std::mutex> lock(mtx);
            std::chrono::system_clock::time_point current_time = std::chrono::system_clock::now();
            if (role == candidate) {
                if (current_time - last_election_start_time >= candidate_election_timeout) {
                    debug_candidate_election_timeout();
                    start_election();
                }
            }
            else if (role == follower) {
                if (current_time - last_received_rpc_time >= follower_election_timeout) {
                    debug_follower_election_timeout();
                    start_election();
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    return;
}

template <class state_machine, class command>
void
raft<state_machine, command>::run_background_ping() {
    while (1) {
        if (is_stopped()) return;
        // TODO
        {
            std::unique_lock<std::mutex> lock(mtx);
            if (is_leader(current_term)) {
                send_heartbeat();
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(150));
    }
    return;
}

/******************************************************************

                        Other functions

*******************************************************************/
template <class state_machine, class command>
int
raft<state_machine, command>::num_nodes() {
    return rpc_clients.size();
}

/******************************************************************

                        Auxiliary functions

*******************************************************************/

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::check_and_update_commit_index() {
    std::vector<int> commit(match_index);
    sort(commit.begin(), commit.end());
    // If there exists an N such that N > commitIndex, a majority
    // of matchIndex[i] ≥ N, and log[N].term == currentTerm:
    // set commitIndex = N (§5.3, §5.4).
    for (int N = commit[num_nodes() / 2]; N > 0; --N) {
        if (log[N].term < current_term) {
            break;
        }
        else if (log[N].term == current_term) {
            if (N > commit_index) {
                int last_commit_index = commit_index;
                commit_index = N;
                debug_update_commit_index(last_commit_index);
                break;
            }
        }
    }
}

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::check_and_update_term(int term) {
    if (term > current_term) {
        int last_term = current_term;
        int last_role = role;
        set_current_term(term);
        role = follower;
        debug_state_to_follower(last_term, last_role);
        set_voted_for(-1);
        last_received_rpc_time = std::chrono::system_clock::now();
        follower_election_timeout = std::chrono::milliseconds(300 + rand() % 200);
    }
}

// assume acquire the mutex
template <class state_machine, class command>
size_t
raft<state_machine, command>::get_last_log_index() {
    VERIFY(log.size() >= 1);
    return log.size() - 1;
}

// assume acquire the mutex
template <class state_machine, class command>
size_t
raft<state_machine, command>::get_next_log_index() {
    return log.size();
}

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::send_heartbeat() {
    int server_number  = num_nodes();
    append_entries_args<command> appendEntriesArgs(current_term, mid, 0, 0, commit_index);
    for (int i = 0; i < server_number; ++i) {
        if (i != mid) {
            thread_pool->add_object_task(this, &raft::send_append_entries, i, appendEntriesArgs);
            debug_send_heartbeat(i);
        }
    }
}

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::set_current_term(int currentTerm) {
    storage->store_current_term(currentTerm);
    current_term = currentTerm;
    debug_metadata();
}

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::set_voted_for(int votedFor) {
    storage->store_voted_for(votedFor);
    voted_for = votedFor;
    debug_metadata();
}

// assume acquire the mutex
template <class state_machine, class command>
void
raft<state_machine, command>::start_election() {
    if (role == follower) {
        int last_term = current_term;
        int last_role = role;
        role = candidate;
        debug_state_to_candidate(last_term, last_role);
    }
    // To begin an election, a follower increments its current term and transitions to candidate state.
    set_current_term(current_term + 1);
    last_election_start_time = std::chrono::system_clock::now();
    candidate_election_timeout = std::chrono::milliseconds(1000 + rand() % 500);
    debug_start_election();
    vote_for_me_set.clear();
    // It then votes for itself and issues RequestVote RPCs in parallel to each of the other servers in the cluster.
    set_voted_for(mid);
    vote_for_me_set.insert(mid);

    int last_log_index = get_last_log_index();
    int last_log_term  = log[last_log_index].term;
    request_vote_args requestVoteArgs(current_term, mid, last_log_index, last_log_term);;
    int server_number = num_nodes();
    for (int i = 0; i < server_number; ++i) {
        if (i != mid) {
            thread_pool->add_object_task(this, &raft::send_request_vote, i, requestVoteArgs);
            debug_send_request_vote(i);
        }
    }
}

/******************************************************************

                        Debug functions

*******************************************************************/

template <class state_machine, class command>
void
raft<state_machine, command>::debug_candidate_election_timeout() {
    RAFT_DEBUG(TIMEOUT, "S%d [T:%d R:%s] Timeout\n", mid, current_term, raft_role_string[role].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_follower_election_timeout() {
    RAFT_DEBUG(TIMEOUT, "S%d [T:%d R:%s] Timeout\n", mid, current_term, raft_role_string[role].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_metadata() {
    RAFT_DEBUG(METADATA, "S%d [T:%d VF:%d R:%s] Metadata\n", mid, current_term, voted_for, raft_role_string[role].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_send_request_vote(int sid) {
    VERIFY(candidate == role);
    RAFT_DEBUG(ELECTION, "S%d [T:%d] -> S%d send RV RPC\n", mid, current_term, sid);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_recv_request_vote(request_vote_args &args) {
    RAFT_DEBUG(ELECTION, "S%d [T:%d R:%s] <- S%d [T:%d] recv RV request\n", mid, current_term, raft_role_string[role].c_str(), args.candidateId, args.term);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_recv_request_vote_reply(int sid, int term, bool vote) {
    RAFT_DEBUG(ELECTION, "S%d [T:%d R:%s] <- S%d [T:%d V:%d] recv RV reply\n", mid, current_term, raft_role_string[role].c_str(), sid, term, vote);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_send_append_entries(int sid, int index, int term, int last_log_index) {
    VERIFY(leader == role);
    RAFT_DEBUG(LOG1, "S%d [T:%d PLI:%d PLT:%d LLI:%d] -> S%d send AE RPC\n", mid, current_term, index, term, last_log_index, sid);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_recv_append_entries(const append_entries_args<command> &arg) {
    int index = get_last_log_index();
    int term  = log[index].term;
    RAFT_DEBUG(LOG1, "S%d [T:%d LLI:%d LLT:%d] <- S%d [T:%d PLI:%d PLT:%d] recv AP RPC request\n", mid, current_term, index, term, arg.leaderId, arg.term, arg.prevLogIndex, arg.prevLogTerm);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_recv_append_entries_reply(const append_entries_reply &reply) {
    RAFT_DEBUG(LOG1, "S%d [T:%d MI:%d] <- S%d [T:%d MI:%d S:%d] recv AP reply\n", mid, current_term, match_index[reply.sid], reply.sid, reply.term, reply.match_index, reply.success);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_update_match_index_and_next_index(int sid, int last_match_index, int last_next_index) {
    RAFT_DEBUG(LOG1, "S%d T%d [S%d MI:%d NI:%d] -> [S%d MI:%d NI:%d] update MI and NI\n", mid, current_term, sid, last_match_index, last_next_index, sid, match_index[sid], next_index[sid]);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_delete_and_append_log(int last_log_index, int last_log_term) {
    int index = get_last_log_index();
    int term  = log[index].term;
    RAFT_DEBUG(LOG1, "S%d [T:%d LLI:%d LLT:%d] -> [T:%d LLI:%d LLT:%d] delete and append log\n", mid, current_term, last_log_index, last_log_term, current_term, index, term);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_send_heartbeat(int sid) {
    VERIFY(leader == role);
    RAFT_DEBUG(HEART, "S%d [T:%d CI:%d] -> S%d send heartbeat\n", mid, current_term, commit_index, sid);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_recv_heartbeat(append_entries_args<command> &arg) {
    RAFT_DEBUG(HEART, "S%d [T:%d CI:%d R:%s] <- S%d [T:%d CI:%d] recv heartbeat\n", mid, current_term, commit_index, raft_role_string[role].c_str(), arg.leaderId, arg.term, arg.leaderCommit);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_server_start() {
    RAFT_DEBUG(START, "S%d [T:%d] start\n", mid, current_term);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_state_to_candidate(int last_term, int last_role) {
    VERIFY(candidate == role);
    RAFT_DEBUG(CANDIDATE, "S%d [T:%d R:%s] -> [T:%d R:%s] Candidate\n", mid, last_term, raft_role_string[last_role].c_str(), current_term, raft_role_string[candidate].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_state_to_follower(int last_term, int last_role) {
    VERIFY(follower == role);
    RAFT_DEBUG(FOLLOWER, "S%d [T:%d R:%s] -> [T:%d R:%s] Follower\n", mid, last_term, raft_role_string[last_role].c_str(), current_term, raft_role_string[follower].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_state_to_leader(int last_term, int last_role) {
    VERIFY(leader == role);
    RAFT_DEBUG(LEADER, "S%d [T:%d R:%s] -> [T:%d R:%s] Leader\n", mid, last_term, raft_role_string[last_role].c_str(), current_term, raft_role_string[leader].c_str());
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_start_election() {
    VERIFY(candidate == role);
    RAFT_DEBUG(ELECTION, "S%d [T:%d] Start Election\n", mid, current_term);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_update_commit_index(int last_commit_index) {
    RAFT_DEBUG(COMMIT, "S%d [T:%d CI:%d LA:%d] -> [T:%d CI:%d LA:%d] update commit_index\n", mid, current_term, last_commit_index, last_applied, current_term, commit_index, last_applied);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_update_last_applied(int value) {
    RAFT_DEBUG(COMMIT, "S%d [T:%d CI:%d LA:%d] -> [T:%d CI:%d LA:%d] update last_applied\n", mid, current_term, commit_index, value, current_term, commit_index, last_applied);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_voted_for(int sid, int term) {
    VERIFY(follower == role);
    RAFT_DEBUG(ELECTION, "S%d [T:%d] -> S%d [T:%d] Vote For\n", mid, current_term, sid, term);
}

template <class state_machine, class command>
void
raft<state_machine, command>::debug_voted_from(int sid, int term) {
    RAFT_DEBUG(ELECTION, "S%d [T:%d] <- S%d [T:%d] Vote From\n", mid, current_term, sid, term);
}

#endif //RAFT_KV_STORE_RAFT_H
