#ifndef RAFT_KV_STORE_RAFT_COMMAND_H
#define RAFT_KV_STORE_RAFT_COMMAND_H

class raft_command {
public:
    virtual ~raft_command() {};
    // These interfaces will be used to persistent the command.
    virtual int size() const = 0;
    virtual void serialize(char *buf, int size) const = 0;
    virtual void deserialize(const char *buf, int size) = 0;
};

#endif //RAFT_KV_STORE_RAFT_COMMAND_H
