#ifndef RAFT_KV_STORE_RAFT_STORAGE_H
#define RAFT_KV_STORE_RAFT_STORAGE_H

#include <fstream>
#include <mutex>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>
#include "protocol/log_entry.h"

constexpr int INIT_SIZE = 1024;

template <class command>
class raft_storage {
public:
     raft_storage(const std::string &);
    ~raft_storage();

    void append_log(size_t, const log_entry<command> &);

    void clear_logs_storage();

    void store_current_term(int);

    void store_voted_for(int);

    void store_logs(const std::vector<log_entry<command>> &);

    int  restore_current_term();

    int  restore_voted_for();

    void restore_logs(std::vector<log_entry<command>> &);

private:
    char *cmd_buf;
    int cmd_buf_size;
    std::mutex mtx;
    std::fstream logs_storage;
    std::fstream metadata_storage;
    std::stringstream logs_filename;
    std::stringstream metadata_filename;
};

template <class command>
raft_storage<command>::raft_storage(const std::string &dir): cmd_buf(new char[INIT_SIZE]), cmd_buf_size(INIT_SIZE) {
    logs_filename << dir << "/logs.raft";
    metadata_filename << dir << "/metadata.raft";

    FILE *file = fopen(logs_filename.str().c_str(), "a+");
    fclose(file);
    logs_storage.open(logs_filename.str(), std::ios::in | std::ios::out | std::ios::binary);

    file = fopen(metadata_filename.str().c_str(), "a+");
    fclose(file);
    metadata_storage.open(metadata_filename.str(), std::ios::in | std::ios::out | std::ios::binary);

    metadata_storage.seekg(0, std::ios::end);
    unsigned long file_size = metadata_storage.tellg();
    if (file_size < 2 * sizeof(int)) {
        char buf[sizeof(int)];
        metadata_storage.seekg(0);
        *((int *)buf) = 0;
        metadata_storage.write(buf, sizeof(int));
        *((int *)buf) = -1;
        metadata_storage.write(buf, sizeof(int));
        metadata_storage.flush();
    }
    logs_storage.seekg(0, std::ios::end);
    file_size = logs_storage.tellg();
    if (file_size <= sizeof(size_t)) {
        std::vector<log_entry<command>> empty_entry;
        command cmd;
        empty_entry.push_back({0, 0, cmd});
        store_logs(empty_entry);
    }
}

template <class command>
raft_storage<command>::~raft_storage() {
    delete []cmd_buf;
    logs_storage.close();
    metadata_storage.close();
}

template <class command>
void
raft_storage<command>::append_log(size_t size, const log_entry<command> &entry) {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(size_t)];
    logs_storage.seekg(0, std::ios::end);
    *((int *)buf) = entry.term;
    logs_storage.write(buf, sizeof(int));
    *((int *)buf) = entry.index;
    logs_storage.write(buf, sizeof(int));
    int cmd_size = entry.cmd.size();
    *((int *)buf) = cmd_size;
    logs_storage.write(buf, sizeof(int));
    if (cmd_size > cmd_buf_size) {
        delete []cmd_buf;
        cmd_buf = new char[cmd_size];
        cmd_buf_size = cmd_size;
    }
    entry.cmd.serialize(cmd_buf, cmd_size);
    logs_storage.write(cmd_buf, cmd_size);
    logs_storage.seekg(0);
    *((size_t *)buf) = size;
    logs_storage.write(buf, sizeof(size_t));
    logs_storage.flush();
}

template <class command>
void
raft_storage<command>::clear_logs_storage() {
    VERIFY(0 == truncate(logs_filename.str().c_str(), 0));
}

template <class command>
void
raft_storage<command>::store_current_term(int current_term) {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(int)];
    *((int*)buf) = current_term;
    metadata_storage.seekg(0);
    metadata_storage.write(buf, sizeof(int));
    metadata_storage.flush();
}

template <class command>
void
raft_storage<command>::store_voted_for(int voted_for) {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(int)];
    *((int*)buf) = voted_for;
    metadata_storage.seekg(sizeof(int));
    metadata_storage.write(buf, sizeof(int));
    metadata_storage.flush();
}

template <class command>
void
raft_storage<command>::store_logs(const std::vector<log_entry<command>> &log_entries) {
    std::unique_lock<std::mutex> lock(mtx);
    clear_logs_storage();
    char buf[sizeof(size_t)];
    logs_storage.seekg(sizeof(size_t));
    for (const auto &entry : log_entries) {
        *((int *)buf) = entry.term;
        logs_storage.write(buf, sizeof(int));
        *((int *)buf) = entry.index;
        logs_storage.write(buf, sizeof(int));
        int cmd_size = entry.cmd.size();
        *((int *)buf) = cmd_size;
        logs_storage.write(buf, sizeof(int));
        if (cmd_size > cmd_buf_size) {
            delete []cmd_buf;
            cmd_buf = new char[cmd_size];
            cmd_buf_size = cmd_size;
        }
        entry.cmd.serialize(cmd_buf, cmd_size);
        logs_storage.write(cmd_buf, cmd_size);
    }
    logs_storage.seekg(0);
    *((size_t *)buf) = log_entries.size();
    logs_storage.write(buf, sizeof(size_t));
    logs_storage.flush();
}

template <class command>
int
raft_storage<command>::restore_current_term() {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(int)];
    metadata_storage.seekg(0);
    metadata_storage.read(buf, sizeof(int));
    return *((int*)buf);
}

template <class command>
int
raft_storage<command>::restore_voted_for() {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(int)];
    metadata_storage.seekg(sizeof(int));
    metadata_storage.read(buf, sizeof(int));
    return *((int*)buf);
}

template <class command>
void
raft_storage<command>::restore_logs(std::vector<log_entry<command>> &log_entries) {
    std::unique_lock<std::mutex> lock(mtx);
    char buf[sizeof(size_t)];
    logs_storage.seekg(0);
    logs_storage.read(buf, sizeof(size_t));
    size_t size = *((size_t *)buf);
    for (size_t i = 0; i < size; ++i) {
        logs_storage.read(buf, sizeof(int));
        int term  = *((int *)buf);
        logs_storage.read(buf, sizeof(int));
        int index = *((int *)buf);
        logs_storage.read(buf, sizeof(int));
        int cmd_size = *((int *)buf);
        command cmd;
        if (cmd_size > cmd_buf_size) {
            delete []cmd_buf;
            cmd_buf = new char[cmd_size];
            cmd_buf_size = cmd_size;
        }
        logs_storage.read(cmd_buf, cmd_size);
        cmd.deserialize(cmd_buf, cmd_size);
        log_entries.push_back({term, index, cmd});
    }
}

#endif //RAFT_KV_STORE_RAFT_STORAGE_H
