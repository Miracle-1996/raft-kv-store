#ifndef RAFT_KV_STORE_KV_STATE_MACHINE_H
#define RAFT_KV_STORE_KV_STATE_MACHINE_H

#include <map>
#include <mutex>
#include <string>
#include <vector>
#include "raft_command.h"
#include "raft_state_machine.h"

class kv_state_machine : public raft_state_machine {
public:
    virtual ~kv_state_machine();

    // Apply a log to the state machine.
    virtual void apply_log(raft_command &) override;

    // Generate a snapshot of the current state.
    virtual std::vector<char> snapshot() override;

    // Apply the snapshot to the state machine.
    virtual void apply_snapshot(const std::vector<char> &) override;

private:
    std::mutex mtx;
    std::map<std::string, std::string> store;

    void cmd_none(kv_command &);
    void cmd_get(kv_command &);
    void cmd_put(kv_command &);
    void cmd_del(kv_command &);
    bool contain(const std::string &key);
};

#endif //RAFT_KV_STORE_KV_STATE_MACHINE_H
