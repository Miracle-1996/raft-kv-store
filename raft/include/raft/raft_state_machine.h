#ifndef RAFT_KV_STORE_RAFT_STATE_MACHINE_H
#define RAFT_KV_STORE_RAFT_STATE_MACHINE_H

#include <vector>
#include "raft_command.h"

class raft_state_machine {
public:
    virtual ~raft_state_machine() {}
    // apply a log to the state machine
    virtual void apply_log(raft_command &) = 0;
    // apply the snapshot to the state machine
    virtual void apply_snapshot(const std::vector<char> &) = 0;
    // generate a snapshot of the current state
    virtual std::vector<char> snapshot() = 0;
};

#endif //RAFT_KV_STORE_RAFT_STATE_MACHINE_H
