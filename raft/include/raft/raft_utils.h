#ifndef RAFT_KV_STORE_RAFT_UTILS_H
#define RAFT_KV_STORE_RAFT_UTILS_H

#include <string>
#include <vector>
#include <utils/verify.h>
#include "protocol/log_entry.h"

enum raft_role {
    candidate = 0,
    follower  = 1,
    leader    = 2
};

const std::string raft_role_string[] = {
        "C",
        "F",
        "L"
};

enum raft_rpc_opcodes {
    op_append_entries   = 0x1212,
    op_request_vote     = 0x3434,
    op_install_snapshot = 0x5656
};

enum TOPIC {
    HEART  = 0,
    ELECTION  = 1,
    TIMEOUT  = 2,
    CANDIDATE = 3,
    FOLLOWER = 4,
    LEADER = 5,
    START = 6,
    METADATA = 7,
    COMMIT = 8,
    LOG1 = 9,
};

const std::string topic_string[] = {
        "PING",
        "VOTE",
        "TIME",
        "CAND",
        "FOLL",
        "LEAD",
        "STAR",
        "META",
        "CMIT",
        "LOG1",
};

template <class command>
class log_with_snapshot {
public:
    log_with_snapshot(raft_storage<command> *s): start_index(0), storage(s) {
        storage->restore_logs(log_entries);
    }

    void append(log_entry<command> &entry) {
        log_entries.push_back(entry);
        storage->append_log(log_entries.size(), entry);
    }

    void append(const std::vector<log_entry<command>> &logs) {
        log_entries.insert(log_entries.end(), logs.begin(), logs.end());
    }

    void delete_after(size_t index, bool store) {
        if (index < start_index) {
            // TODO
        }
        auto iter = log_entries.begin() + (index - start_index);
        log_entries.erase(iter, log_entries.end());
        if (store) {
            storage->store_logs(log_entries);
        }
    }

    void delete_and_append(size_t index, const std::vector<log_entry<command>> &append_logs) {
        auto iter1 = log_entries.begin() + index;
        auto iter2 = append_logs.begin();
        for (; iter1 != log_entries.end() and iter2 != append_logs.end(); ++iter1, ++iter2) {
            VERIFY(iter1->index == iter2->index);
            if (iter1->term != iter2->term) {
                break;
            }
        }
        bool revise = false;
        if (iter1 != log_entries.end()) {
            revise = true;
            delete_after(iter1 - log_entries.begin() + start_index, false);
        }
        if (iter2 != append_logs.end()) {
            revise = true;
            log_entries.insert(log_entries.end(), iter2, append_logs.end());
        }
        if (revise) {
            storage->store_logs(log_entries);
        }
    }

    log_entry<command> get_last_entry() {
        return log_entries.back();
    }

    size_t size() {
        return start_index + log_entries.size();
    }

    std::vector<log_entry<command>> sub_log(size_t index) {
        if (index < start_index) {
            // TODO
        }
        auto iter = log_entries.begin() + (index - start_index);
        return std::vector<log_entry<command>>(iter, log_entries.end());
    }

    log_entry<command> &operator[](size_t index) {
        if (index < start_index) {
            VERIFY(0);
        }
        return log_entries[index - start_index];
    }

private:
    size_t start_index;
    raft_storage<command> *storage;
    std::vector<log_entry<command>> log_entries;
};

#endif //RAFT_KV_STORE_RAFT_UTILS_H
