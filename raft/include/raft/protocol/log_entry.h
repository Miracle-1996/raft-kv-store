#ifndef RAFT_KV_STORE_LOG_ENTRY_H
#define RAFT_KV_STORE_LOG_ENTRY_H

#include <cstring>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

template <class command>
class log_entry {
public:
    int term;
    int index;
    command cmd;

    log_entry(): term(0), index(0) {}
    log_entry(int, int, const command&);
};

template <class command>
log_entry<command>::log_entry(int term, int index, const command &cmd): term(term), index(index), cmd(cmd) {}

template <class command>
Marshall& operator<<(Marshall &m, const log_entry<command> &entry) {
    m << entry.term;
    m << entry.index;
    m << entry.cmd;
    return m;
}

template <class command>
Unmarshall& operator>>(Unmarshall &u, log_entry<command> &entry) {
    u >> entry.term;
    u >> entry.index;
    u >> entry.cmd;
    return u;
}

#endif //RAFT_KV_STORE_LOG_ENTRY_H
