#ifndef RAFT_KV_STORE_APPEND_ENTRIES_ARGS_H
#define RAFT_KV_STORE_APPEND_ENTRIES_ARGS_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>
#include "log_entry.h"

template <class command>
class append_entries_args {
public:
    int term;
    int leaderId;
    int prevLogIndex;
    int prevLogTerm;
    int leaderCommit;
    std::vector<log_entry<command>> log_entries;

    append_entries_args() {}
    append_entries_args(int, int, int, int, int);
    append_entries_args(int, int, int, int, int, const std::vector<log_entry<command>> &);
};

template<class command>
append_entries_args<command>::append_entries_args(int term, int leaderId, int prevLogIndex, int prevLogTerm, int leaderCommit):
        term(term), leaderId(leaderId), prevLogIndex(prevLogIndex), prevLogTerm(prevLogTerm), leaderCommit(leaderCommit) {}

template<class command>
append_entries_args<command>::append_entries_args(int term, int leaderId, int prevLogIndex, int prevLogTerm, int leaderCommit, const std::vector<log_entry<command>> &log_entries):
        term(term), leaderId(leaderId), prevLogIndex(prevLogIndex), prevLogTerm(prevLogTerm), leaderCommit(leaderCommit), log_entries(log_entries) {}

template <class command>
Marshall& operator<<(Marshall &m, const append_entries_args<command> &args) {
    m << args.term;
    m << args.leaderId;
    m << args.prevLogIndex;
    m << args.prevLogTerm;
    m << args.leaderCommit;
    m << args.log_entries;
    return m;
}

template <class command>
Unmarshall& operator>>(Unmarshall &u, append_entries_args<command> &args) {
    u >> args.term;
    u >> args.leaderId;
    u >> args.prevLogIndex;
    u >> args.prevLogTerm;
    u >> args.leaderCommit;
    u >> args.log_entries;
    return u;
}

#endif //RAFT_KV_STORE_APPEND_ENTRIES_ARGS_H
