#ifndef RAFT_KV_STORE_REQUEST_VOTE_REPLY_H
#define RAFT_KV_STORE_REQUEST_VOTE_REPLY_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class request_vote_reply {
public:
    int sid;
    int term;
    bool voteGranted;

    request_vote_reply() {}
    request_vote_reply(int, int, bool);
};

Marshall&   operator<<(Marshall &m, const request_vote_reply &reply);
Unmarshall& operator>>(Unmarshall &u, request_vote_reply &reply);

#endif //RAFT_KV_STORE_REQUEST_VOTE_REPLY_H
