#ifndef RAFT_KV_STORE_INSTALL_SNAPSHOT_REPLY_H
#define RAFT_KV_STORE_INSTALL_SNAPSHOT_REPLY_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class install_snapshot_reply {
public:
};

Marshall&   operator<<(Marshall &m, const install_snapshot_reply &reply);
Unmarshall& operator>>(Unmarshall &u, install_snapshot_reply &reply);

#endif //RAFT_KV_STORE_INSTALL_SNAPSHOT_REPLY_H
