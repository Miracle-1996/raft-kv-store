#ifndef RAFT_KV_STORE_INSTALL_SNAPSHOT_ARGS_H
#define RAFT_KV_STORE_INSTALL_SNAPSHOT_ARGS_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class install_snapshot_args {
public:
};

Marshall&   operator<<(Marshall &m, const install_snapshot_args &args);
Unmarshall& operator>>(Unmarshall &u, install_snapshot_args &args);

#endif //RAFT_KV_STORE_INSTALL_SNAPSHOT_ARGS_H
