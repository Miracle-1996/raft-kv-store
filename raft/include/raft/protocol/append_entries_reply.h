#ifndef RAFT_KV_STORE_APPEND_ENTRIES_REPLY_H
#define RAFT_KV_STORE_APPEND_ENTRIES_REPLY_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class append_entries_reply {
public:
    int sid;
    int term;
    int match_index;
    bool success;

    append_entries_reply() {}
    append_entries_reply(int, int, int, bool);
};

Marshall&   operator<<(Marshall &m, const append_entries_reply &reply);
Unmarshall& operator>>(Unmarshall &u, append_entries_reply &reply);

#endif //RAFT_KV_STORE_APPEND_ENTRIES_REPLY_H
