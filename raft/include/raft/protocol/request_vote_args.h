#ifndef RAFT_KV_STORE_REQUEST_VOTE_ARGS_H
#define RAFT_KV_STORE_REQUEST_VOTE_ARGS_H

#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

class request_vote_args {
public:
    int term;
    int candidateId;
    int lastLogIndex;
    int lastLogTerm;

    request_vote_args() {}
    request_vote_args(int, int, int, int);
};

Marshall&   operator<<(Marshall &m, const request_vote_args &args);
Unmarshall& operator>>(Unmarshall &u, request_vote_args &args);

#endif //RAFT_KV_STORE_REQUEST_VOTE_ARGS_H
