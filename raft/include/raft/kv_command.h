#ifndef RAFT_KV_STORE_KV_COMMAND_H
#define RAFT_KV_STORE_KV_COMMAND_H

#include <condition_variable>
#include <chrono>
#include <mutex>
#include <string>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>
#include "raft_command.h"

class kv_command : public raft_command {
public:
    enum command_type {
        CMD_NONE, // Do nothing
        CMD_GET, // Get a key-value pair
        CMD_PUT, // Put a key-value pair
        CMD_DEL  // Delete a key-value pair
    };

    struct result {
        std::chrono::system_clock::time_point start;
        // Ops     Succ     Key     Value
        // Get       T      key     value
        // Get       F      key      ""
        // Del       T      key   old_value
        // Del       F      key      ""
        // Put       T      key   new_value
        // Put  F(replace)  key   old_value
        std::string key, value;
        bool successful;
        bool done;
        std::mutex mtx;             // protect the struct
        std::condition_variable cv; // notify the caller
    };

    kv_command();
    kv_command(command_type type, const std::string &key, const std::string &value);
    kv_command(const kv_command &);

    virtual ~kv_command();

    command_type type;
    std::string key;
    std::string value;
    std::shared_ptr<result> res;

    virtual int  size() const override;
    virtual void serialize(char *buf, int size) const override;
    virtual void deserialize(const char *buf, int size) override;
};

Marshall&   operator<<(Marshall &m, const kv_command& cmd);
Unmarshall& operator>>(Unmarshall &u, kv_command& cmd);

#endif //RAFT_KV_STORE_KV_COMMAND_H
