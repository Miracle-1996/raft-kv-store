#include <raft/protocol/request_vote_reply.h>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

request_vote_reply::request_vote_reply(int sid, int term, bool voteGranted):  sid(sid), term(term), voteGranted(voteGranted) {}

Marshall&   operator<<(Marshall &m, const request_vote_reply &reply) {
    m << reply.sid;
    m << reply.term;
    m << reply.voteGranted;
    return m;
}

Unmarshall& operator>>(Unmarshall &u, request_vote_reply &reply) {
    u >> reply.sid;
    u >> reply.term;
    u >> reply.voteGranted;
    return u;
}
