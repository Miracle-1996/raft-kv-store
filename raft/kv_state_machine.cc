#include <sstream>
#include <raft/kv_command.h>
#include <raft/kv_state_machine.h>
#include <utils/verify.h>

kv_state_machine::~kv_state_machine() {}

bool
kv_state_machine::contain(const std::string &key) {
    return (store.find(key) != store.end()) ? true : false;
}

void
kv_state_machine::cmd_none(kv_command &kv_cmd) {
    kv_cmd.res->key = "";
    kv_cmd.res->value = "";
    kv_cmd.res->successful = true;
}

void
kv_state_machine::cmd_get(kv_command &kv_cmd) {
    std::unique_lock<std::mutex> lock(mtx);
    kv_cmd.res->key = kv_cmd.key;
    if (contain(kv_cmd.key)) {
        kv_cmd.res->value = store[kv_cmd.key];
        kv_cmd.res->successful = true;
    }
    else {
        kv_cmd.res->value = "";
        kv_cmd.res->successful = false;
    }
}

void
kv_state_machine::cmd_put(kv_command &kv_cmd) {
    std::unique_lock<std::mutex> lock(mtx);
    kv_cmd.res->key = kv_cmd.key;
    if (contain(kv_cmd.key)) {
        kv_cmd.res->value = store[kv_cmd.key];
        kv_cmd.res->successful = false;
        store[kv_cmd.key] = kv_cmd.value;
    }
    else {
        kv_cmd.res->value = kv_cmd.value;
        kv_cmd.res->successful = true;
        store[kv_cmd.key] = kv_cmd.value;
    }
}

void
kv_state_machine::cmd_del(kv_command &kv_cmd) {
    std::unique_lock<std::mutex> lock(mtx);
    kv_cmd.res->key = kv_cmd.key;
    if (contain(kv_cmd.key)) {
        kv_cmd.res->value = store[kv_cmd.key];
        kv_cmd.res->successful = true;
        store.erase(kv_cmd.key);
    }
    else {
        kv_cmd.res->value = "";
        kv_cmd.res->successful = false;
    }
}

void
kv_state_machine::apply_log(raft_command &cmd) {
    kv_command &kv_cmd = dynamic_cast<kv_command&>(cmd);
    std::unique_lock<std::mutex> lock(kv_cmd.res->mtx);

    kv_cmd.res->start = std::chrono::system_clock::now();
    switch (kv_cmd.type)
    {
        case kv_command::command_type::CMD_NONE:
            cmd_none(kv_cmd);
            break;

        case kv_command::command_type::CMD_GET:
            cmd_get(kv_cmd);
            break;

        case kv_command::command_type::CMD_PUT:
            cmd_put(kv_cmd);
            break;

        case kv_command::command_type::CMD_DEL:
            cmd_del(kv_cmd);
            break;

        default:
            break;
    }
    kv_cmd.res->done = true;
    kv_cmd.res->cv.notify_all();
}

std::vector<char>
kv_state_machine::snapshot() {
    std::stringstream buf;
    {
        std::unique_lock<std::mutex> lock(mtx);
        for (auto it = store.begin(); it != store.end(); ++it) {
            buf << it->first << ':' << it->second << ':';
        }
    }

    std::vector<char> data;
    std::string string_buffer = buf.str();
    for (const char &byte : string_buffer) {
        data.push_back(byte);
    }
    return data;
}

void
kv_state_machine::apply_snapshot(const std::vector<char> &snapshot) {
    std::stringstream buf;
    for (const char &byte : snapshot) {
        buf << byte;
    }
    std::string string_buffer = buf.str();

    std::map<std::string, std::string> kv_map;
    while (string_buffer.length() > 0) {
        // key
        size_t index = string_buffer.find(':');
        std::string key = string_buffer.substr(0, index);
        string_buffer.erase(0, index + 1);
        // value
        index = string_buffer.find(':');
        std::string value = string_buffer.substr(0, index);
        string_buffer.erase(0, index + 1);
        // insert
        kv_map[key] = value;
    }
    std::unique_lock<std::mutex> lock(mtx);
    store = kv_map;
}
