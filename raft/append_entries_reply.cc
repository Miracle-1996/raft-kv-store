#include <raft/protocol/append_entries_reply.h>
#include <rpc/marshall.h>
#include <rpc/unmarshall.h>

append_entries_reply::append_entries_reply(int sid, int term, int match_index, bool success): sid(sid), term(term), match_index(match_index), success(success) {}

Marshall&   operator<<(Marshall &m, const append_entries_reply &reply) {
    m << reply.sid;
    m << reply.term;
    m << reply.match_index;
    m << reply.success;
    return m;
}

Unmarshall& operator>>(Unmarshall &u, append_entries_reply &reply) {
    u >> reply.sid;
    u >> reply.term;
    u >> reply.match_index;
    u >> reply.success;
    return u;
}